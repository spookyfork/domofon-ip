#ifndef SW_LOG
#define SW_LOG

#define LOG_CRITICAL "CRITICAL"
#define LOF_ERROR "ERROR"
#define LOG_WARNING "WARNING"
#define LOG_INFO "INFO"
#define LOG_DEBUG "DEBUG"
#define LOG_NOTSET "NOTSET"

#ifdef _DEBUG

void LOG_INIT();
void LOG_CLOSE();
void LOG(const char level[], const char format[], ...);

#else

#define LOG_INIT() {}

#define LOG_CLOSE() {}

#define LOG(a,b,...) {}

#endif

#endif // !SW_LOG


