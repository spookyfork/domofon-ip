#include "sw_sip2.h"
#include "main.h"

#include <time.h>
#include <osip2/internal.h>
#include <osip2/osip.h>
#include <osipparser2/osip_parser.h>
#include "sw_log.h"
#include "sw_pds.h"

#define LOCAL_TAG "070563644179"

int swsip_Init(struct sw_call *call, char *local_host, int local_port, int (*cb)(osip_transaction_t*, osip_message_t*, char*, int, int))
{
  if (call == NULL || local_host == NULL) {
    return SWSIP_PARAM_ERROR;
  }
  int result = 0;
  memset(call->call_id, 0, 100);
  call->cseq = 1;
  call->local_host = local_host;
  if (local_port < 0) {
    return SWSIP_PARAM_ERROR;
  }
  call->local_port = local_port;
  call->local_tag = (char*) LOCAL_TAG;
  call->remote_host = NULL;
  call->remote_port = 0;
  call->remote_tag = NULL;
  call->state = CALL_IDLE;
  if (cb == NULL) {
    return SWSIP_PARAM_ERROR;
  }
  call->send_cb = cb;
  call->invite_transaction = NULL;
  call->dead_list = (osip_list_t*) osip_malloc(sizeof(osip_list_t));
  if ((result = osip_list_init(call->dead_list)) != OSIP_SUCCESS) {
    return result;
  }

  call->last_pds = NULL;

  LOG(LOG_DEBUG, "swsip Initialized");
  return __swsip_InitOsip(call);
}

int swsip_Call(struct sw_call *call, char *address, int port)
{
  int errorCode;

  if (call->state != CALL_IDLE) {
    LOG(LOG_WARNING, "Call already in progress");
    return SWSIP_BUSY;
  }

  LOG(LOG_INFO, "Calling %s:%d", address, port);
  call->remote_host = address;
  call->remote_port = port;

  osip_message_t *invite = NULL;
  errorCode = __swsip_CreateInviteRequest(&invite, call);
  if (errorCode != SWSIP_OK) {
    return errorCode;
  }

  osip_transaction_t *transaction = NULL;
  int res = osip_transaction_init(&transaction, ICT, call->osip, invite);
  if (res != 0)
    return res;
  osip_transaction_set_your_instance(transaction, call);
  osip_event_t *sipevent = osip_new_outgoing_sipmessage(invite);
  sipevent->transactionid = transaction->transactionid;
  osip_transaction_add_event(transaction, sipevent);

  call->invite_transaction = transaction;

  call->state = CALL_INVITING;

  return SWSIP_OK;
}

int swsip_Call_with_body(struct sw_call *call, char *address, int port, char *content_type, char *body, int body_len)
{
  int errorCode;

  if (call->state != CALL_IDLE) {
    LOG(LOG_WARNING, "Call already in progress");
    return SWSIP_BUSY;
  }

  LOG(LOG_INFO, "Calling %s:%d", address, port);
  call->remote_host = address;
  call->remote_port = port;

  osip_message_t *invite = NULL;
  errorCode = __swsip_CreateInviteRequest(&invite, call);
  if (errorCode != SWSIP_OK) {
    return errorCode;
  }

  //Set body
  osip_message_set_content_type(invite, content_type);
  osip_message_set_body(invite, body, body_len);

  osip_transaction_t *transaction = NULL;
  int res = osip_transaction_init(&transaction, ICT, call->osip, invite);
  if (res != 0)
    return res;
  osip_transaction_set_your_instance(transaction, call);
  osip_event_t *sipevent = osip_new_outgoing_sipmessage(invite);
  sipevent->transactionid = transaction->transactionid;
  osip_transaction_add_event(transaction, sipevent);

  call->invite_transaction = transaction;
  call->state = CALL_INVITING;

  return SWSIP_OK;
}

int swsip_Cancel(struct sw_call *call)
{
  //Check the state of the call
  //Create CANCEL message
  //add it to osip fifo
  //update the state of the call

  if (call->state != CALL_INVITING && call->state != CALL_PROCEEDING && call->state != CALL_RINGING) {
    LOG(LOG_WARNING, "Cannot cancel current call");
    return SWSIP_WRONG_STATE;
  }

  LOG(LOG_INFO, "Cancelling connection with %s:%d", call->remote_host, call->remote_port);

  osip_message_t *cancel = NULL;
  if (__swsip_CreateCancelRequest(&cancel, call, call->invite_transaction) != SWSIP_OK) {
    //FIXME
  }

  osip_transaction_t *transaction = NULL;
  int res = osip_transaction_init(&transaction, NICT, call->osip, cancel);
  if (res != 0)
    return res;
  osip_transaction_set_your_instance(transaction, call);
  osip_event_t *sipevent = osip_new_outgoing_sipmessage(cancel);
  sipevent->transactionid = transaction->transactionid;
  osip_transaction_add_event(transaction, sipevent);

  call->state = CALL_CANCELING;

  return SWSIP_OK;
}

int swsip_Hangup(struct sw_call *call)
{
  //Check the state of the call
  //Create BYE message
  //add it to osip fifo
  //update the state of the call

  if (call->state != CALL_MEDIA_TRANSPORT) {
    LOG(LOG_WARNING, "Cannot hangup current call");
    return SWSIP_WRONG_STATE;
  }

  LOG(LOG_INFO, "Hanging up connection with %s:%d", call->remote_host, call->remote_port);

  osip_message_t *bye = NULL;
  if (__swsip_CreateByeRequest(&bye, call) != SWSIP_OK) {

  }

  osip_transaction_t *transaction = NULL;
  int res = osip_transaction_init(&transaction, NICT, call->osip, bye);
  if (res != 0)
    return res;
  osip_transaction_set_your_instance(transaction, call);
  osip_event_t *sipevent = osip_new_outgoing_sipmessage(bye);
  sipevent->transactionid = transaction->transactionid;
  osip_transaction_add_event(transaction, sipevent);

  call->state = CALL_BYEING;

  return SWSIP_OK;

}

int swsip_MessageSend(struct sw_call *call, char *body, int body_length)
{
  //Create MESSAGE message
  //add it to osip fifo

  //There are 2 ways to do this:
  //1. Send Messages Only when we are calling
  //2. Create new call id to send them
  //TODO: For now ignore because we only receive messages

  if (call->state != CALL_MEDIA_TRANSPORT) {
    LOG(LOG_WARNING, "Cannot send a message now.");
    return SWSIP_WRONG_STATE;
  }

  if (body == NULL || body_length == 0) {
    LOG(LOG_WARNING, "Message has to have a body.");
    return SWSIP_PARAM_ERROR;
  }

  LOG(LOG_INFO, "Sending MESSAGE to %s:%d", call->remote_host, call->remote_port);
  osip_message_t *message = NULL;
  if (__swsip_CreateMessageRequest(&message, call, body, body_length) != SWSIP_OK) {
    //FIXME
  }
  call->cseq++;

  osip_transaction_t *transaction = NULL;
  int res = osip_transaction_init(&transaction, NICT, call->osip, message);
  if (res != 0)
    return res;
  osip_transaction_set_your_instance(transaction, call);
  osip_event_t *sipevent = osip_new_outgoing_sipmessage(message);
  sipevent->transactionid = transaction->transactionid;
  osip_transaction_add_event(transaction, sipevent);
  return SWSIP_OK;
}

int swsip_Update(struct sw_call *call)
{
  int result = 0;
  result = osip_ict_execute(call->osip);
  if (result != OSIP_SUCCESS)
    return result;
  result = osip_ist_execute(call->osip);
  if (result != OSIP_SUCCESS)
    return result;
  result = osip_nict_execute(call->osip);
  if (result != OSIP_SUCCESS)
    return result;
  result = osip_nist_execute(call->osip);
  if (result != OSIP_SUCCESS)
    return result;

  osip_timers_ict_execute(call->osip);
  osip_timers_ist_execute(call->osip);
  osip_timers_nict_execute(call->osip);
  osip_timers_nist_execute(call->osip);

  result = __swsip_ClearTransactionList(call->dead_list);

  return result;
}

int swsip_ParseMessage(struct sw_call *call, char *message, int len)
{
  osip_event_t *event = osip_parse(message, len);
  if (event != NULL) {
    if (0 != osip_find_transaction_and_add_event(call->osip, event)) {
      osip_transaction_t *tran;
      if (MSG_IS_REQUEST(event->sip)) {
        tran = osip_create_transaction(call->osip, event);
        osip_transaction_set_your_instance(tran, call->osip);	// store osip in transaction for later usage
        osip_transaction_add_event(tran, event);
      } else {
        //ignore unknown response
      }
    }
  }
  return SWSIP_OK;
}

int swsip_Close(struct sw_call *call)
{
  __swsip_ClearTransactionList(&(call->osip->osip_ict_transactions));
  __swsip_ClearTransactionList(&(call->osip->osip_nict_transactions));
  __swsip_ClearTransactionList(&(call->osip->osip_ist_transactions));
  __swsip_ClearTransactionList(&(call->osip->osip_nict_transactions));

  osip_release(call->osip);
  __swsip_ClearTransactionList(call->dead_list);
  osip_free(call->dead_list);
  free(call->remote_tag);
  return SWSIP_OK;
}

int __swsip_ClearTransactionList(osip_list_t *list)
{

  if (list == NULL)
    return SWSIP_PARAM_ERROR;

  osip_list_iterator_t iterator;
  osip_transaction_t *transaction = (osip_transaction_t*) osip_list_get_first(list, &iterator);
  if (transaction == NULL)
    return SWSIP_OK;

  while (osip_list_iterator_has_elem(iterator)) {
    osip_transaction_free(transaction);
    transaction = (osip_transaction_t*) osip_list_get_next(&iterator);
  }
  while (list->nb_elt != 0) {
    if (osip_list_remove(list, 0) == 0)
      break;
  }
  return SWSIP_OK;
}

/**
 * Reset the call struct and free up resources so it's ready to make another call
 */
int __swsip_Reset(struct sw_call *call)
{
  __swsip_ClearTransactionList(call->dead_list);
  memset(call->call_id, 0, 100);
  call->cseq = 1;
  call->state = CALL_IDLE;
  call->remote_host = NULL;
  osip_free(call->remote_tag);
  call->remote_tag = NULL;
  //osip_transaction_free(call->invite_transaction);
  call->invite_transaction = NULL;

  return SWSIP_OK;
}

//--------------------- CALLBACKS
#pragma region callbacks

__weak int swsip_OnCallProgress_cb(struct sw_call *call)
{
  UNUSED(call);
  return SWSIP_OK;
}

__weak int swsip_OnCallRinging_cb(struct sw_call *call)
{
  UNUSED(call);
  return SWSIP_OK;
}

__weak int swsip_OnCallAccepted_cb(struct sw_call *call)
{
  UNUSED(call);
  return SWSIP_OK;
}

__weak int swsip_OnCallRefused_cb(struct sw_call *call)
{
  UNUSED(call);
  return SWSIP_OK;
}

__weak int swsip_OnCallRedirected_cb(struct sw_call *call)
{
  UNUSED(call);
  return SWSIP_OK;
}

__weak int swsip_OnCallConfirmed_cb(struct sw_call *call)
{
  UNUSED(call);
  return SWSIP_OK;
}

__weak int swsip_OnCallTimeout_cb(struct sw_call *call)
{
  UNUSED(call);
  return SWSIP_OK;
}

__weak int swsip_OnCallReInviteAccepted_cb(struct sw_call *call)
{
  UNUSED(call);
  return SWSIP_OK;
}

__weak int swsip_OnCallReInviteRefused_cb(struct sw_call *call)
{
  UNUSED(call);
  return SWSIP_OK;
}

__weak int swsip_OnCallReInviteTimeout_cb(struct sw_call *call)
{
  UNUSED(call);
  return SWSIP_OK;
}

__weak int swsip_OnCallCancel_cb(struct sw_call *call)
{
  UNUSED(call);
  return SWSIP_OK;
}

__weak int swsip_OnCallBye_cb(struct sw_call *call)
{
  UNUSED(call);
  return SWSIP_OK;
}

#pragma endregion

//-----------------MESSAGES
#pragma region messagesagent

__weak int swsip_OnMessageReceived_cb(struct sw_call *call, char *body, int length)
{
  UNUSED(call);
  if (strcmp(body, "OPEN") == 0) {
    LOG(LOG_DEBUG, "OPEN GATE\n");
  } else if (strcmp(body, "CLOSE") == 0) {
    LOG(LOG_DEBUG, "CLOSE GATE\n");
  } else {
    LOG(LOG_WARNING, "unknown message\n");
  }
  return SWSIP_OK;
}

__weak int swsip_OnMessageDelivered_cb(struct sw_call *call)
{
  UNUSED(call);
  return SWSIP_OK;
}

__weak int swsip_OnMessageFalied_cb(struct sw_call *call)
{
  UNUSED(call);
  return SWSIP_OK;
}

#pragma endregion

//-----------------OSIP2 INTEGRATION
#pragma region osipintegration

int __swsip_InitOsip(struct sw_call *call)
{
  int retVal = osip_init(&(call->osip));
  if (retVal != 0)
    return retVal;
  // callback called when a SIP message must be sent.
  osip_set_cb_send_message(call->osip, call->send_cb);
  // callback called when a SIP transaction is TERMINATED.
  osip_set_kill_transaction_callback(call->osip, OSIP_ICT_KILL_TRANSACTION, &cb_ict_kill_transaction);
  osip_set_kill_transaction_callback(call->osip, OSIP_IST_KILL_TRANSACTION, &cb_ist_kill_transaction);
  osip_set_kill_transaction_callback(call->osip, OSIP_NICT_KILL_TRANSACTION, &cb_nict_kill_transaction);
  osip_set_kill_transaction_callback(call->osip, OSIP_NIST_KILL_TRANSACTION, &cb_nist_kill_transaction);
  // callback called when the callback to send message have failed.
  osip_set_transport_error_callback(call->osip, OSIP_ICT_TRANSPORT_ERROR, &cb_transport_error);
  osip_set_transport_error_callback(call->osip, OSIP_IST_TRANSPORT_ERROR, &cb_transport_error);
  osip_set_transport_error_callback(call->osip, OSIP_NICT_TRANSPORT_ERROR, &cb_transport_error);
  osip_set_transport_error_callback(call->osip, OSIP_NIST_TRANSPORT_ERROR, &cb_transport_error);
  // callback called when a received answer has been accepted by the transaction.
  osip_set_message_callback(call->osip, OSIP_ICT_STATUS_1XX_RECEIVED, &cb_rcv1xx);
  osip_set_message_callback(call->osip, OSIP_ICT_STATUS_2XX_RECEIVED, &cb_rcv2xx);
  osip_set_message_callback(call->osip, OSIP_ICT_STATUS_3XX_RECEIVED, &cb_rcv3456xx);
  osip_set_message_callback(call->osip, OSIP_ICT_STATUS_4XX_RECEIVED, &cb_rcv3456xx);
  osip_set_message_callback(call->osip, OSIP_ICT_STATUS_5XX_RECEIVED, &cb_rcv3456xx);
  osip_set_message_callback(call->osip, OSIP_ICT_STATUS_6XX_RECEIVED, &cb_rcv3456xx);
  // callback called when a received answer has been accepted by the transaction.
  osip_set_message_callback(call->osip, OSIP_NICT_STATUS_1XX_RECEIVED, &cb_rcv1xx);
  osip_set_message_callback(call->osip, OSIP_NICT_STATUS_2XX_RECEIVED, &cb_rcv2xx);
  osip_set_message_callback(call->osip, OSIP_NICT_STATUS_3XX_RECEIVED, &cb_rcv3456xx);
  osip_set_message_callback(call->osip, OSIP_NICT_STATUS_4XX_RECEIVED, &cb_rcv3456xx);
  osip_set_message_callback(call->osip, OSIP_NICT_STATUS_5XX_RECEIVED, &cb_rcv3456xx);
  osip_set_message_callback(call->osip, OSIP_NICT_STATUS_6XX_RECEIVED, &cb_rcv3456xx);

  // callback called when a response has been sent by the transaction.
  osip_set_message_callback(call->osip, OSIP_NIST_STATUS_2XX_SENT, &cb_free_transaction);
  osip_set_message_callback(call->osip, OSIP_NIST_STATUS_4XX_SENT, &cb_free_transaction);
  osip_set_message_callback(call->osip, OSIP_NIST_STATUS_5XX_SENT, &cb_free_transaction);
  osip_set_message_callback(call->osip, OSIP_IST_STATUS_2XX_SENT, &cb_free_transaction);
  osip_set_message_callback(call->osip, OSIP_IST_STATUS_4XX_SENT, &cb_free_transaction);
  osip_set_message_callback(call->osip, OSIP_IST_STATUS_5XX_SENT, &cb_free_transaction);
  osip_set_message_callback(call->osip, OSIP_ICT_ACK_SENT, &cb_free_transaction);

  // callback called when a received request has been accepted by the transaction.
  osip_set_message_callback(call->osip, OSIP_IST_INVITE_RECEIVED, &cb_not_implemented);
  osip_set_message_callback(call->osip, OSIP_IST_ACK_RECEIVED, &cb_not_implemented);
  osip_set_message_callback(call->osip, OSIP_NIST_REGISTER_RECEIVED, &cb_not_implemented);
  osip_set_message_callback(call->osip, OSIP_NIST_BYE_RECEIVED, &cb_rcvreq);
  osip_set_message_callback(call->osip, OSIP_NIST_CANCEL_RECEIVED, &cb_rcvreq);
  osip_set_message_callback(call->osip, OSIP_NIST_INFO_RECEIVED, &cb_rcvreq);
  osip_set_message_callback(call->osip, OSIP_NIST_OPTIONS_RECEIVED, &cb_rcvreq);
  osip_set_message_callback(call->osip, OSIP_NIST_SUBSCRIBE_RECEIVED, &cb_not_implemented);
  osip_set_message_callback(call->osip, OSIP_NIST_NOTIFY_RECEIVED, &cb_not_implemented);
  osip_set_message_callback(call->osip, OSIP_NIST_UNKNOWN_REQUEST_RECEIVED, &cb_rcvreq);
  // other callbacks exists... They are optionnal.

  osip_set_message_callback(call->osip, OSIP_ICT_STATUS_TIMEOUT, &cb_ict_timeout);
  osip_set_message_callback(call->osip, OSIP_NICT_STATUS_TIMEOUT, &cb_nict_timeout);

  osip_set_application_context(call->osip, call);

  TRACE_INITIALIZE((osip_trace_level_t)6, NULL);
  return SWSIP_OK;
}

void __swsip_UpdateTag(struct sw_call *call, osip_message_t *message)
{
  if (call->remote_tag == NULL) {
    osip_uri_param_t *dest;
    if (osip_to_get_tag(message->to, &dest) == 0)
      call->remote_tag = osip_strdup(dest->gvalue);
  }
}

int __swsip_CreateInviteRequest(osip_message_t **message, struct sw_call *call)
{
  osip_message_t *sip_invite_message;
  osip_uri_t *uri;
  int errorCode;
  if ((errorCode = osip_message_init(&sip_invite_message)) != OSIP_SUCCESS)
    return errorCode;

  osip_message_set_method(sip_invite_message, osip_strdup("INVITE"));
  osip_message_set_version(sip_invite_message, osip_strdup("SIP/2.0"));
  if ((errorCode = osip_uri_init(&uri)) != OSIP_SUCCESS) {
    osip_message_free(sip_invite_message);
    return errorCode;
  }

  osip_uri_set_host(uri, osip_strdup(call->remote_host));

  char str_port[10];
  snprintf(str_port, 10, "%d", call->remote_port);

  osip_uri_set_port(uri, osip_strdup(str_port));
  osip_message_set_uri(sip_invite_message, uri);

  char tmp[200];
  snprintf(tmp, 200, "SIP/2.0/UDP %s:%d;rport;branch=z9hG4bK%u", call->local_host, call->local_port, osip_build_random_number());
  if ((errorCode = osip_message_set_via(sip_invite_message, tmp)) != OSIP_SUCCESS) {
    osip_message_free(sip_invite_message);
    return errorCode;
  }

  ERROR_CHECK_FUNCTION(errorCode, sip_invite_message, osip_message_set_max_forwards(sip_invite_message, "70"));
  snprintf(tmp, 200, "<sip:%s:%d>", call->remote_host, call->remote_port);
  ERROR_CHECK_FUNCTION(errorCode, sip_invite_message, osip_message_set_to(sip_invite_message, tmp));
  snprintf(tmp, 200, "<sip:%s>; tag = %s", call->local_host, call->local_tag);
  ERROR_CHECK_FUNCTION(errorCode, sip_invite_message, osip_message_set_from(sip_invite_message, tmp));
  snprintf(call->call_id, 100, "domofon%u@%s", osip_build_random_number(), call->remote_host);
  ERROR_CHECK_FUNCTION(errorCode, sip_invite_message, osip_message_set_call_id(sip_invite_message, call->call_id));
  snprintf(tmp, 200, "%d INVITE", call->cseq);
  ERROR_CHECK_FUNCTION(errorCode, sip_invite_message, osip_message_set_cseq(sip_invite_message, tmp));
  snprintf(tmp, 200, "<sip:%s:%d>", call->local_host, call->local_port);
  ERROR_CHECK_FUNCTION(errorCode, sip_invite_message, osip_message_set_contact(sip_invite_message, tmp));
  ERROR_CHECK_FUNCTION(errorCode, sip_invite_message, osip_message_set_expires(sip_invite_message, "3600"));
  ERROR_CHECK_FUNCTION(errorCode, sip_invite_message, osip_message_set_user_agent(sip_invite_message, "libosip 2.0"));

  *message = sip_invite_message;
  return SWSIP_OK;
}

int __swsip_CreateCancelRequest(osip_message_t **message, struct sw_call *call, osip_transaction_t *transaction)
{
  osip_message_t *cancel;
  osip_uri_t *uri;
  int errorCode;

  if ((errorCode = osip_message_init(&cancel)) != OSIP_SUCCESS)
    return errorCode;
  osip_message_set_method(cancel, osip_strdup("CANCEL"));
  osip_message_set_version(cancel, osip_strdup("SIP/2.0"));
  ERROR_CHECK_FUNCTION(errorCode, cancel, osip_uri_init(&uri));

  osip_uri_set_host(uri, osip_strdup(call->remote_host));

  char str_port[10];
  snprintf(str_port, 10, "%d", call->remote_port);

  osip_uri_set_port(uri, osip_strdup(str_port));
  osip_message_set_uri(cancel, uri);

  char tmp[200];
  osip_generic_param_t *br;
  //same branch
  ERROR_CHECK_FUNCTION(errorCode, cancel, osip_via_param_get_byname(transaction->topvia, (char*) "branch", &br));
  snprintf(tmp, 200, "SIP/2.0/UDP %s:%d;rport;branch=%s", call->local_host, call->local_port, br->gvalue);
  ERROR_CHECK_FUNCTION(errorCode, cancel, osip_message_set_via(cancel, tmp));
  ERROR_CHECK_FUNCTION(errorCode, cancel, osip_message_set_max_forwards(cancel, "70"));

  snprintf(tmp, 200, "<sip:%s:%d>", call->remote_host, call->remote_port);
  ERROR_CHECK_FUNCTION(errorCode, cancel, osip_message_set_to(cancel, tmp));

  snprintf(tmp, 200, "<sip:%s>; tag = %s", call->local_host, call->local_tag);
  ERROR_CHECK_FUNCTION(errorCode, cancel, osip_message_set_from(cancel, tmp));

  //snprintf(call->call_id, 100, "%s", call->call_id);
  ERROR_CHECK_FUNCTION(errorCode, cancel, osip_message_set_call_id(cancel, call->call_id));

  snprintf(tmp, 200, "%d CANCEL", call->cseq);
  ERROR_CHECK_FUNCTION(errorCode, cancel, osip_message_set_cseq(cancel, tmp));

  snprintf(tmp, 200, "<sip:%s:%d>", call->local_host, call->local_port);
  ERROR_CHECK_FUNCTION(errorCode, cancel, osip_message_set_contact(cancel, tmp));
  ERROR_CHECK_FUNCTION(errorCode, cancel, osip_message_set_expires(cancel, "3600"));
  ERROR_CHECK_FUNCTION(errorCode, cancel, osip_message_set_user_agent(cancel, "libosip 2.0"));

  *message = cancel;
  return SWSIP_OK;
}

int __swsip_CreateAckRequest(osip_message_t **message, struct sw_call *call, osip_transaction_t *transaction, osip_message_t *ok)
{
  osip_message_t *ack;
  osip_uri_t *uri;
  int errorCode;
  if ((errorCode = osip_message_init(&ack)) != OSIP_SUCCESS)
    return errorCode;

  osip_message_set_method(ack, osip_strdup("ACK"));
  osip_message_set_version(ack, osip_strdup("SIP/2.0"));

  //URI
  ERROR_CHECK_FUNCTION(errorCode, ack, osip_uri_clone(transaction->orig_request->req_uri, &uri));
  osip_message_set_uri(ack, uri);

  //Only top Via -- new branch
  char tmp[200];
  snprintf(tmp, 200, "SIP/2.0/UDP %s:%d;rport;branch=z9hG4bK%u", call->local_host, call->local_port, osip_build_random_number());
  ERROR_CHECK_FUNCTION(errorCode, ack, osip_message_set_via(ack, tmp));

  //Max-Forwards
  ERROR_CHECK_FUNCTION(errorCode, ack, osip_message_set_max_forwards(ack, "70"));

  //To
  ERROR_CHECK_FUNCTION(errorCode, ack, osip_to_clone(ok->to, &(ack->to)));

  //From
  snprintf(tmp, 200, "<sip:%s>; tag = %s", call->local_host, call->local_tag);
  ERROR_CHECK_FUNCTION(errorCode, ack, osip_message_set_from(ack, tmp));

  //Call-ID
  ERROR_CHECK_FUNCTION(errorCode, ack, osip_message_set_call_id(ack, call->call_id));

  //CSeq
  snprintf(tmp, 200, "%d ACK", call->cseq);
  ERROR_CHECK_FUNCTION(errorCode, ack, osip_message_set_cseq(ack, tmp));

  //Contact
  snprintf(tmp, 200, "<sip:%s:%d>", call->local_host, call->local_port);
  ERROR_CHECK_FUNCTION(errorCode, ack, osip_message_set_contact(ack, tmp));

  //Expires
  ERROR_CHECK_FUNCTION(errorCode, ack, osip_message_set_expires(ack, "3600"));

  //User-Agent
  ERROR_CHECK_FUNCTION(errorCode, ack, osip_message_set_user_agent(ack, "libosip 2.0"));

  *message = ack;
  return SWSIP_OK;
}

int __swsip_CreateByeRequest(osip_message_t **message, struct sw_call *call)
{
  osip_message_t *bye;
  osip_uri_t *uri;
  int errorCode;

  if ((errorCode = osip_message_init(&bye)) != OSIP_SUCCESS)
    return errorCode;
  osip_message_set_method(bye, osip_strdup("BYE"));
  osip_message_set_version(bye, osip_strdup("SIP/2.0"));
  ERROR_CHECK_FUNCTION(errorCode, bye, osip_uri_init(&uri));
  osip_uri_set_host(uri, osip_strdup(call->remote_host));

  char str_port[10];
  snprintf(str_port, 10, "%d", call->remote_port);
  osip_uri_set_port(uri, osip_strdup(str_port));
  osip_message_set_uri(bye, uri);

  char tmp[200];
  snprintf(tmp, 200, "SIP/2.0/UDP %s:%d;rport;branch=z9hG4bK%u", call->local_host, call->local_port, osip_build_random_number());
  ERROR_CHECK_FUNCTION(errorCode, bye, osip_message_set_via(bye, tmp));
  ERROR_CHECK_FUNCTION(errorCode, bye, osip_message_set_max_forwards(bye, "70"));

  snprintf(tmp, 200, "<sip:%s:%d>; tag = %s", call->remote_host, call->remote_port, call->remote_tag);
  ERROR_CHECK_FUNCTION(errorCode, bye, osip_message_set_to(bye, tmp));

  snprintf(tmp, 200, "<sip:%s>; tag = %s", call->local_host, call->local_tag);
  ERROR_CHECK_FUNCTION(errorCode, bye, osip_message_set_from(bye, tmp));

  //snprintf(call->call_id, 100, "%s", call->call_id);
  ERROR_CHECK_FUNCTION(errorCode, bye, osip_message_set_call_id(bye, call->call_id));

  snprintf(tmp, 200, "%d BYE", call->cseq);
  ERROR_CHECK_FUNCTION(errorCode, bye, osip_message_set_cseq(bye, tmp));

  snprintf(tmp, 200, "<sip:%s:%d>", call->local_host, call->local_port);
  ERROR_CHECK_FUNCTION(errorCode, bye, osip_message_set_contact(bye, tmp));
  ERROR_CHECK_FUNCTION(errorCode, bye, osip_message_set_expires(bye, "3600"));
  ERROR_CHECK_FUNCTION(errorCode, bye, osip_message_set_user_agent(bye, "libosip 2.0"));

  *message = bye;
  return SWSIP_OK;
}

int __swsip_CreateMessageRequest(osip_message_t **message, struct sw_call *call, char *body, int body_length)
{
  osip_message_t *msg;
  osip_uri_t *uri;
  int errorCode;
  if ((errorCode = osip_message_init(&msg)) != OSIP_SUCCESS)
    return errorCode;
  osip_message_set_method(msg, osip_strdup("MESSAGE"));
  osip_message_set_version(msg, osip_strdup("SIP/2.0"));
  ERROR_CHECK_FUNCTION(errorCode, msg, osip_uri_init(&uri));
  osip_uri_set_host(uri, osip_strdup(call->remote_host));

  char str_port[10];
  snprintf(str_port, 10, "%d", call->remote_port);
  osip_uri_set_port(uri, osip_strdup(str_port));
  osip_message_set_uri(msg, uri);

  char tmp[200];
  snprintf(tmp, 200, "SIP/2.0/UDP %s:%d;rport;branch=z9hG4bK%u", call->local_host, call->local_port, osip_build_random_number());
  ERROR_CHECK_FUNCTION(errorCode, msg, osip_message_set_via(msg, tmp));
  ERROR_CHECK_FUNCTION(errorCode, msg, osip_message_set_max_forwards(msg, "70"));

  snprintf(tmp, 200, "<sip:%s:%d> tag = %s", call->remote_host, call->remote_port, call->remote_tag);
  ERROR_CHECK_FUNCTION(errorCode, msg, osip_message_set_to(msg, tmp));

  snprintf(tmp, 200, "<sip:%s>; tag = %s", call->local_host, call->local_tag);
  ERROR_CHECK_FUNCTION(errorCode, msg, osip_message_set_from(msg, tmp));

  //snprintf(call->call_id, 100, "%s", call->call_id);
  ERROR_CHECK_FUNCTION(errorCode, msg, osip_message_set_call_id(msg, call->call_id));

  snprintf(tmp, 200, "%d INVITE", call->cseq);
  ERROR_CHECK_FUNCTION(errorCode, msg, osip_message_set_cseq(msg, tmp));

  snprintf(tmp, 200, "<sip:%s:%d>", call->local_host, call->local_port);
  ERROR_CHECK_FUNCTION(errorCode, msg, osip_message_set_contact(msg, tmp));
  ERROR_CHECK_FUNCTION(errorCode, msg, osip_message_set_expires(msg, "3600"));
  ERROR_CHECK_FUNCTION(errorCode, msg, osip_message_set_user_agent(msg, "libosip 2.0"));

  *message = msg;
  return SWSIP_OK;
}

int __swsip_CreateResponse(osip_message_t **message, struct sw_call *call, osip_message_t *request, int status_code, const char reason[])
{
  osip_message_t *response;
  int errorCode;
  if ((errorCode = osip_message_init(&response)) != OSIP_SUCCESS) {
    return errorCode;
  }

  ERROR_CHECK_FUNCTION(errorCode, response, osip_from_clone(request->from, &(response->from)));
  ERROR_CHECK_FUNCTION(errorCode, response, osip_to_clone(request->to, &(response->to)));
  ERROR_CHECK_FUNCTION(errorCode, response, osip_cseq_clone(request->cseq, &(response->cseq)));
  ERROR_CHECK_FUNCTION(errorCode, response, osip_call_id_clone(request->call_id, &(response->call_id)));
  int (*ff)(void*, void**) = (int(*)(void*, void**)) &osip_via_clone;
  ERROR_CHECK_FUNCTION(errorCode, response, osip_list_clone(&(request->vias), &(response->vias), ff));

  char temp[200];
  ERROR_CHECK_FUNCTION(errorCode, response, osip_to_set_tag(response->to, osip_strdup(call->local_tag))); //set tag to response
  osip_message_set_version(response, osip_strdup("SIP/2.0"));

  snprintf(temp, 200, "sip:<%s:%d>", call->local_host, call->local_port);

  ERROR_CHECK_FUNCTION(errorCode, response, osip_message_set_contact(response, temp));
  ERROR_CHECK_FUNCTION(errorCode, response, osip_message_set_user_agent(response, "libosip 2.0"));

  osip_message_set_status_code(response, status_code);
  osip_message_set_reason_phrase(response, osip_strdup(reason));

  *message = response;
  return SWSIP_OK;
}

void cb_ict_kill_transaction(int type, osip_transaction_t *t)
{
  LOG(LOG_DEBUG, "*cb_ict_kill_transaction");
  struct sw_call *call = (struct sw_call*) osip_get_application_context((osip_t*) t->config);
  osip_list_add(call->dead_list, t, -1);
}

void cb_ist_kill_transaction(int type, osip_transaction_t *t)
{
  LOG(LOG_DEBUG, "*cb_ist_kill_transaction");
  struct sw_call *call = (struct sw_call*) osip_get_application_context((osip_t*) t->config);
  osip_list_add(call->dead_list, t, -1);
}

void cb_nict_kill_transaction(int type, osip_transaction_t *t)
{
  LOG(LOG_DEBUG, "*cb_nict_kill_transaction");
  struct sw_call *call = (struct sw_call*) osip_get_application_context((osip_t*) t->config);
  osip_list_add(call->dead_list, t, -1);
}

void cb_nist_kill_transaction(int type, osip_transaction_t *t)
{
  LOG(LOG_DEBUG, "*cb_nist_kill_transaction");
  struct sw_call *call = (struct sw_call*) osip_get_application_context((osip_t*) t->config);
  osip_list_add(call->dead_list, t, -1);
}

void cb_transport_error(int type, osip_transaction_t *t, int error)
{
  //osip_transaction_free(t);
  LOG(LOG_DEBUG, "*cb_transport_error");

}

void cb_free_transaction(int type, osip_transaction_t *t, osip_message_t *m)
{
  LOG(LOG_DEBUG, "*cb_free_transaction");
  struct sw_call *call = (struct sw_call*) osip_get_application_context((osip_t*) t->config);
  osip_list_add(call->dead_list, t, -1);
  //osip_transaction_free(t);
  //osip_free(t);
}

void cb_rcv1xx(int type, osip_transaction_t *t, osip_message_t *m)
{
  struct sw_call *call = (struct sw_call*) osip_transaction_get_your_instance(t);

  //Check what the response is for
  //Check and update state of the call
  //Fire callbacks
  //TODO: Ignore if something is wrong, but maybe cancel the call then

  LOG(LOG_DEBUG, "*cb_rcv1xx");
  LOG(LOG_INFO, "Received reponse %d for %s", m->status_code, t->orig_request->sip_method);

  if (MSG_IS_RESPONSE_FOR(m, "INVITE")) {
    if (MSG_TEST_CODE(m, 100)) {
      if (call->state == CALL_INVITING) {
        call->state = CALL_PROCEEDING;
        __swsip_UpdateTag(call, m);
        swsip_OnCallProgress_cb(call);
      } else {
        //Something went wrong or the response is old (?)
        //IGNORE for now
        LOG(LOG_WARNING, "Received reponse 100 for INVITE, but state of the call is %d", call->state);
      }
    } else if (MSG_TEST_CODE(m, 180)) {
      if (call->state == CALL_INVITING || call->state == CALL_PROCEEDING) {
        call->state = CALL_RINGING;
        __swsip_UpdateTag(call, m);
        swsip_OnCallRinging_cb(call);
      } else {
        //Something went wrong or the response is old (?)
        //IGNORE for now
        LOG(LOG_WARNING, "Received reponse 180 for INVITE, but state of the call is %d", call->state);
      }
    }
  }
}

void cb_rcv2xx(int type, osip_transaction_t *t, osip_message_t *m)
{

  struct sw_call *call = (struct sw_call*) osip_transaction_get_your_instance(t);
  LOG(LOG_DEBUG, "*cb_rcv2xx");
  LOG(LOG_INFO, "Received reponse %d for %s", m->status_code, t->orig_request->sip_method);

  if (MSG_IS_RESPONSE_FOR(m, "INVITE")) {
    if (MSG_TEST_CODE(m, 200)) {
      if (call->state == CALL_INVITING || call->state == CALL_PROCEEDING || call->state == CALL_RINGING) {
        //Session established

        //We are expecting PDS message in the body
        //We should be checking content-type here, but mjsip1.7 is trash
        //osip_message_get_content_type(m);
        if (call->last_pds) {
          //Free the last pds struct if we have one
          pds_free(call->last_pds);
          free(call->last_pds);
          call->last_pds = NULL;
        }

        osip_body_t *body = NULL;
        osip_message_get_body(m, 0, &body);
        if (body && body->body) {
          //If there is a body
          struct pds_message *temp_pds = malloc(sizeof(struct pds_message));
          if (PDS_OK != pds_parse(body->body, temp_pds)) {
            pds_free(temp_pds);
            free(temp_pds);
            call->last_pds = NULL;
          } else {
            call->last_pds = temp_pds;
          }
        }

        osip_message_t *ack = NULL;
        if (__swsip_CreateAckRequest(&ack, call, t, m) != SWSIP_OK) {
          //FIXME
        }
        call->osip->cb_send_message(t, ack, t->ict_context->destination, t->ict_context->port, t->out_socket);
        osip_message_free(ack);
        call->state = CALL_MEDIA_TRANSPORT;
        __swsip_UpdateTag(call, m);
        call->cseq++;
        swsip_OnCallAccepted_cb(call);

        //Free pds
        if (call->last_pds) {
          pds_free(call->last_pds);
          free(call->last_pds);
          call->last_pds = NULL;
        }

      } else {
        LOG(LOG_WARNING, "Received reponse 200 for INVITE, but state of the call is %d", call->state);
      }
    }
  } else if (MSG_IS_RESPONSE_FOR(m, "CANCEL")) {
    if (MSG_TEST_CODE(m, 200)) {
      if (call->state == CALL_CANCELING)
        call->state = CALL_CANCELING_OK;
    } else {
    }
  } else if (MSG_IS_RESPONSE_FOR(m, "BYE")) {
    if (MSG_TEST_CODE(m, 200)) {
      if (call->state == CALL_BYEING)
        //call->state = CALL_IDLE;
        __swsip_Reset(call);
    } else {
    }
  } else if (MSG_IS_RESPONSE_FOR(m, "MESSAGE")) {
    swsip_OnMessageDelivered_cb(call);
  }
}

void cb_rcv3456xx(int type, osip_transaction_t *t, osip_message_t *m)
{
  //486 Busy Here
  //487 Request terminated

  struct sw_call *call = (struct sw_call*) osip_transaction_get_your_instance(t);
  LOG(LOG_DEBUG, "*cb_rcv3456xx");
  LOG(LOG_INFO, "Received reponse %d for %s", m->status_code, t->orig_request->sip_method);

  if (MSG_IS_RESPONSE_FOR(m, "INVITE")) {
    if (call->state == CALL_INVITING || call->state == CALL_PROCEEDING || call->state == CALL_RINGING || call->state == CALL_CANCELING
        || call->state == CALL_CANCELING_OK) {
      osip_message_t *ack = NULL;
      //ict_create_ack(t, t->orig_request);
      if (__swsip_CreateAckRequest(&ack, call, t, m) != SWSIP_OK) {
        //FIXME
      }
      //sw_create_ack(m, &ack);
      osip_event_t *evt = osip_new_outgoing_sipmessage(ack);
      osip_transaction_add_event(t, evt);
    }
    if (MSG_TEST_CODE(m, 486)) {
      if (call->state != CALL_CANCELING_OK) {
        swsip_OnCallRefused_cb(call);
      } else {
        //TODO: Check for other codes and call appropriate callbacks
        swsip_OnCallRefused_cb(call);
      }
    }
    __swsip_Reset(call);
    //call->state = CALL_IDLE;
  } else if (MSG_IS_RESPONSE_FOR(m, "MESSAGE")) {
    swsip_OnMessageFalied_cb(call);
  }
}

void cb_not_implemented(int type, osip_transaction_t *t, osip_message_t *m)
{
  LOG(LOG_INFO, "Received request %s - responding with Not Implemented", m->sip_method);
  osip_message_t *resp = NULL;
  struct sw_call *call = (struct sw_call*) osip_transaction_get_your_instance(t);
  if (__swsip_CreateResponse(&resp, call, m, SIP_NOT_IMPLEMENTED, "Not Implemented") != SWSIP_OK) {
    //FIXME
  }
  osip_event_t *evt = osip_new_outgoing_sipmessage(resp);
  osip_transaction_add_event(t, evt);
}

void cb_rcvreq(int type, osip_transaction_t *t, osip_message_t *m)
{
  LOG(LOG_DEBUG, "*cb_rcvreq");
  osip_message_t *resp = NULL;
  struct sw_call *call = (struct sw_call*) osip_get_application_context((osip_t*) t->config);

  LOG(LOG_INFO, "Received request %s", m->sip_method);
  osip_event_t *evt = NULL;
  switch (type) {
  case OSIP_NIST_BYE_RECEIVED:
    //remote host wants to end the call

    //Check current state of the call
    //Respond with 200 and update the call
    //Fire callback

    if (call->state == CALL_MEDIA_TRANSPORT) {
      if (__swsip_CreateResponse(&resp, call, m, SIP_OK, "OK") != SWSIP_OK) {
        //TODO
      }
      evt = osip_new_outgoing_sipmessage(resp);
      osip_transaction_add_event(t, evt);
      //call->state = CALL_IDLE;
      swsip_OnCallBye_cb(call);
      __swsip_Reset(call);
    } else {
      LOG(LOG_WARNING, "BYE received but call is in state %d", call->state);
    }
    break;
  case OSIP_IST_ACK_RECEIVED:
    //remote host is calling us, we responded with 200 and received ACK

    break;
  case OSIP_NIST_OPTIONS_RECEIVED:
    //we received OPTIONS request from remote host (client)
    //respond with OPTIONS, BYE, MESSAGE, ACK (?)

    if (__swsip_CreateResponse(&resp, call, m, SIP_OK, "OK") != SWSIP_OK) {
      //TODO
    }
    osip_message_set_allow(resp, "OPTION, BYE, MESSAGE");
    osip_message_set_accept(resp, "application/sw_domofon");
    evt = osip_new_outgoing_sipmessage(resp);
    osip_transaction_add_event(t, evt);
    break;

  case OSIP_NIST_UNKNOWN_REQUEST_RECEIVED:
    //unknown request -- MESSAGE
    if (MSG_IS_MESSAGE(m)) {
      osip_body_t *body;
      osip_message_get_body(m, 0, &body);
      osip_message_t *ok;

      int result = swsip_OnMessageReceived_cb(call, body->body, body->length);
      const char *phrase = osip_message_get_reason(result);
      if (__swsip_CreateResponse(&ok, call, m, 200, phrase) != SWSIP_OK) {
        //XXX: Error Handling
      } else {
        evt = osip_new_outgoing_sipmessage(ok);
        osip_transaction_add_event(t, evt);
      }
      //MAKE SURE THIS IS BEING FREED

      //Decide how to respond based on the result
      //OK - SWSIP_OK -- 200
      //Cannot parse pds - SWSIP_PARAM_ERROR -- 400 Bad Request
      //Request from someone that is not in the call -- 403 Forbidden
      //Hardware error - SWSIP_UNDEFINED_ERROR -- 500 Internal Server Error



    } else {
      cb_not_implemented(type, t, m);
    }
    break;
  case OSIP_NIST_INFO_RECEIVED:
    cb_not_implemented(type, t, m);
    break;
  case OSIP_NIST_CANCEL_RECEIVED:
    //remote host initiated the call and is now cancelling it

    break;
  case OSIP_NIST_NOTIFY_RECEIVED:
    cb_not_implemented(type, t, m);
    break;
  default:
    break;
  }
}

void cb_ict_timeout(int type, osip_transaction_t *t, osip_message_t *m)
{
  struct sw_call *call = (struct sw_call*) osip_get_application_context((osip_t*) t->config);
  //call->state = CALL_IDLE;
  swsip_OnCallTimeout_cb(call);
  __swsip_Reset(call);
}

void cb_nict_timeout(int type, osip_transaction_t *t, osip_message_t *m)
{
  struct sw_call *call = (struct sw_call*) osip_get_application_context((osip_t*) t->config);
  //Message timeout , Options Timeout, Cancel? bye?
  //swsip_OnCallTimeout_cb(call);
  osip_message_t *msg = m;
  if (msg == NULL) {
    msg = t->orig_request;
  }

  if (MSG_IS_OPTIONS(msg)) {
    //Nothing
  } else if (MSG_IS_MESSAGE(msg)) {
    swsip_OnMessageFalied_cb(call);
  } else if (MSG_IS_CANCEL(msg)) {
    //call->state = CALL_IDLE;
    swsip_OnCallTimeout_cb(call);
    __swsip_Reset(call);
  } else if (MSG_IS_BYE(msg)) {
    //call->state = CALL_IDLE;
    swsip_OnCallTimeout_cb(call);
    __swsip_Reset(call);
  }
}

#pragma endregion
