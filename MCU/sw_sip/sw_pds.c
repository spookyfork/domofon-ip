/*
 * sw_pds.c
 *
 *  Created on: 1 Apr 2021
 *      Author: rand
 *
 *  07/04/2021 - Changing sampling_rate in pds_audio_header from float to uint32_t since that is what is used in STM32F4
 *    Update2 - Changed scanf and printf formats to avoid warnings,
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sw_pds.h"

#ifdef UNIT_TESTING
extern void* _test_malloc(const size_t size, const char* file, const int line);
extern void* _test_free(const size_t size, const char* file, const int line);
#define malloc(size) _test_malloc(size, __FILE__, __LINE__)
#define free(ptr) _test_free(ptr, __FILE__, __LINE__)
#endif

#ifdef WIN32
#define strcasecmp _stricmp
#else
#include <strings.h>
#endif

int pds_init_message(struct pds_message *pds)
{
  if (pds == NULL)
    return PDS_PARAM_ERROR;
  pds->audio_headers = NULL;
  pds->video_headers = NULL;
  pds->gate = 0;
  return PDS_OK;
}

int pds_free(struct pds_message *pds)
{
  int result1 = __pds_list_clear(&(pds->audio_headers));
  int result2 = __pds_list_clear(&(pds->video_headers));
  if (result1 != PDS_OK)
    return result1;
  return result2;
}

int pds_alloc_audio_header(struct pds_audio_header **header, uint16_t src_port, uint16_t dst_port, uint32_t sampling_rate, uint8_t sample_size,
    uint8_t channels, uint8_t encoding)
{
  *header = (struct pds_audio_header*) malloc(sizeof(struct pds_audio_header));
  if (*header == NULL) {
    return PDS_NOMEM;
  }
  return pds_init_audio_header(*header, src_port, dst_port, sampling_rate, sample_size, channels, encoding);
}

int pds_init_audio_header(struct pds_audio_header *header, uint16_t src_port, uint16_t dst_port, uint32_t sampling_rate, uint8_t sample_size, uint8_t channels,
    uint8_t encoding)
{
  if (header == NULL)
    return PDS_PARAM_ERROR;
  if (src_port < 0 || src_port > 65353 || dst_port < 0 || dst_port > 65353 || sampling_rate <= 0 || sample_size <= 0 || channels <= 0) {
    return PDS_PARAM_ERROR;
  }
  header->src_port = src_port;
  header->dst_port = dst_port;
  header->sampling_rate = sampling_rate;
  header->sample_size = sample_size;
  header->channels = channels;
  header->encoding = encoding;
  header->__str = NULL;
  return PDS_OK;
}

int pds_alloc_video_header(struct pds_video_header **header, uint16_t src_port, uint16_t dst_port, uint16_t width, uint16_t height, uint16_t packet_size,
    const char *video_format)
{
  *header = (struct pds_video_header*) malloc(sizeof(struct pds_video_header));
  if (*header == NULL) {
    return PDS_NOMEM;
  }
  return pds_init_video_header(*header, src_port, dst_port, width, height, packet_size, video_format);
}

int pds_init_video_header(struct pds_video_header *header, uint16_t src_port, uint16_t dst_port, uint16_t width, uint16_t height, uint16_t packet_size,
    const char *video_format)
{
  if (header == NULL || video_format == NULL)
    return PDS_PARAM_ERROR;
  if (src_port < 0 || src_port > 65353 || dst_port < 0 || dst_port > 65353 || width <= 0 || height <= 0 || header <= 0) {
    return PDS_PARAM_ERROR;
  }
  header->dst_port = dst_port;
  header->src_port = src_port;
  header->height = height;
  header->width = width;
  header->packet_size = packet_size;
  header->video_format = video_format;
  header->__str = NULL;
  return PDS_OK;
}

int pds_add_audio_header(struct pds_message *pds, struct pds_audio_header *header)
{
  if (pds == NULL || header == NULL) {
    return PDS_PARAM_ERROR;
  }
  return __pds_list_add(&(pds->audio_headers), header);
}

int pds_add_video_header(struct pds_message *pds, struct pds_video_header *header)
{
  if (pds == NULL || header == NULL) {
    return PDS_PARAM_ERROR;
  }
  return __pds_list_add(&(pds->video_headers), header);
}

/**
 * adding always to the end
 */
int __pds_list_add(struct pds_list **node, void *object)
{
  if (object == NULL || node == NULL)
    return PDS_PARAM_ERROR;
  struct pds_list *current = *node;
  struct pds_list *new_node = (struct pds_list*) malloc(sizeof(struct pds_list));
  if (new_node == NULL) {
    return PDS_NOMEM;
  }
  new_node->element = object;
  new_node->next = NULL;
  while (current && current->next != NULL) {
    current = current->next;
  }
  if (current != NULL) {
    current->next = new_node;
  } else {
    *node = new_node;
  }
  return PDS_OK;
}

/**
 * we may remove the head so we pass address to the pointer
 * DO NOT FREE THE OBJECT
 */
int __pds_list_remove(struct pds_list **node, void *object)
{
  if (node == NULL || object == NULL)
    return PDS_PARAM_ERROR;

  struct pds_list *prev = NULL;
  struct pds_list *current = *node;
  while (current != NULL && current->element != object) {
    prev = current;
    current = current->next;
  }
  if (current != NULL) {
    //We found the element
    if (prev != NULL) {
      //Not first element
      prev->next = current->next;
    } else {
      //First element
      *node = current->next;
    }
    free(current);
  }
  return PDS_OK;
}
/**
 * remove all elements with FREE()
 */

int __pds_list_clear(struct pds_list **node)
{
  if (node == NULL)
    return PDS_PARAM_ERROR;

  struct pds_list *next = NULL;
  while (*node != NULL) {
    next = (*node)->next;

    //Free internal string if there is one
    //FIXME: this works only for elements that are headers
    if (((union pds_header*) (*node)->element)->ah.__str) {
      free(((union pds_header*) (*node)->element)->ah.__str);
    }
    //Free the element
    free((*node)->element);
    //Free the node
    free((*node));
    *node = next;
  }
  *node = NULL;
  return PDS_OK;
}

/**
 * Returned buffer is with terminating character
 */
int pds_to_str(struct pds_message *pds, char **buffer, int *buffer_size)
{
  char header_buffer[100];
  int size = 0;
  int total_size = 0;
  //AudioHeaders
  struct pds_list *ah = pds->audio_headers;
  struct pds_list *vh = pds->video_headers;
  while (ah != NULL) {
    struct pds_audio_header *h = ((struct pds_audio_header*) ah->element);
    size = __pds_audio_header_to_str(h, header_buffer, 100);
    if (size < 0) {
      __pds_list_clear(&(pds->audio_headers));
      return size;
    }
    total_size += size;

    h->__str = (char*) malloc(size * sizeof(char));
    h->__str_size = size;
    memcpy(h->__str, header_buffer, size);
    ah = ah->next;
  }

  while (vh != NULL) {
    struct pds_video_header *h = ((struct pds_video_header*) vh->element);
    size = __pds_video_header_to_str(h, header_buffer, 100);
    if (size < 0) {
      __pds_list_clear(&(pds->audio_headers));
      __pds_list_clear(&(pds->video_headers));
      return size;
    }
    total_size += size;
    h->__str = (char*) malloc(size * sizeof(char));
    h->__str_size = size;
    memcpy(h->__str, header_buffer, size);
    vh = vh->next;
  }

  //Make one big message

  *buffer_size = total_size + 1;
  *buffer = (char*) malloc(sizeof(char) * (total_size + 1)); // +1 for null termination
  if (*buffer == NULL)
    return PDS_NOMEM;
  *(*buffer + total_size) = 0;

  ah = pds->audio_headers;
  vh = pds->video_headers;
  int offset = 0;
  while (ah != NULL) {
    struct pds_audio_header *h = ((struct pds_audio_header*) ah->element);
    memcpy(*buffer + offset, h->__str, h->__str_size);
    offset += h->__str_size;
    ah = ah->next;
  }

  while (vh != NULL) {
    struct pds_video_header *h = ((struct pds_video_header*) vh->element);
    memcpy(*buffer + offset, h->__str, h->__str_size);
    offset += h->__str_size;
    vh = vh->next;
  }

  return PDS_OK;
}

int __pds_video_header_to_str(struct pds_video_header *header, char *buffer, int len)
{
  //port port width height format
  if (header == NULL || buffer == NULL || len <= 0)
    return PDS_PARAM_ERROR;
  return snprintf(buffer, len, "v = %i %i %i %i %i %s\n", header->src_port, header->dst_port, header->width, header->height, header->packet_size,
      header->video_format);
}

int __pds_audio_header_to_str(struct pds_audio_header *header, char *buffer, int len)
{
  if (header == NULL || buffer == NULL || len <= 0)
    return PDS_PARAM_ERROR;
  //port port sample_rate size channels signed big_endian
  return snprintf(buffer, len, "a = %hu %hu %lu %hu %hu %s %s\n", header->src_port, header->dst_port, header->sampling_rate, header->sample_size,
      header->channels, header->encoding & PDS_AUDIO_SIGNED ? "true" : "false", header->encoding & PDS_AUDIO_BIG_ENDIAN ? "true" : "false");
}

int pds_parse(char *string, struct pds_message *pds)
{
  if (pds == NULL || string == NULL)
    return PDS_PARAM_ERROR;
  pds_init_message(pds);
  //For each line
  //Check first nonwhitespace character
  //Create header for it
  //Load data from the string line
  char *line_start = string;
  char *delim = strchr(string, '\n');
  char header_chr, eq;
  uint16_t len = 0;
  while (delim) {
    *delim = 0;

    //Need one char and '=' , len will be the next index of header value without whitespaces
    if (2 != sscanf(line_start, " %c %c %hn", &header_chr, &eq, &len)) {
      *delim = '\n';
      return PDS_SYNTAXERROR;
    }
    struct pds_audio_header *ah;
    struct pds_video_header *vh;
    if (eq == '=') {

      switch (header_chr) {
      case 'a':
        ah = malloc(sizeof(struct pds_audio_header));
        if (PDS_OK != __pds_parse_audio_header(&line_start[len], ah)) {
          free(ah);
          *delim = '\n';
          return PDS_SYNTAXERROR;
        } else {
          __pds_list_add(&(pds->audio_headers), ah);
        }
        break;
      case 'v':
        vh = malloc(sizeof(struct pds_video_header));
        if (PDS_OK != __pds_parse_video_header(&line_start[len], vh)) {
          free(vh);
          *delim = '\n';
          return PDS_SYNTAXERROR;
        } else {
          __pds_list_add(&(pds->video_headers), vh);
        }
        break;
      case 'b':
        if ( PDS_OK != __pds_parse_gate_header(&line_start[len], &(pds->gate))) {
          *delim = '\n';
          return PDS_SYNTAXERROR;
        } else {
          //
        }
        break;
      default:
        //Syntax error?
        break;
      }
    } else {
      //something aint right
    }

    *delim = '\n';
    delim++;
    line_start = delim;
    delim = strchr(delim, '\n');
  }

  return PDS_OK;
}

int __pds_parse_audio_header(char *data, struct pds_audio_header *header)
{
  if (data == NULL || header == NULL)
    return PDS_PARAM_ERROR;
  char enc_signed[20];
  char big_endian[20];
  header->__str = NULL;
  header->__str_size = 0;
  int sp, dp, ss, c;
  long int rate;
  int read_fields = sscanf(data, "%i %i %li %i %i %s %s\n", &sp, &dp, &rate, &ss, &c, enc_signed, big_endian);
  header->encoding = 0;
  if (read_fields != 7)
    return PDS_SYNTAXERROR;

  if (sp < 0 || sp > 0xffff || dp < 0 || dp > 0xffff || ss < 0 || c < 0 || rate < 0 || rate > UINT32_MAX)
    return PDS_SYNTAXERROR;

  header->channels = c;
  header->dst_port = dp;
  header->sample_size = ss;
  header->src_port = sp;
  header->sampling_rate = rate;

  if (strcasecmp("true", enc_signed) == 0) {
    header->encoding |= PDS_AUDIO_SIGNED;
  } else if (strcasecmp("false", enc_signed) == 0) {
    header->encoding &= ~PDS_AUDIO_SIGNED;
  } else {
    //Try reading it as int
    int val = INT16_MAX;
    if (1 == sscanf(enc_signed, "%i", &val)) {
      if (val > 0)
        header->encoding |= PDS_AUDIO_SIGNED;
      else
        header->encoding &= ~PDS_AUDIO_SIGNED;
    } else {
      return PDS_SYNTAXERROR;
    }
  }

  if (strcasecmp("true", big_endian) == 0) {
    header->encoding |= PDS_AUDIO_BIG_ENDIAN;
  } else if (strcasecmp("false", big_endian) == 0) {
    header->encoding &= ~PDS_AUDIO_BIG_ENDIAN;
  } else {
    //Try reading it as int
    int val = INT16_MAX;
    if (1 == sscanf(big_endian, "%i", &val)) {
      if (val > 0)
        header->encoding |= PDS_AUDIO_BIG_ENDIAN;
      else
        header->encoding &= ~PDS_AUDIO_BIG_ENDIAN;
    } else {
      return PDS_SYNTAXERROR;
    }
  }
  return pds_audio_header_validate(header);
}

int __pds_parse_video_header(char *data, struct pds_video_header *header)
{
  if (data == NULL || header == NULL)
    return PDS_PARAM_ERROR;
  header->__str = NULL;
  header->__str_size = 0;
  char temp_format[50];
  int sp, dp, w, h, ps;
  if (6 == sscanf(data, "%i %i %i %i %i %s\n", &sp, &dp, &w, &h, &ps, temp_format)) {
    if (sp < 0 || sp > 0xffff || dp < 0 || dp > 0xffff || w < 0 || h < 0) {
      return PDS_SYNTAXERROR;
    }
    header->src_port = sp;
    header->dst_port = dp;
    header->width = w;
    header->height = h;
    header->packet_size = ps;

    //FIXME: if there are other formats added
    if (strcasecmp(temp_format, PDS_VIDEO_565RGB) == 0)
      header->video_format = PDS_VIDEO_565RGB;
    else
      header->video_format = NULL;
    return pds_video_header_validate(header);
  } else {
    return PDS_SYNTAXERROR;
  }
}

int __pds_parse_gate_header(char *data, uint8_t *val)
{
  if (val == NULL || data == NULL)
    return PDS_PARAM_ERROR;
  char temp[50];
  if (1 == sscanf(data, " %s\n", temp)) {
    if (strcasecmp(temp, "open") == 0) {
      *val = PDS_GATE_OPEN;
    }
    if (strcasecmp(temp, "close") == 0) {
      *val = PDS_GATE_CLOSE;
    }
    if (strcasecmp(temp, "error") == 0) {
      *val = PDS_GATE_ERROR;
    }
  }
  return PDS_OK;
}

int pds_audio_header_validate(struct pds_audio_header *header)
{
  if (header == NULL)
    return PDS_PARAM_ERROR;
  if (header->src_port < 0 || header->src_port > 65353 || header->dst_port < 0 || header->dst_port > 65353 || header->sampling_rate <= 0
      || header->sample_size <= 0 || header->channels <= 0) {
    return PDS_SYNTAXERROR;
  }
  return PDS_OK;
}

int pds_video_header_validate(struct pds_video_header *header)
{
  if (header == NULL)
    return PDS_PARAM_ERROR;
  if (header->src_port < 0 || header->src_port > 65353 || header->dst_port < 0 || header->dst_port > 65353 || header->width <= 0 || header->height <= 0
      || header->packet_size <= 0) {
    return PDS_SYNTAXERROR;
  }
  return PDS_OK;
}

int pds_match_audio_format(struct pds_message *msg, struct pds_audio_header *header, struct pds_audio_header **result)
{
  if (msg == NULL || header == NULL || result == NULL)
    return PDS_PARAM_ERROR;

  *result = NULL;
  struct pds_list *l = msg->audio_headers;
  while (l != NULL) {
    struct pds_audio_header *th = (struct pds_audio_header*) l->element;
    if (th->channels == header->channels && th->encoding == header->encoding && th->sample_size == header->sample_size
        && th->sampling_rate == header->sampling_rate) {
      *result = th;
      return PDS_OK;
    }
    l = l->next;
  }
  return PDS_OK;
}

int pds_match_video_format(struct pds_message *msg, struct pds_video_header *header, struct pds_video_header **result)
{
  if (msg == NULL || header == NULL || result == NULL)
    return PDS_PARAM_ERROR;

  *result = NULL;
  struct pds_list *l = msg->video_headers;
  while (l != NULL) {
    struct pds_video_header *th = (struct pds_video_header*) l->element;
    if (th->width == header->width && th->height == header->height && th->packet_size == header->packet_size
        && strcasecmp(th->video_format, header->video_format) == 0) {
      *result = th;
      return PDS_OK;
    }
    l = l->next;
  }
  return PDS_OK;
}

