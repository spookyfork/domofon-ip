
#ifndef SW_SIP2
#define SW_SIP2


#include <time.h>
#include <osip2/internal.h>
#include <osip2/osip.h>
#include <osipparser2/osip_parser.h>

#include "sw_pds.h"

#ifndef UNUSED
#define UNUSED(X) (void)X      /* To avoid gcc/g++ warnings */
#endif

#define ERROR_CHECK_FUNCTION(errorCode, message, function) 	if ((errorCode = function) != OSIP_SUCCESS) { \
osip_message_free(message); \
return errorCode; }


#define SWSIP_OK 0
#define SWSIP_UNDEFINED_ERROR -1
#define SWSIP_PARAM_ERROR -2
#define SWSIP_WRONG_STATE -3
#define SWSIP_NOMEM -4
#define SWSIP_SYNTAXERROR -5
#define SWSIP_NOTFOUND -6
#define SWSIP_API_NOT_INITIALIZED -7
#define SWSIP_NO_NETWORK -10
#define SWSIP_PORT_BUSY -11
#define SWSIP_UNKNOWN_HOST -12
#define SWSIP_DISK_FULL -30
#define SWSIP_NO_RIGHTS -31
#define SWSIP_FILE_NOT_EXIST -32
#define SWSIP_TIMEOUT -50
#define SWSIP_TOOMUCHCALL -51
#define SWSIP_WRONG_FORMAT -52
#define SWSIP_NOCOMMONCODEC -53
#define SWSIP_BUSY -54

typedef enum {
	CALL_IDLE = 0,
	CALL_INVITING,
	CALL_PROCEEDING,
	CALL_RINGING,
	CALL_MEDIA_TRANSPORT,
	CALL_CANCELING,
	CALL_CANCELING_OK,
	CALL_BYEING,
	CALL_ENDING
} SW_CALL_STATE;

struct sw_call {
	char call_id[100];
	int cseq;
	SW_CALL_STATE state;
	char* remote_host;
	int remote_port;
	char* remote_tag;
	char* local_host;
	int local_port;
	char* local_tag;
	osip_t* osip;
	osip_transaction_t* invite_transaction;
	//struct node* last;
	osip_list_t *dead_list;
	int (*send_cb) (osip_transaction_t *, osip_message_t *, char *, int, int);
	struct pds_message *last_pds;
};

typedef struct sw_call sw_call_t;

//!! NO INCOMING CALLS
//--------------------- BASIC OPERATIONS
int swsip_Init(struct sw_call* call, char* local_host, int local_port, int(*cb)(osip_transaction_t *, osip_message_t *, char *, int, int));
int swsip_Call(struct sw_call* call, char* address, int port);
int swsip_Call_with_body(struct sw_call* call, char* address, int port, char* content_type, char* body, int body_len);
int swsip_Cancel(struct sw_call* call);
int swsip_Hangup(struct sw_call* call);
int swsip_Update(struct sw_call* call);
int swsip_ParseMessage(struct sw_call* call, char* message, int len);
int swsip_Close(struct sw_call* call);

int __swsip_ClearTransactionList(osip_list_t* list);
int __swsip_Reset(struct sw_call* call);

//--------------------- CALLBACKS
int swsip_MessageSend(struct sw_call* call, char* body, int body_length);
int swsip_OnCallProgress_cb(struct sw_call* call);
int swsip_OnCallRinging_cb(struct sw_call* call);
int swsip_OnCallAccepted_cb(struct sw_call* call);
int swsip_OnCallRefused_cb(struct sw_call* call);
int swsip_OnCallRedirected_cb(struct sw_call* call);
int swsip_OnCallConfirmed_cb(struct sw_call* call);
int swsip_OnCallTimeout_cb(struct sw_call* call);
int swsip_OnCallReInviteAccepted_cb(struct sw_call* call);
int swsip_OnCallReInviteRefused_cb(struct sw_call* call);
int swsip_OnCallReInviteTimeout_cb(struct sw_call* call);
int swsip_OnCallCancel_cb(struct sw_call* call);
int swsip_OnCallBye_cb(struct sw_call* call);

////--------------------- MESSAGES
int swsip_OnMessageReceived_cb(struct sw_call* call, char* body, int length);
int swsip_OnMessageDelivered_cb(struct sw_call* call);
int swsip_OnMessageFalied_cb(struct sw_call* call);

//----------------osip2
int __swsip_InitOsip(struct sw_call* call);

void __swsip_UpdateTag(struct sw_call* call, osip_message_t *message);

int __swsip_CreateInviteRequest(osip_message_t** message, struct sw_call *call);
int __swsip_CreateCancelRequest(osip_message_t** message, struct sw_call* call, osip_transaction_t * transcation);
int __swsip_CreateAckRequest(osip_message_t** message, struct sw_call* call, osip_transaction_t * transaction, osip_message_t *ok);
int __swsip_CreateByeRequest(osip_message_t** message, struct sw_call* call);
int __swsip_CreateMessageRequest(osip_message_t** message, struct sw_call* call, char* body, int body_length);
int __swsip_CreateResponse(osip_message_t** message, struct sw_call* call, osip_message_t* request, int status_code, const char reason[]);

void cb_ict_kill_transaction(int type, osip_transaction_t *t);
void cb_ist_kill_transaction(int type, osip_transaction_t *t);
void cb_nict_kill_transaction(int type, osip_transaction_t *t);
void cb_nist_kill_transaction(int type, osip_transaction_t *t);
void cb_transport_error(int type, osip_transaction_t *t, int error);
void cb_free_transaction(int type, osip_transaction_t *t, osip_message_t *m);
void cb_rcv1xx(int type, osip_transaction_t *t, osip_message_t *m);
void cb_rcv2xx(int type, osip_transaction_t *t, osip_message_t *m);
void cb_rcv3456xx(int type, osip_transaction_t *t, osip_message_t *m);
void cb_not_implemented(int type, osip_transaction_t *t, osip_message_t *m);
void cb_rcvreq(int type, osip_transaction_t *t, osip_message_t *m);
void cb_ict_timeout(int type, osip_transaction_t *t, osip_message_t *m);
void cb_nict_timeout(int type, osip_transaction_t *t, osip_message_t *m);


#endif // !SW_SIP2
