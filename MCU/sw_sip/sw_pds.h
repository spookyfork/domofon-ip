/*
 * sw_pds.h
 *
 *  Created on: 1 Apr 2021
 *      Author: rand
 */

#ifndef SW_PDS_H_
#define SW_PDS_H_

#define PDS_OK 0
#define PDS_UNDEFINED_ERROR -1
#define PDS_PARAM_ERROR -2
#define PDS_NOMEM -4
#define PDS_SYNTAXERROR -5

#define PDS_AUDIO_SIGNED 0b00000001
#define PDS_AUDIO_BIG_ENDIAN 0b00000010
#define PDS_VIDEO_565RGB "TYPE_USHORT_565_RGB"

#define PDS_GATE_CLOSE 0
#define PDS_GATE_OPEN 1
#define PDS_GATE_ERROR 2

#include <stdint.h>

/** 
 07/04/2021 - Changing sampling_rate in pds_audio_header from float to uint32_t since that is what is used in STM32F4
 
 */

struct pds_audio_header {
  uint16_t src_port;
  uint16_t dst_port;
  //Internal use only
  char *__str;
  uint16_t __str_size;

  uint32_t sampling_rate;
  uint8_t sample_size;
  uint8_t channels;
  uint8_t encoding; // = PDS_AUDIO_SIGNED | PDS_AUDIO_BIG_ENDIAN; //signed, endianness
};

struct pds_video_header {
  uint16_t src_port;
  uint16_t dst_port;
  //Internal use only
  char *__str;
  uint16_t __str_size;

  uint16_t width;
  uint16_t height;
  uint16_t packet_size;
  const char *video_format; //= PDS_VIDEO_565RGB;;
};

union pds_header {
  struct pds_audio_header ah;
  struct pds_video_header vh;
};

struct pds_list {
  void *element;
  struct pds_list *next;
};

struct pds_message {
  struct pds_list *audio_headers;
  struct pds_list *video_headers;
  uint8_t gate;
};

int pds_init_message(struct pds_message *pds);
int pds_free(struct pds_message *pds);
int pds_alloc_audio_header(struct pds_audio_header **header, uint16_t src_port, uint16_t dst_port, uint32_t sampling_rate, uint8_t sample_size,
    uint8_t channels, uint8_t encoding);
int pds_init_audio_header(struct pds_audio_header *header, uint16_t src_port, uint16_t dst_port, uint32_t sampling_rate, uint8_t sample_size, uint8_t channels,
    uint8_t encoding);
int pds_alloc_video_header(struct pds_video_header **header, uint16_t src_port, uint16_t dst_port, uint16_t width, uint16_t height, uint16_t packet_size,
    const char *video_format);
int pds_init_video_header(struct pds_video_header *header, uint16_t src_port, uint16_t dst_port, uint16_t width, uint16_t height, uint16_t packet_size,
    const char *video_format);
int pds_add_audio_header(struct pds_message *pds, struct pds_audio_header *header);
int pds_add_video_header(struct pds_message *pds, struct pds_video_header *header);
int __pds_list_add(struct pds_list **node, void *object);
int __pds_list_remove(struct pds_list **node, void *object);
int __pds_list_clear(struct pds_list **node);
int pds_to_str(struct pds_message *pds, char **buffer, int *buffer_size);
int __pds_video_header_to_str(struct pds_video_header *header, char *buffer, int len);
int __pds_audio_header_to_str(struct pds_audio_header *header, char *buffer, int len);
int pds_parse(char *string, struct pds_message *pds);
int __pds_parse_audio_header(char *data, struct pds_audio_header *header);
int __pds_parse_video_header(char *data, struct pds_video_header *header);
int __pds_parse_gate_header(char *data, uint8_t *val);
int pds_audio_header_validate(struct pds_audio_header *header);
int pds_video_header_validate(struct pds_video_header *header);
int pds_match_audio_format(struct pds_message *msg, struct pds_audio_header *header, struct pds_audio_header **result);
int pds_match_video_format(struct pds_message *msg, struct pds_video_header *header, struct pds_video_header **result);

#endif /* SW_PDS_H_ */
