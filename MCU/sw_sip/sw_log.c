#include <stdio.h>
#include <time.h>
#include <sys/timeb.h>
#include <stdarg.h>
#include "sw_log.h"

FILE *_log_file = NULL;

//Windows Debug
#ifdef _DEBUG

void LOG_INIT()
{
	_log_file = fopen("logfile.txt", "a");
}

void LOG_CLOSE()
{
	fflush(_log_file);
	fclose(_log_file);
}

void LOG(const char level[], const char format[], ...)
{
	va_list arg;
	int done;
	char time_buffer[80];
	char message_buffer[800];

	time_t timestamp;
	struct tm *ptm;
	struct timeb now;
	int tenths_ms;

	time(&timestamp);
	ptm = localtime(&timestamp);
	ftime(&now);
	tenths_ms = now.millitm;
	//create time string 
	ptm = localtime(&timestamp);
	snprintf(time_buffer, 80, "%04d-%02d-%02d %02d:%02d:%02d.%04d", 1900 + ptm->tm_year, ptm->tm_mon + 1, ptm->tm_mday, ptm->tm_hour, ptm->tm_min, ptm->tm_sec, tenths_ms);

	va_start(arg, format);
	vsnprintf(message_buffer, 800, format, arg);
	va_end(arg);

	done = fprintf(_log_file, "%s - %s - %s\n", time_buffer, level, message_buffer);

	fflush(_log_file);
}
#elif defined(TEST)

#define LOG_INIT() {}

#define LOG_CLOSE() {}

void LOG(const char level[], const char format[], ...)
{
  va_list arg;
  int done;
  char message_buffer[800];

  va_start(arg, format);
  vsnprintf(message_buffer, 800, format, arg);
  va_end(arg);

  done = printf("%s - %s\n", level, message_buffer);
}

#else

#endif
