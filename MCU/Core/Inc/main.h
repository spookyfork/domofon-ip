/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.h
 * @brief          : Header for main.c file.
 *                   This file contains the common defines of the application.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define ETH_NRST_Pin GPIO_PIN_2
#define ETH_NRST_GPIO_Port GPIOE
#define BTN_WKUP_Pin GPIO_PIN_0
#define BTN_WKUP_GPIO_Port GPIOA
#define BTN_WKUP_EXTI_IRQn EXTI0_IRQn
#define BTN_CALL_Pin GPIO_PIN_8
#define BTN_CALL_GPIO_Port GPIOD
#define BTN_CALL_EXTI_IRQn EXTI9_5_IRQn
#define BTN_CANCEL_Pin GPIO_PIN_9
#define BTN_CANCEL_GPIO_Port GPIOD
#define BTN_CANCEL_EXTI_IRQn EXTI9_5_IRQn
#define DCMI_RST_Pin GPIO_PIN_12
#define DCMI_RST_GPIO_Port GPIOD
#define LED_ORANGE_Pin GPIO_PIN_13
#define LED_ORANGE_GPIO_Port GPIOD
#define LED_RED_Pin GPIO_PIN_14
#define LED_RED_GPIO_Port GPIOD
#define LED_BLUE_Pin GPIO_PIN_15
#define LED_BLUE_GPIO_Port GPIOD
#define Audio_RST_Pin GPIO_PIN_4
#define Audio_RST_GPIO_Port GPIOD
#define DCMI_PWR_EN_Pin GPIO_PIN_6
#define DCMI_PWR_EN_GPIO_Port GPIOD
/* USER CODE BEGIN Private defines */
#define FLAG_RESET       0b00000000
#define FLAG_TX_COMPLETE 0b00000001
#define FLAG_LINE_EVENT  0b00000010
#define FLAG_FRAME_EVENT 0b00000100
#define FLAG_VSYNC_EVENT 0b00001000
#define FLAG_DCMI_ON     0b00010000
#define FLAG_SWAP        0b00100000
#define FLAG_RX_HALFCPLT 0b01000000
#define FLAG_RX_CPLT     0b10000000

#define SAMPLES_NUMBER 2048
#define IMAGE_BUFFER_SIZE 2560 // 640 * 4 lines or 1280 * 2 lines
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
