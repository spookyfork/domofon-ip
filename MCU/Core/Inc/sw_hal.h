/*
 * sw_hal.h
 *
 *  Created on: Nov 23, 2020
 *      Author: rand
 */

#ifndef INC_SW_HAL_H_
#define INC_SW_HAL_H_


void SW_MX_DMA_Init(void);
void SW_MX_I2C1_Init(void);
void SW_MX_I2S3_Init(void);
void SW_MX_DCMI_Init(void);
void SW_HAL_I2C_MspInit(I2C_HandleTypeDef* hi2c);
void SW_HAL_I2C_MspDeInit(I2C_HandleTypeDef* hi2c);
void SW_HAL_I2S_MspInit(I2S_HandleTypeDef* hi2s);
void SW_HAL_I2S_MspDeInit(I2S_HandleTypeDef* hi2s);

void HAL_DCMI_MspInit(DCMI_HandleTypeDef* hdcmi);
void HAL_DCMI_MspDeInit(DCMI_HandleTypeDef* hdcmi);

#endif /* INC_SW_HAL_H_ */
