/*
 * sw_callbacks.h
 *
 *  Created on: Oct 29, 2020
 *      Author: rand
 */

#ifndef INC_SW_CALLBACKS_H_
#define INC_SW_CALLBACKS_H_

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin);

void HAL_DCMI_ErrorCallback(DCMI_HandleTypeDef *hdcmi);
void HAL_DCMI_LineEventCallback(DCMI_HandleTypeDef *hdcmi);

void HAL_DCMI_FrameEventCallback(DCMI_HandleTypeDef *hdcmi);
void HAL_DCMI_VsyncEventCallback(DCMI_HandleTypeDef *hdcmi);

void HAL_DCMI_VsyncCallback(DCMI_HandleTypeDef *hdcmi);

void HAL_DCMI_HsyncCallback(DCMI_HandleTypeDef *hdcmi);

void sw_dma_halfcptl_callback(DMA_HandleTypeDef *_hdma);

void sw_dma_cptl_callback(DMA_HandleTypeDef *_hdma);
#endif /* INC_SW_CALLBACKS_H_ */
