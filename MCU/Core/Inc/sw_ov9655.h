/*
 * sw_ov9655.h
 *
 *  Created on: Oct 26, 2020
 *      Author: rand
 */

#ifndef INC_SW_OV9655_H_
#define INC_SW_OV9655_H_


#define OV9655_I2C_ADDRESS_W 0x60
#define OV9655_I2C_ADDRESS_R 0x61

#define OV9655_REG_GAIN 0x00
#define OV9655_REG_BLUE 0x01
#define OV9655_REG_RED 0x02
#define OV9655_REG_VREF 0x03
#define OV9655_REG_COM1 0x04
#define OV9655_REG_BAVE 0x05
#define OV9655_REG_GbAVE 0x06
#define OV9655_REG_GrAVE 0x07
#define OV9655_REG_RAVE 0x08
#define OV9655_REG_COM2 0x09
#define OV9655_REG_PID 0x0a
#define OV9655_REG_VER 0x0b
#define OV9655_REG_COM3 0x0c
#define OV9655_REG_COM4 0x0d
#define OV9655_REG_COM5 0x0e
#define OV9655_REG_COM6 0x0f
#define OV9655_REG_AEC 0x10
#define OV9655_REG_CLKRC 0x11
#define OV9655_REG_COM7 0x12
#define OV9655_REG_COM8 0x13
#define OV9655_REG_COM9 0x14
#define OV9655_REG_COM10 0x15
#define OV9655_REG_REG16 0x16
#define OV9655_REG_HSTART 0x17
#define OV9655_REG_HSTOP 0x18
#define OV9655_REG_VSTRT 0x19
#define OV9655_REG_VSTOP 0x1a
#define OV9655_REG_PSHFT 0x1b
#define OV9655_REG_MIDH 0x1c
#define OV9655_REG_MIDL 0x1d
#define OV9655_REG_MVFP 0x1e
#define OV9655_REG_LAEC 0x1f
#define OV9655_REG_BOS 0x20
#define OV9655_REG_GBOS 0x21
#define OV9655_REG_GROS 0x22
#define OV9655_REG_ROS 0x23
#define OV9655_REG_AEW 0x24
#define OV9655_REG_AEB 0x25
#define OV9655_REG_VPT 0x26
#define OV9655_REG_BBIAS 0x27
#define OV9655_REG_GbBIAS 0x28
#define OV9655_REG_PREGAIN 0x29
#define OV9655_REG_EXHCH 0x2a
#define OV9655_REG_EXHCL 0x2b
#define OV9655_REG_RBIAS 0x2c
#define OV9655_REG_ADVFL 0x2d
#define OV9655_REG_ADVFH 0x2e
#define OV9655_REG_YAVE 0x2f
#define OV9655_REG_HSYST 0x30
#define OV9655_REG_HSYSEN 0x31
#define OV9655_REG_HREF 0x32
#define OV9655_REG_CHLF 0x33
#define OV9655_REG_AREF1 0x34
#define OV9655_REG_AREF2 0x35
#define OV9655_REG_AREF3 0x36
#define OV9655_REG_ADC1 0x37
#define OV9655_REG_ADC2 0x38
#define OV9655_REG_AREF4 0x39
#define OV9655_REG_TSLB 0x3a
#define OV9655_REG_COM11 0x3b
#define OV9655_REG_COM12 0x3c
#define OV9655_REG_COM13 0x3d
#define OV9655_REG_COM14 0x3e
#define OV9655_REG_EDGE 0x3f
#define OV9655_REG_COM15 0x40
#define OV9655_REG_COM16 0x41
#define OV9655_REG_COM17 0x42
//#define OV9655_REG_ 0x43
//#define OV9655_REG_ 0x44
//#define OV9655_REG_ 0x45
//#define OV9655_REG_ 0x46
//#define OV9655_REG_ 0x47
//#define OV9655_REG_ 0x48
//#define OV9655_REG_ 0x49
//#define OV9655_REG_ 0x4a
//#define OV9655_REG_ 0x4b
//#define OV9655_REG_ 0x4c
//#define OV9655_REG_ 0x4d
//#define OV9655_REG_ 0x4e
#define OV9655_REG_MTX1 0x4f
#define OV9655_REG_MTX2 0x50
#define OV9655_REG_MTX3 0x51
#define OV9655_REG_MTX4 0x52
#define OV9655_REG_MTX5 0x53
#define OV9655_REG_MTX6 0x54
#define OV9655_REG_BRTN 0x55
#define OV9655_REG_CNST1 0x56
#define OV9655_REG_CNST2 0x57
#define OV9655_REG_MTXS 0x58
#define OV9655_REG_AWBOP1 0x59
#define OV9655_REG_AWBOP2 0x5a
#define OV9655_REG_AWBOP3 0x5b
#define OV9655_REG_AWBOP4 0x5c
#define OV9655_REG_AWBOP5 0x5d
#define OV9655_REG_AWBOP6 0x5e
#define OV9655_REG_BLMT 0x5f
#define OV9655_REG_RLMT 0x60
#define OV9655_REG_GLMT 0x61
#define OV9655_REG_LCC1 0x62
#define OV9655_REG_LCC2 0x63
#define OV9655_REG_LCC3 0x64
#define OV9655_REG_LCC4 0x65
#define OV9655_REG_LCC5 0x66
#define OV9655_REG_MANU 0x67
#define OV9655_REG_MANV 0x68
//#define OV9655_REG_ 0x69
#define OV9655_REG_BD50MAX 0x6a
#define OV9655_REG_DBLV 0x6b
//#define OV9655_REG_ 0x6c
//#define OV9655_REG_ 0x6d
//#define OV9655_REG_ 0x6e
//#define OV9655_REG_ 0x6f
#define OV9655_REG_DNSTH 0x70
//#define OV9655_REG_ 0x71
#define OV9655_REG_POIDX 0x72
#define OV9655_REG_PCKDV 0x73
#define OV9655_REG_XINDX 0x74
#define OV9655_REG_YINDX 0x75
//#define OV9655_REG_ 0x76
//#define OV9655_REG_ 0x77
//#define OV9655_REG_ 0x78
//#define OV9655_REG_ 0x79
#define OV9655_REG_SLOP 0x7a
#define OV9655_REG_GAM1 0x7b
#define OV9655_REG_GAM2 0x7c
#define OV9655_REG_GAM3 0x7d
#define OV9655_REG_GAM4 0x7e
#define OV9655_REG_GAM5 0x7f
#define OV9655_REG_GAM6 0x80
#define OV9655_REG_GAM7 0x81
#define OV9655_REG_GAM8 0x82
#define OV9655_REG_GAM9 0x83
#define OV9655_REG_GAM10 0x84
#define OV9655_REG_GAM11 0x85
#define OV9655_REG_GAM12 0x86
#define OV9655_REG_GAM13 0x87
#define OV9655_REG_GAM14 0x88
#define OV9655_REG_GAM15 0x89
//#define OV9655_REG_ 0x8a
#define OV9655_REG_COM18 0x8b
#define OV9655_REG_COM19 0x8c
#define OV9655_REG_COM20 0x8d
//#define OV9655_REG_ 0x8e
//#define OV9655_REG_ 0x8f
//#define OV9655_REG_ 0x90
//#define OV9655_REG_ 0x91
#define OV9655_REG_DMLNL 0x92
#define OV9655_REG_DMLNH 0x93
//#define OV9655_REG_ 0x94
//#define OV9655_REG_ 0x95
//#define OV9655_REG_ 0x96
//#define OV9655_REG_ 0x97
//#define OV9655_REG_ 0x98
//#define OV9655_REG_ 0x99
//#define OV9655_REG_ 0x9a
//#define OV9655_REG_ 0x9b
//#define OV9655_REG_ 0x9c
#define OV9655_REG_LCC6 0x9d
#define OV9655_REG_LCC7 0x9e
//#define OV9655_REG_ 0x9f
//#define OV9655_REG_ 0xa0
#define OV9655_REG_AECH 0xa1
#define OV9655_REG_BD50 0xa2
#define OV9655_REG_BD60 0xa3
#define OV9655_REG_COM21 0xa4
//#define OV9655_REG_ 0xa5
#define OV9655_REG_GREEN 0xa6
#define OV9655_REG_VZST 0xa7
#define OV9655_REG_REFA8 0xa8
#define OV9655_REG_REFA9 0xa9
//#define OV9655_REG_ 0xaa
//#define OV9655_REG_ 0xab
#define OV9655_REG_BLC1 0xac
#define OV9655_REG_BLC2 0xad
#define OV9655_REG_BLC3 0xae
#define OV9655_REG_BLC4 0xaf
#define OV9655_REG_BLC5 0xb0
#define OV9655_REG_BLC6 0xb1
#define OV9655_REG_BLC7 0xb2
#define OV9655_REG_BLC8 0xb3
#define OV9655_REG_CTRLB4 0xb4
//#define OV9655_REG_ 0xb5
//#define OV9655_REG_ 0xb6
#define OV9655_REG_FRSTL 0xb7
#define OV9655_REG_FRSTH 0xb8
//#define OV9655_REG_ 0xb9
//#define OV9655_REG_ 0xba
//#define OV9655_REG_ 0xbb
#define OV9655_REG_ADBOFF 0xbc
#define OV9655_REG_ADROFF 0xbd
#define OV9655_REG_ADGbOFF 0xbe
#define OV9655_REG_ADGrOFF 0xbf
//#define OV9655_REG_ 0xc0
//#define OV9655_REG_ 0xc1
//#define OV9655_REG_ 0xc2
//#define OV9655_REG_ 0xc3
#define OV9655_REG_COM23 0xc4
#define OV9655_REG_BD60MAX 0xc5
//#define OV9655_REG_ 0xc6
#define OV9655_REG_COM24 0xc7
//#define OV9655_REG_ 0xc8
//#define OV9655_REG_ 0xc9
//#define OV9655_REG_ 0xca
//#define OV9655_REG_ 0xcb
//#define OV9655_REG_ 0xcc
//#define OV9655_REG_ 0xcd
//#define OV9655_REG_ 0xce
//#define OV9655_REG_ 0xcf

uint8_t sw_write_camera_reg(uint8_t controlBytes[], uint8_t numBytes);
uint8_t sw_write_camera_reg_masked(uint8_t registerAddr, uint8_t value, uint8_t mask);
uint8_t sw_read_camera_reg(uint8_t mapbyte, uint8_t* received_byte);
void sw_read_all_registers(uint8_t *RegArray);
uint8_t sw_ov9655_check_registers();

uint8_t sw_ov9655_init();
uint8_t sw_ov9655_init_qvga();
uint8_t sw_ov9655_reset_regs();
uint8_t sw_ov9655_power_up();
uint8_t sw_ov9655_deinit();
uint8_t __sw_ov9655_qvga_setup();
uint8_t __sw_ov9655_vga_setup();


#endif /* INC_SW_OV9655_H_ */
