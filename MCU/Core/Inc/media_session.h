/*
 * media_session.h
 *
 *
 *  Created on: Apr 19, 2021
 *      Author: rand
 */

#ifndef INC_MEDIA_SESSION_H_
#define INC_MEDIA_SESSION_H_

 //Src and Rcv Ports
 //remote Host
 //Prepare networking stuff for session(ports etc recv_cb, send function)
 //
 //Start Media session
 //Stop media session
 //Close ports etc

#include <stdint.h>

#ifndef UNIT_TESTING
#include "lwip.h"
#else
typedef char* ip_addr_t;
typedef ip_addr_t ip4_addr_t;

#define IP_ADDR_ANY        (&ip_addr_any)
#define PBUF_TRANSPORT 2
#define PBUF_RAM 3

typedef void(*udp_recv_fn)(void *arg, struct udp_pcb *pcb, struct pbuf *p,
  const ip_addr_t *addr, uint16_t port);

struct udp_pcb {
  uint16_t local_port;
  udp_recv_fn recv;
  void*  recv_arg;
};

struct pbuf {
  void *payload;
  int len;
};
#endif // !UNIT_TESTING


#define SWMS_OK 0
#define SWMS_UNDEFINED_ERROR -1
#define SWMS_PARAM_ERROR -2
#define SWMS_NOMEM -4
#define SWMS_SYNTAXERROR -5
#define SWMS_NOTFOUND -6
#define SWMS_API_NOT_INITIALIZED -7
#define SWMS_NO_NETWORK -10
#define SWMS_PORT_BUSY -11
#define SWMS_UNKNOWN_HOST -12

#define __SWMS_STATE_RESET            0b00000000
#define __SWMS_STATE_INIT             0b00000001
#define __SWMS_STATE_OPEN             0b00000010
#define __SWMS_STATE_ACTIVE           0b00000100
#define __SWMS_STATE_CLOSED           0b10000000

#define __SWMS_CONNECTION_RESET       0b00000000
#define __SWMS_CONNECTION_IN          0b00000001
#define __SWMS_CONNECTION_OUT         0b00000010
#define __SWMS_CONNECTION_IN_ACT      0b00001000
#define __SWMS_CONNECTION_OUT_ACT     0b00010000

typedef void(*swms_recv_fn)(void *data, int len);
typedef void(*void_fn)(void);
typedef int(*signal_fn)(int signal);

enum CB_SIGNALS {
  MS_START_AUDIO_IN = 0, MS_START_AUDIO_OUT, MS_START_VIDEO_OUT, MS_STOP_AUDIO_IN, MS_STOP_AUDIO_OUT, MS_STOP_VIDEO_OUT, CB_SIZE
};

struct session {
  int32_t src_port;
  int32_t recv_port;
  int32_t remote_src_port;
  int32_t remote_dst_port;
  uint8_t __connection;
  swms_recv_fn recv;
  struct udp_pcb *stack;
};

struct media_session {
  struct session audio;
  struct session video;
  uint8_t __state;
  const char *host;
  signal_fn callbacks[CB_SIZE];
};

int swms_Init(struct media_session *ms, int src_port, int recv_port, swms_recv_fn callback, int video_src_port, int video_recv_port,
  swms_recv_fn video_callback); //init struct with local port to use
int swms_Open(struct media_session *ms); //allocate audio_stack and bind it to port given
int swms_OpenSession(struct session *s);
int swms_Bind(struct media_session *ms, const char *remote_host, uint16_t audio_src_port, uint16_t audio_dst_port, uint16_t video_src_port,
  uint16_t video_dst_port);
int swms_BindSession(struct session *s, uint16_t src_port, uint16_t dst_port);
int swms_Start(struct media_session *ms); //Begin media transfer
int swms_Stop(struct media_session *ms);
int swms_Close(struct media_session *ms);

int swms_SetCallback(struct media_session *ms, enum CB_SIGNALS signal, signal_fn callback);

int swms_SendAudio(struct media_session *ms, void *data, uint16_t len);
int swms_SendVideo(struct media_session *ms, void *data, uint16_t len);
void __swms_RecvCallback(void *arg, struct udp_pcb *pcb, struct pbuf *p, const ip_addr_t *addr, uint16_t port);

#endif /* INC_MEDIA_SESSION_H_ */
