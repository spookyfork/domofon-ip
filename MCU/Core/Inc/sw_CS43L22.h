/*
 * sw_CS43L22.h
 *
 *  Created on: 08.11.2019
 *      Author: rand
 */

#ifndef SW_CS43L22_H_
#define SW_CS43L22_H_

#include "main.h"
#include <stdint.h>

/* Recommended Power-Up Sequence:
 * 1. Hold NRESET until power supplies are stable.
 * 2. Bring NRESET high.
 * 3. The default state of "Power Ctl.1" register (0x02) is 0x01. Load the desired register settings while keeping the "Power Ctl.1" register set to 0x01.
 * 4. Load the required initialization settings listed in Section 4.11
 * 5. Apply MCLK to the appropriate frequency, as discussed in Section 4.6. SCLK may be applied or set to master at any time; LRCK may only be applied or set to master
 * 		white the PDN bit is set to 1.
 * 6. Set the "Power Ctl.1" register (0x02) to 0x9E.
 * 7. Bring NRESET low if the analog or digital supplies drop below the recommended operating conditions to prevent power glitch related issues.
 */

/* Recommended Power-Down Sequence:
 * To minimize audible pops when turning off or placing DAC in stability,
 * 1. Mute DAC and PWM outputs.
 * 2. Disable soft ramp and zero cross volume transitions.
 * 3. Set the "Power Ctl.1" register (0x02) to 0x9F.
 * 4. Wait at least 100 us.
 * 5. MCLK may be removed at this point.
 * 6. To achieve the lowest operating quiescent current, bring NRESET low. All control port registers will be reset to their default state.
 */

/* Required Initialization Settings:
 * After power-up sequence step 3.
 * 1. Write 0x99 to register 0x00.
 * 2. Write 0x80 to register 0x47.
 * 3. Write '1'b to bit 7 in register 0x32.
 * 4. Write '0'b to bit 7 in register 0x32.
 * 5. Write 0x00 to register 0x00.
 */

typedef enum {
  CS43L22_22kHZ_16bit,
  CS43L22_44kHZ_16bit,
  CS43L22_48kHZ_16bit,
  CS43L22_22kHZ_24bit,
  CS43L22_44kHZ_24bit,
  CS43L22_48kHZ_24bit,
  CS43L22_22kHZ_32bit,
  CS43L22_44kHZ_32bit,
  CS43L22_48kHZ_32bit
} CS43L22_CONGIFURATION;

#define CS43L22_I2C_ADDRESS_R 0x95
#define CS43L22_I2C_ADDRESS_W 0x94

#define CS43L22_MAPBYTE_INC 0x80

#define CS43L22_REG_ID				0x01
#define CS43L22_REG_POWER_CTL1		0x02
#define CS43L22_REG_POWER_CTL2		0x04
#define CS43L22_REG_CLOCKING_CTL	0x05
#define CS43L22_REG_INTERFACE_CTL1	0x06
#define CS43L22_REG_INTERFACE_CTL2	0x07
#define CS43L22_REG_PASS_A_SEL		0x08
#define CS43L22_REG_PASS_B_SEL		0x09
#define CS43L22_REG_ANALOG_ZCSR		0x0A
#define CS43L22_REG_PASS_GANG		0x0C
#define CS43L22_REG_PLAYBACK_CTL1	0x0D
#define CS43L22_REG_MISC_CTL		0x0E
#define CS43L22_REG_PLAYBACK_CTL2	0x0F
#define CS43L22_REG_PASS_A_VOL		0x14
#define CS43L22_REG_PASS_B_VOL		0x15
#define CS43L22_REG_PCMA_VOL		0x1A
#define CS43L22_REG_PCMB_VOL		0x1B
#define CS43L22_REG_BEEP_FREQ		0x1C
#define CS43L22_REG_BEEP_VOL		0x1D
#define CS43L22_REG_BEEP_TONE_CFG	0x1E
#define CS43L22_REG_CONE_CTL		0x1F
#define CS43L22_REG_MASTER_A_VOL	0x20
#define CS43L22_REG_MASTER_B_VOL	0x21
#define CS43L22_REG_HEADPHONE_A_VOL	0x22
#define CS43L22_REG_HEADPHONE_B_VOL	0x23
#define CS43L22_REG_SPEAKER_A_VOL	0x24
#define CS43L22_REG_SPEAKER_B_VOL	0x25
#define CS43L22_REG_CHANNEL_MIX_SWP	0x26
#define CS43L22_REG_LIMIT_CTL_1		0x27
#define CS43L22_REG_LIMIT_CTL_2		0x28
#define CS43L22_REG_LIMITER_ATT_R	0x29
#define CS43L22_REG_OVERFLOW		0x2E
#define CS43L22_REG_BATTERY_COMP	0x2F
#define CS43L22_REG_VP_BATTERY_LVL	0x30
#define CS43L22_REG_SPEAKER_STATUS	0x31
#define CS43L22_REG_CHARGE_PUMP_F	0x34

// RESERVED: 0x03, 0x0B, 0x10, 0x11, 0x12, 0x13, 16, 17, 18, 19, 2a, 2b, 2c, 2d, 32, 33

uint8_t __sw_send_codec_ctrl(uint8_t controlBytes[], uint8_t numBytes);
uint8_t __sw_read_codec_register(uint8_t mapbyte, uint8_t *received_byte);

uint8_t sw_CS43L22_power_up();
uint8_t sw_CS43L22_init();
uint8_t sw_CS43L22_power_down();

uint8_t sw_CS43L22_mute();
uint8_t sw_CS43L22_unmute();
uint8_t sw_CS43L22_freeze();
uint8_t sw_CS43L22_unfreeze();

uint8_t sw_CS43L22_load_config(CS43L22_CONGIFURATION config);

uint8_t sw_CS43L22_read_chip_id(uint8_t *result);

#endif /* SW_CS43L22_H_ */
