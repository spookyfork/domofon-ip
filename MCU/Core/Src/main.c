/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 * Change I2C to Very High Frequency
 * Add pin to I2C
 * DCMI_PWR_EN_Pin GPIO Settings
 *
 */

/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "lwip.h"
#include "pdm2pcm.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <sys/time.h>
#include "stdio.h"
#include "sw_log.h"
#include "sw_sip2.h"
#include "sw_pds.h"
#include "media_session.h"
#include "sw_CS43L22.h"
#include "sw_ov9655.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define LOCAL_HOST_ADDRESS "192.168.1.20"
#define REMOTE_HOST_ADDRESS "192.168.1.11"
#define CONTENT_TYPE_PDS "application/pds"
#define LOCAL_PORT 5060
#define REMOTE_PORT 5060

#define PLAYBACK_BUFFER_SIZE 4096

#define MIN(x, y) (((x) < (y)) ? (x) : (y))

//Uncomment only one of the below
#define USE_MEDIA_SESSION //Create and use media session functions instead of raw lwip to send packets
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
CRC_HandleTypeDef hcrc;

I2C_HandleTypeDef hi2c1;

I2S_HandleTypeDef hi2s3;
DMA_HandleTypeDef hdma_spi3_tx;

RTC_HandleTypeDef hrtc;

SPI_HandleTypeDef hspi2;
DMA_HandleTypeDef hdma_spi2_rx;

TIM_HandleTypeDef htim10;
TIM_HandleTypeDef htim12;
TIM_HandleTypeDef htim13;

/* USER CODE BEGIN PV */

uint8_t callback_flag = 0;
uint32_t buttonPressTime = 0;
uint32_t btnWakeUpPressTIme = 0;
uint32_t btnCancelPressTime = 0;
uint32_t btnCallPressTime = 0;

/**** HAL AND PDM2PCM VARIABLES ****/
DCMI_HandleTypeDef hdcmi;
DMA_HandleTypeDef hdma_dcmi;
extern PDM_Filter_Handler_t PDM1_filter_handler;
extern PDM_Filter_Config_t PDM1_filter_config;

/**** AUDIO VARIABLES AND BUFFERS ****/
uint64_t PDM_buffer[SAMPLES_NUMBER + 1];
uint16_t PCM_buffer[SAMPLES_NUMBER + 1];
uint16_t audio_buffer[PLAYBACK_BUFFER_SIZE];
uint16_t offset = 0;
uint8_t current_playback_half = 1;

/**** VIDEO VARIABLES AND BUFFERS ****/
uint16_t image_data[IMAGE_BUFFER_SIZE + 2];
uint16_t image_offset = 0;
uint16_t part = 0;

/**** LWIP VARIABLES ****/
extern struct netif gnetif;
ip4_addr_t dst;
struct udp_pcb *udp_img;
struct udp_pcb *udp_stack;

/**** PDS VARIABLES ****/
struct pds_message pds;
struct pds_audio_header *sender_header, *receiver_header;
struct pds_video_header *video_header;
char *body;
int body_size;

/**** MEDIA SESSION ****/
struct media_session ms;

/**** SWSIP VARIABLES ****/
struct sw_call call;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_RTC_Init(void);
static void MX_I2C1_Init(void);
static void MX_I2S3_Init(void);
static void MX_SPI2_Init(void);
static void MX_CRC_Init(void);
static void MX_TIM10_Init(void);
static void MX_TIM12_Init(void);
static void MX_TIM13_Init(void);
/* USER CODE BEGIN PFP */
/**** OSIP SEND CALLBACK ****/
static void SW_DMA_Init(void);
static void SW_DCMI_Init(void);

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin);
/**** DMA CALLBACKS are in sw_callbacks.c ****/

/**** OSIP SEND CALLBACK ****/
int swsip_Udp_Send(osip_transaction_t *transaction, osip_message_t *msg, char *host, int port, int out_socket);
void swsip_Udp_Recv(void *arg, struct udp_pcb *pcb, struct pbuf *p, const ip_addr_t *addr, u16_t port);

/**** LWIP CALLBACKS -- TEMP ****/
void sw_audio_Recv(void *arg, struct udp_pcb *pcb, struct pbuf *p, const ip_addr_t *addr, u16_t port);

//TODO: Implement this -- HAL Integration
void sw_SwapAudioCamera();
void sw_FilterAndSend(uint16_t start, uint16_t end);
void sw_SendVideo(); //pack and send
void sw_PlaybackTransmit();

/**** MEDIA SESSION CALLBACKS (integration with bare metal modules) ****/
int swms_StartAudioInput_cb(int signal);
int swms_StartAudioOutput_cb(int signal);
int swms_StartVideoOutput_cb(int signal);
void swms_AudioRecv_cb(void *data, int len);
int swms_StopAudioInput_cb(int signal);
int swms_StopAudioOutput_cb(int signal);
int swms_StopVideoOutput_cb(int signal);

/**** SWSIP CALLBACKS ****/
int swsip_OnCallAccepted_cb(struct sw_call *call);
int swsip_OnCallRefused_cb(struct sw_call *call);
int swsip_OnCallTimeout_cb(struct sw_call *call);
int swsip_OnCallBye_cb(struct sw_call *call);
int swsip_OnMessageReceived_cb(struct sw_call *call, char *body, int length);

void invite_only(struct sw_call *call);
int initNetwork();
int closeNetwork();

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
static void SW_DCMI_Init(void)
{
  hdcmi.Instance = DCMI;
  hdcmi.Init.SynchroMode = DCMI_SYNCHRO_HARDWARE;
  hdcmi.Init.PCKPolarity = DCMI_PCKPOLARITY_RISING;
  hdcmi.Init.VSPolarity = DCMI_VSPOLARITY_HIGH;
  hdcmi.Init.HSPolarity = DCMI_HSPOLARITY_LOW;
  hdcmi.Init.CaptureRate = DCMI_CR_ALL_FRAME;
  hdcmi.Init.ExtendedDataMode = DCMI_EXTEND_DATA_8B;
  hdcmi.Init.JPEGMode = DCMI_JPEG_DISABLE;
  if (HAL_DCMI_Init(&hdcmi) != HAL_OK) {
    Error_Handler();
  }
}

static void SW_DMA_Init(void)
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA2_CLK_ENABLE();

  /* DMA2_Stream1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream1_IRQn);
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  uint32_t tick = HAL_GetTick();
  if (GPIO_Pin == BTN_WKUP_Pin && tick - buttonPressTime > 1000) {
    buttonPressTime = tick;
    callback_flag |= FLAG_SWAP;
  } else if (GPIO_Pin == BTN_CALL_Pin && tick - btnCallPressTime > 1000) {
    btnCallPressTime = tick;

    swms_Init(&ms, 6988, 6988, swms_AudioRecv_cb, 6900, -1, NULL);
    swms_Open(&ms);
    swms_SetCallback(&ms, MS_START_AUDIO_IN, &swms_StartAudioInput_cb);
    swms_SetCallback(&ms, MS_START_AUDIO_OUT, &swms_StartAudioOutput_cb);
    swms_SetCallback(&ms, MS_START_VIDEO_OUT, &swms_StartVideoOutput_cb);
    swms_SetCallback(&ms, MS_STOP_AUDIO_IN, &swms_StopAudioInput_cb);
    swms_SetCallback(&ms, MS_STOP_AUDIO_OUT, &swms_StopAudioOutput_cb);
    swms_SetCallback(&ms, MS_STOP_VIDEO_OUT, &swms_StopVideoOutput_cb);

    swsip_Call_with_body(&call, REMOTE_HOST_ADDRESS, REMOTE_PORT, CONTENT_TYPE_PDS, body, body_size);

  } else if (GPIO_Pin == BTN_CANCEL_Pin && tick - btnCancelPressTime > 1000) {
    btnCancelPressTime = tick;
    if (swsip_Cancel(&call) == SWSIP_WRONG_STATE) {
      swsip_Hangup(&call);
    }
    swms_Stop(&ms);
    swms_Close(&ms);
  }

}

/**
 * Callback for osip to send data using lwip
 * @param transaction
 * @param msg
 * @param host
 * @param port
 * @param out_socket
 * @return
 */
int swsip_Udp_Send(osip_transaction_t *transaction, osip_message_t *msg, char *host, int port, int out_socket)
{
  //make message into string
  char *str_msg = NULL;
  size_t size;
  int funcResult = 0;
  osip_message_to_str(msg, &str_msg, &size);

  //send it
  //Create pbuf
  struct pbuf *p = pbuf_alloc(PBUF_TRANSPORT, size, PBUF_RAM);
  memcpy((char*) (p->payload), str_msg, size);

  //pass it to lwip
  ipaddr_aton(host, &dst);
  udp_sendto(udp_stack, p, &dst, port);

  //free pbuf
  pbuf_free(p);
  //free osip string
  osip_free(str_msg);
  return funcResult;
}

/**
 * TEMPORARY lwip callback for receiving audio over UDP
 * audio should be in stereo
 * @param arg
 * @param pcb
 * @param p
 * @param addr
 * @param port
 */
void sw_audio_Recv(void *arg, struct udp_pcb *pcb, struct pbuf *p, const ip_addr_t *addr, u16_t port)
{
  if (!p) {
    return;
  }
  memcpy(&audio_buffer[offset], p->payload, p->len);
  offset = (offset + p->len / 2) % PLAYBACK_BUFFER_SIZE;
  pbuf_free(p);
  return;
}

void sw_SwapAudioCamera()
{
  if (callback_flag & FLAG_DCMI_ON) {
    HAL_DCMI_Stop(&hdcmi);
    HAL_GPIO_TogglePin(DCMI_PWR_EN_GPIO_Port, DCMI_PWR_EN_Pin);

    HAL_DCMI_DeInit(&hdcmi);
    HAL_I2C_Init(&hi2c1);
    sw_CS43L22_unmute();
    HAL_I2C_DeInit(&hi2c1);

    HAL_I2S_MspInit(&hi2s3); //TODO: change only pins without doint DMA init
    sw_PlaybackTransmit();
  } else {
    HAL_I2C_Init(&hi2c1);
    sw_CS43L22_mute();
    HAL_I2C_DeInit(&hi2c1);
    HAL_DCMI_Init(&hdcmi);

    //HAL_DCMI_MspInit(&hdcmi); //TODO: change only pins without doint DMA init
    part = 0;
    HAL_GPIO_TogglePin(DCMI_PWR_EN_GPIO_Port, DCMI_PWR_EN_Pin);
    HAL_DCMI_Start_DMA(&hdcmi, DCMI_MODE_CONTINUOUS, (uint32_t) &image_data[1], IMAGE_BUFFER_SIZE / 2);
  }
  callback_flag = (callback_flag & ~FLAG_DCMI_ON) | (~callback_flag & FLAG_DCMI_ON);
}

void sw_FilterAndSend(uint16_t start, uint16_t end)
{
  int i = start;
  while (i < end) {
    PDM_Filter(&PDM_buffer[i], &PCM_buffer[i], &PDM1_filter_handler);
    //XXX Uncomment this and comment next 4 lines
    swms_SendAudio(&ms, &PCM_buffer[i], PDM1_filter_config.output_samples_number * 2);
    i += PDM1_filter_config.output_samples_number;
    //MX_LWIP_Process();
  }

}

void sw_SendVideo()
{
  int line_length = 640;
  int packet_size = line_length * 2;
  image_data[image_offset] = part++;
  //XXX Uncomment this and comment next 4 lines
  swms_SendVideo(&ms, &image_data[image_offset], packet_size + 2);
  image_offset += line_length;
  if (image_offset >= IMAGE_BUFFER_SIZE) {
    image_offset = 0;
  }
}

void sw_PlaybackTransmit()
{
  HAL_I2S_Transmit_DMA(&hi2s3, &audio_buffer[current_playback_half * PLAYBACK_BUFFER_SIZE / 2],
      offset - (current_playback_half * PLAYBACK_BUFFER_SIZE / 2) > 0 ? offset - (current_playback_half * PLAYBACK_BUFFER_SIZE / 2) : PLAYBACK_BUFFER_SIZE / 2);
  current_playback_half = !current_playback_half;
  offset = current_playback_half * PLAYBACK_BUFFER_SIZE / 2;
}

int swms_StartAudioInput_cb(int signal)
{
  //Get first samples from the microphone
  HAL_SPI_Receive_DMA(&hspi2, (uint8_t*) &PDM_buffer[0], SAMPLES_NUMBER * sizeof(int64_t) / sizeof(int16_t));

  return SWMS_OK;
}

int swms_StartAudioOutput_cb(int signal)
{
  //XXX DMA is in Normal Mode so we dont need to do anything here
  //Also since we start with Video transmission by default, playback will be turned on only when push to talk is pressed

  return SWMS_OK;
}

int swms_StartVideoOutput_cb(int signal)
{
  //Get image from the camera
  HAL_DCMI_DisableCrop(&hdcmi);
  HAL_StatusTypeDef res = HAL_DCMI_Start_DMA(&hdcmi, DCMI_MODE_CONTINUOUS, (uint32_t) &image_data[1], IMAGE_BUFFER_SIZE / 2);
  if (res != HAL_OK)
    HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, GPIO_PIN_SET);
  return SWMS_OK;
}

uint16_t audio_data_received = 0;
uint8_t timer_state = 0;

/**
 * Receive callback for audio data
 * Data needs to be in stereo, 16bit samples, frequency the same as I2S3 frequency, Little Endian
 */
void swms_AudioRecv_cb(void *data, int len)
{
  memcpy(&audio_buffer[offset], data, len);
  offset = (offset + len / 2);
  if (offset >= PLAYBACK_BUFFER_SIZE)
    offset = 0;

  /* XXX:Automatic DCMI/I2S3 swap
   *
   * After receiving data start a timer (1 sec ?).
   * If within that time we receive enough data (1/2 of buffer?) automatically swap to audio and stop the timer.
   * If not we do nothing.
   * For instance: if we get only 100ms worth of audio in 1 sec then it is not feasible to play that audio.
   *
   * After the swap start another timer (1 sec?).
   * Every time we receive an audio packet we reset the timer
   * If we don't receive data for 1sec the timer fires PeriodElapsed callback and we swap back to Camera.
   *
   * This will only work if the PC client uses push-to-talk functionality or if there is audio detection (velocity is above threshold).
   * If it floods MCU with audio packets (like MCU does now) the Camera will never work.
   *
   */

  if (callback_flag & FLAG_DCMI_ON) {
    //If we are now using the camera
    if (htim13.State == HAL_TIM_STATE_READY) {      //XXX Those states may be wrong
      //First time we receive data
      HAL_TIM_Base_Start_IT(&htim13);
      audio_data_received = len;

      timer_state = 1;
    } else if (htim13.State == HAL_TIM_STATE_BUSY) {      //XXX Those states may be wrong
      //We already started a timer (and got our first data)
      audio_data_received += len;
      if (audio_data_received >= PLAYBACK_BUFFER_SIZE >> 1) {
        //We have enough data to start swap and start playback - this is done in main loop
        callback_flag |= FLAG_SWAP;
        HAL_GPIO_WritePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin, GPIO_PIN_SET);
        //Reset the timer (we use the same one to decide if we need to swap back to Camera)
        htim13.Instance->CNT = 0;
      }
    }
  } else {
    //If we are currently using audio output and NOT the camera
    //Every time we get new data we need to reset the timer -- Timer should be working right now
    htim13.Instance->CNT = 0;
  }
}

int swms_StopAudioInput_cb(int signal)
{
  HAL_SPI_DMAStop(&hspi2);
  return SWMS_OK;
}

int swms_StopAudioOutput_cb(int signal)
{
  //XXX Video is on by default
  return SWMS_OK;
}

int swms_StopVideoOutput_cb(int signal)
{
  HAL_DCMI_Stop(&hdcmi);
  return SWMS_OK;
}

struct video_connection {
  uint16_t src_port;
  uint16_t recv_port;
  uint16_t remote_src_port;
  uint16_t remote_dst_port;

  uint16_t packet_size;
  uint16_t width;
  uint16_t height;
  struct udp_pcb *stack;
} vc;

/**
 * Callback for lwip that feeds sip messages to osip
 * @param arg
 * @param pcb
 * @param p
 * @param addr
 * @param port
 */
void swsip_Udp_Recv(void *arg, struct udp_pcb *pcb, struct pbuf *p, const ip_addr_t *addr, u16_t port)
{
  int funcResult = 0;
  //arg: user-provided argument (struct sw_call)
  struct sw_call *call = (struct sw_call*) arg;
  if (!p) {
    //Connection closed or something ?
    return;
  }
  //pass it to the call for parsing
  if ((funcResult = swsip_ParseMessage(call, p->payload, p->len)) != 0) {
    LOG(LOG_WARNING, "Error Parsing message: %d", funcResult);
  }

  //Free the pbuf
  pbuf_free(p);
  return;
}

int swsip_OnCallAccepted_cb(struct sw_call *call)
{
  if (!call->last_pds) {
    return SWSIP_OK;
  }
  int a_src_port, a_dst_port, v_src_port, v_dst_port;
  a_src_port = a_dst_port = v_src_port = v_dst_port = 0;

  //Get Audio Ports
  struct pds_audio_header *audio_match;
  struct pds_video_header *video_match;

  //Get Response for the format we are using to send audio out
  if (pds_match_audio_format(call->last_pds, sender_header, &audio_match) == PDS_OK) {
    if (audio_match != NULL)
      a_dst_port = audio_match->dst_port;
  }

  //Get response for the format we are usign to
  if (pds_match_audio_format(call->last_pds, receiver_header, &audio_match) == PDS_OK) {
    if (audio_match != NULL)
      a_src_port = audio_match->src_port;
  }

  if (pds_match_video_format(call->last_pds, video_header, &video_match) == PDS_OK) {
    if (video_match != NULL)
      v_dst_port = video_match->dst_port;
  }
  swms_Bind(&ms, REMOTE_HOST_ADDRESS, a_src_port, a_dst_port, v_src_port, v_dst_port);
  swms_Start(&ms);

  return SWSIP_OK;
}

int swsip_OnCallRefused_cb(struct sw_call *call)
{
  swms_Close(&ms);
  return SWSIP_OK;
}

int swsip_OnCallTimeout_cb(struct sw_call *call)
{
  swms_Close(&ms);
  return SWSIP_OK;
}

int swsip_OnCallBye_cb(struct sw_call *call)
{
  swms_Stop(&ms);
//  HAL_SPI_DMAStop(&hspi2);
//  HAL_I2S_DMAStop(&hi2s3);
//  HAL_DCMI_Stop(&hdcmi);
  swms_Close(&ms);
  return SWSIP_OK;
}

//Result is the SIP response code that should be sent
int swsip_OnMessageReceived_cb(struct sw_call *call, char *body, int length)
{
  struct pds_message msg;

  //XXX we should check if this is correct call here and respond with 403 Forbidden if not

  if (pds_parse(body, &msg) == PDS_OK) {
    switch (msg.gate) {
    case PDS_GATE_OPEN:
      //Open Gate
      //XXX: Should do some sort of check if the gate is actually open
      HAL_GPIO_WritePin(LED_ORANGE_GPIO_Port, LED_ORANGE_Pin, GPIO_PIN_SET);
      __HAL_TIM_CLEAR_FLAG(&htim10, TIM_SR_UIF); //Update interrupt Flag register needs to be reset to avoid immediate interrupt after starting
      htim10.Instance->CNT = 0;
      HAL_TIM_Base_Start_IT(&htim10);
      //Start Buzzer - no interrupt
      HAL_TIM_PWM_Start(&htim12, TIM_CHANNEL_1);

      break;
    case PDS_GATE_CLOSE:
      //XXX: Gate button released code goes here
      //We open the gate for 5 seconds every time the button is pressed
      //but we could make it so that if the button is pressed for shorter than 5 seconds
      //the gate opens for 5 seconds
      //and if it is pressed for longer then it stays open for as long as the button is pressed.
      //This however would require additional error checking. For instance, upper threshold after which it closes anyway.
      break;
    case PDS_GATE_ERROR:
      //XXX: Error handling here
      break;
    default:
      break;
    }

    return SIP_OK;
  } else {
    return SIP_BAD_REQUEST;
  }
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  //GATE TIMER
  if (htim->Instance == TIM10) {
    HAL_GPIO_WritePin(LED_ORANGE_GPIO_Port, LED_ORANGE_Pin, GPIO_PIN_RESET);
    HAL_TIM_Base_Stop_IT(&htim10);
    HAL_TIM_PWM_Stop(&htim12, TIM_CHANNEL_1);
    //AUDIO/CAMERA swap timer
  } else if (htim->Instance == TIM13) {
    HAL_GPIO_WritePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin, GPIO_PIN_RESET);
    //We did not receive audio packets for 1sec we need to swap back to Camera
    if (!(callback_flag & FLAG_DCMI_ON))
      callback_flag |= FLAG_SWAP;
    //and stop the timer
    HAL_TIM_Base_Stop_IT(&htim13);
  }
}

void media_recv(void *buffer, int len)
{
  //Format is in mono
  //TODO: Maybe offload this to the other side and offer separate audio formats (requires updating the app)
  for (int i = 0; i < MIN(len / 2, (PLAYBACK_BUFFER_SIZE - offset)/2); i += 1) {
    audio_buffer[offset + i * 2] = ((uint16_t*) buffer)[i];
  }
  //memcpy(&audio_buffer[offset], buffer, MIN(len, PLAYBACK_BUFFER_SIZE - offset));
  offset = (offset + len);
  if (offset >= PLAYBACK_BUFFER_SIZE)
    offset = 0;
  //HAL_I2S_Transmit_DMA(&hi2s3, playback, len / 2);
}

void invite_only(struct sw_call *call)
{
  initNetwork();

  //Set up media session
  swms_Init(&ms, 6988, 6988, swms_AudioRecv_cb, 6900, -1, NULL);
  swms_Open(&ms);
  swms_SetCallback(&ms, MS_START_AUDIO_IN, &swms_StartAudioInput_cb);
  swms_SetCallback(&ms, MS_START_AUDIO_OUT, &swms_StartAudioOutput_cb);
  swms_SetCallback(&ms, MS_START_VIDEO_OUT, &swms_StartVideoOutput_cb);
  swms_SetCallback(&ms, MS_STOP_AUDIO_IN, &swms_StopAudioInput_cb);
  swms_SetCallback(&ms, MS_STOP_AUDIO_OUT, &swms_StopAudioOutput_cb);
  swms_SetCallback(&ms, MS_STOP_VIDEO_OUT, &swms_StopVideoOutput_cb);

  //Create pds message
  pds_init_message(&pds);
  pds_alloc_audio_header(&sender_header, ms.audio.src_port, 0, 10250, 16, 1, PDS_AUDIO_SIGNED);
  pds_alloc_audio_header(&receiver_header, 0, ms.audio.recv_port, 8000, 16, 2, PDS_AUDIO_SIGNED);
  pds_alloc_video_header(&video_header, ms.video.src_port, 0, 640, 480, 1280, PDS_VIDEO_565RGB);
  pds_add_audio_header(&pds, sender_header);
  pds_add_audio_header(&pds, receiver_header);
  pds_add_video_header(&pds, video_header);

  //swms_Bind(&ms, "192.168.1.11", 6987, 6988, 0, 6900); //6987 MUST BE THE SOURCE PORT OF THE INCOMING AUDIO

  callback_flag |= FLAG_DCMI_ON;

  pds_to_str(&pds, &body, &body_size);
  body_size--; //ignore the string termination byte

  swsip_Call_with_body(call, REMOTE_HOST_ADDRESS, REMOTE_PORT, CONTENT_TYPE_PDS, body, body_size);
  //free(body);

  while (1) {
    if (callback_flag & FLAG_RX_HALFCPLT) {
      callback_flag &= ~FLAG_RX_HALFCPLT;
      // Filter first half
      sw_FilterAndSend(0, SAMPLES_NUMBER / 2);
    }
    if (callback_flag & FLAG_RX_CPLT) {
      callback_flag &= ~FLAG_RX_CPLT;
      //Filter second half
      sw_FilterAndSend(SAMPLES_NUMBER / 2, SAMPLES_NUMBER);
    }
    //Finished Playback and the camera is not working now
    if ((callback_flag & FLAG_TX_COMPLETE) && !(callback_flag & FLAG_DCMI_ON)) {
      callback_flag &= ~FLAG_TX_COMPLETE;
      //Transmit Playback Data
      sw_PlaybackTransmit();
    }
    if (callback_flag & FLAG_LINE_EVENT) {
      callback_flag &= ~FLAG_LINE_EVENT;
      sw_SendVideo();
    }
    if (callback_flag & FLAG_VSYNC_EVENT) {
      callback_flag &= ~FLAG_VSYNC_EVENT;
      //vsyncEvent = 0;
      sw_SendVideo();
      part = 0;
    }
    if (callback_flag & FLAG_SWAP) {
      callback_flag &= ~FLAG_SWAP;
      sw_SwapAudioCamera();
      sw_PlaybackTransmit();
    }
    swsip_Update(call);
    MX_LWIP_Process();
  }
}

int initNetwork()
{
  int error = 0;

  udp_stack = udp_new();

  ipaddr_aton("192.168.1.11", &dst);
  while (!netif_is_up(&gnetif) || dhcp_supplied_address(&gnetif) != 1) {
    MX_LWIP_Process();
    HAL_Delay(100);
  }
  LOG(LOG_DEBUG, "Initialised.");

  //Bind
  if ((error = udp_bind(udp_stack, IP4_ADDR_ANY, 5060)) != ERR_OK) {
    LOG(LOG_CRITICAL, "Bind failed with error code : %d", error);
    return error;
  }
  char *addr_str = ipaddr_ntoa(&(gnetif.ip_addr));
  LOG(LOG_DEBUG, "Bind done on address %s, port %d", addr_str, LOCAL_PORT);

  //Set recv callback
  udp_recv(udp_stack, &swsip_Udp_Recv, &call);

  return 0;
}

int closeNetwork()
{

  int errorCode = 0;

  udp_remove(udp_stack);

  return errorCode;
}
//XXX: remove thisfunction
void startRawTransmission()
{
  //UDP Stack for image sending
  udp_img = udp_new();
  udp_bind(udp_img, IP4_ADDR_ANY, 6900);

  //Udp Stack for audio recv
  udp_stack = udp_new();
  udp_bind(udp_stack, IP4_ADDR_ANY, 6988);

  ipaddr_aton("192.168.1.11", &dst);
  while (!netif_is_up(&gnetif) || dhcp_supplied_address(&gnetif) != 1) {
    MX_LWIP_Process();
    HAL_Delay(100);
  }
  //Set recv callback
  udp_recv(udp_stack, &sw_audio_Recv, NULL);

  //Get first samples from the microphone
  HAL_SPI_Receive_DMA(&hspi2, (uint8_t*) &PDM_buffer[0], SAMPLES_NUMBER * sizeof(int64_t) / sizeof(int16_t));
  //Get image from the camera
  HAL_DCMI_DisableCrop(&hdcmi);
  HAL_DCMI_Start_DMA(&hdcmi, DCMI_MODE_CONTINUOUS, (uint32_t) &image_data[1], IMAGE_BUFFER_SIZE / 2);
  callback_flag |= FLAG_DCMI_ON;

  while (1) {
    if (callback_flag & FLAG_RX_HALFCPLT) {
      callback_flag &= ~FLAG_RX_HALFCPLT;
      // Filter first half
      sw_FilterAndSend(0, SAMPLES_NUMBER / 2);
    }
    if (callback_flag & FLAG_RX_CPLT) {
      callback_flag &= ~FLAG_RX_CPLT;
      //Filter second half
      sw_FilterAndSend(SAMPLES_NUMBER / 2, SAMPLES_NUMBER);
    }
    //Finished Playback and the camera is not working now
    if ((callback_flag & FLAG_TX_COMPLETE) && !(callback_flag & FLAG_DCMI_ON)) {
      callback_flag &= ~FLAG_TX_COMPLETE;
      //Transmit Playback Data
      sw_PlaybackTransmit();
    }
    if (callback_flag & FLAG_LINE_EVENT) {
      callback_flag &= ~FLAG_LINE_EVENT;
      sw_SendVideo();
    }
    if (callback_flag & FLAG_VSYNC_EVENT) {
      callback_flag &= ~FLAG_VSYNC_EVENT;
      //vsyncEvent = 0;
      sw_SendVideo();
      part = 0;
    }
    if (callback_flag & FLAG_SWAP) {
      callback_flag &= ~FLAG_SWAP;
      sw_SwapAudioCamera();
      sw_PlaybackTransmit();
    }
    MX_LWIP_Process();
  }
}

void startMediaSession()
{
  ipaddr_aton("192.168.1.11", &dst);
  while (!netif_is_up(&gnetif) || dhcp_supplied_address(&gnetif) != 1) {
    MX_LWIP_Process();
    HAL_Delay(100);
  }

  swms_Init(&ms, 6988, 6988, swms_AudioRecv_cb, 6900, -1, NULL);
  swms_Open(&ms);
  swms_SetCallback(&ms, MS_START_AUDIO_IN, &swms_StartAudioInput_cb);
  swms_SetCallback(&ms, MS_START_AUDIO_OUT, &swms_StartAudioOutput_cb);
  swms_SetCallback(&ms, MS_START_VIDEO_OUT, &swms_StartVideoOutput_cb);
  swms_SetCallback(&ms, MS_STOP_AUDIO_IN, &swms_StopAudioInput_cb);
  swms_SetCallback(&ms, MS_STOP_AUDIO_OUT, &swms_StopAudioOutput_cb);
  swms_SetCallback(&ms, MS_STOP_VIDEO_OUT, &swms_StopVideoOutput_cb);

  swms_Bind(&ms, "192.168.1.11", 6987, 6988, 0, 6900); //6987 MUST BE THE SOURCE PORT OF THE INCOMING AUDIO

  callback_flag |= FLAG_DCMI_ON;
  swms_Start(&ms);

  while (1) {
    if (callback_flag & FLAG_RX_HALFCPLT) {
      callback_flag &= ~FLAG_RX_HALFCPLT;
      // Filter first half
      sw_FilterAndSend(0, SAMPLES_NUMBER / 2);
    }
    if (callback_flag & FLAG_RX_CPLT) {
      callback_flag &= ~FLAG_RX_CPLT;
      //Filter second half
      sw_FilterAndSend(SAMPLES_NUMBER / 2, SAMPLES_NUMBER);
    }
    //Finished Playback and the camera is not working now
    if ((callback_flag & FLAG_TX_COMPLETE) && !(callback_flag & FLAG_DCMI_ON)) {
      callback_flag &= ~FLAG_TX_COMPLETE;
      //Transmit Playback Data
      sw_PlaybackTransmit();
    }
    if (callback_flag & FLAG_LINE_EVENT) {
      callback_flag &= ~FLAG_LINE_EVENT;
      sw_SendVideo();
    }
    if (callback_flag & FLAG_VSYNC_EVENT) {
      callback_flag &= ~FLAG_VSYNC_EVENT;
      //vsyncEvent = 0;
      sw_SendVideo();
      part = 0;
    }
    if (callback_flag & FLAG_SWAP) {
      callback_flag &= ~FLAG_SWAP;
      sw_SwapAudioCamera();
      sw_PlaybackTransmit();
    }
    MX_LWIP_Process();
  }
}

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_LWIP_Init();
  MX_RTC_Init();
  MX_I2C1_Init();
  MX_I2S3_Init();
  MX_SPI2_Init();
  MX_CRC_Init();
  MX_PDM2PCM_Init();
  MX_TIM10_Init();
  MX_TIM12_Init();
  MX_TIM13_Init();
  /* USER CODE BEGIN 2 */

  SW_DMA_Init();

  CRC->CR = CRC_CR_RESET;

  if (sw_CS43L22_power_up() != 0x00)
    HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, GPIO_PIN_SET);
  sw_CS43L22_mute();
  sw_CS43L22_unmute();
  sw_CS43L22_mute();

  HAL_GPIO_WritePin(DCMI_RST_GPIO_Port, DCMI_RST_Pin, GPIO_PIN_RESET);
  HAL_Delay(10);
  HAL_GPIO_WritePin(DCMI_RST_GPIO_Port, DCMI_RST_Pin, GPIO_PIN_SET);

  HAL_GPIO_WritePin(DCMI_PWR_EN_GPIO_Port, DCMI_PWR_EN_Pin, GPIO_PIN_RESET);
  HAL_Delay(100);

  if (HAL_OK != sw_ov9655_init_qvga())
    HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, GPIO_PIN_SET);

  //Swapping to DCMI For now
  HAL_I2C_DeInit(&hi2c1);
  SW_DCMI_Init();

  //We now have Camera and Audio Output read with pins set for camera
  swsip_Init(&call, "192.168.1.20", 5060, &swsip_Udp_Send);

  __HAL_TIM_CLEAR_FLAG(&htim10, TIM_SR_UIF);
  __HAL_TIM_CLEAR_FLAG(&htim12, TIM_SR_UIF);
  __HAL_TIM_CLEAR_FLAG(&htim13, TIM_SR_UIF);

  invite_only(&call);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1) {

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
  RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = { 0 };

  /** Configure the main internal regulator output voltage
   */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
   * in the RCC_OscInitTypeDef structure.
   */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI | RCC_OSCILLATORTYPE_LSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
   */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK) {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_I2S | RCC_PERIPHCLK_RTC;
  PeriphClkInitStruct.PLLI2S.PLLI2SN = 170;
  PeriphClkInitStruct.PLLI2S.PLLI2SR = 2;
  PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK) {
    Error_Handler();
  }
}

/**
 * @brief CRC Initialization Function
 * @param None
 * @retval None
 */
static void MX_CRC_Init(void)
{

  /* USER CODE BEGIN CRC_Init 0 */

  /* USER CODE END CRC_Init 0 */

  /* USER CODE BEGIN CRC_Init 1 */

  /* USER CODE END CRC_Init 1 */
  hcrc.Instance = CRC;
  if (HAL_CRC_Init(&hcrc) != HAL_OK) {
    Error_Handler();
  }
  __HAL_CRC_DR_RESET(&hcrc);
  /* USER CODE BEGIN CRC_Init 2 */

  /* USER CODE END CRC_Init 2 */

}

/**
 * @brief I2C1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK) {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
 * @brief I2S3 Initialization Function
 * @param None
 * @retval None
 */
static void MX_I2S3_Init(void)
{

  /* USER CODE BEGIN I2S3_Init 0 */

  /* USER CODE END I2S3_Init 0 */

  /* USER CODE BEGIN I2S3_Init 1 */

  /* USER CODE END I2S3_Init 1 */
  hi2s3.Instance = SPI3;
  hi2s3.Init.Mode = I2S_MODE_MASTER_TX;
  hi2s3.Init.Standard = I2S_STANDARD_PHILIPS;
  hi2s3.Init.DataFormat = I2S_DATAFORMAT_16B;
  hi2s3.Init.MCLKOutput = I2S_MCLKOUTPUT_ENABLE;
  hi2s3.Init.AudioFreq = I2S_AUDIOFREQ_8K;
  hi2s3.Init.CPOL = I2S_CPOL_LOW;
  hi2s3.Init.ClockSource = I2S_CLOCK_PLL;
  hi2s3.Init.FullDuplexMode = I2S_FULLDUPLEXMODE_DISABLE;
  if (HAL_I2S_Init(&hi2s3) != HAL_OK) {
    Error_Handler();
  }
  /* USER CODE BEGIN I2S3_Init 2 */

  /* USER CODE END I2S3_Init 2 */

}

/**
 * @brief RTC Initialization Function
 * @param None
 * @retval None
 */
static void MX_RTC_Init(void)
{

  /* USER CODE BEGIN RTC_Init 0 */

  /* USER CODE END RTC_Init 0 */

  RTC_TimeTypeDef sTime = { 0 };
  RTC_DateTypeDef sDate = { 0 };

  /* USER CODE BEGIN RTC_Init 1 */

  /* USER CODE END RTC_Init 1 */
  /** Initialize RTC Only
   */
  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 255;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if (HAL_RTC_Init(&hrtc) != HAL_OK) {
    Error_Handler();
  }

  /* USER CODE BEGIN Check_RTC_BKUP */

  /* USER CODE END Check_RTC_BKUP */

  /** Initialize RTC and set the Time and Date
   */
  sTime.Hours = 0x0;
  sTime.Minutes = 0x0;
  sTime.Seconds = 0x0;
  sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sTime.StoreOperation = RTC_STOREOPERATION_RESET;
  if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BCD) != HAL_OK) {
    Error_Handler();
  }
  sDate.WeekDay = RTC_WEEKDAY_MONDAY;
  sDate.Month = RTC_MONTH_JANUARY;
  sDate.Date = 0x1;
  sDate.Year = 0x0;

  if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BCD) != HAL_OK) {
    Error_Handler();
  }
  /* USER CODE BEGIN RTC_Init 2 */

  /* USER CODE END RTC_Init 2 */

}

/**
 * @brief SPI2 Initialization Function
 * @param None
 * @retval None
 */
static void MX_SPI2_Init(void)
{

  /* USER CODE BEGIN SPI2_Init 0 */

  /* USER CODE END SPI2_Init 0 */

  /* USER CODE BEGIN SPI2_Init 1 */

  /* USER CODE END SPI2_Init 1 */
  /* SPI2 parameter configuration*/
  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_1LINE;
  hspi2.Init.DataSize = SPI_DATASIZE_16BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi2.Init.NSS = SPI_NSS_SOFT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_64;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi2) != HAL_OK) {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI2_Init 2 */

  /* USER CODE END SPI2_Init 2 */

}

/**
 * @brief TIM10 Initialization Function
 * @param None
 * @retval None
 */
static void MX_TIM10_Init(void)
{

  /* USER CODE BEGIN TIM10_Init 0 */

  /* USER CODE END TIM10_Init 0 */

  /* USER CODE BEGIN TIM10_Init 1 */

  /* USER CODE END TIM10_Init 1 */
  htim10.Instance = TIM10;
  htim10.Init.Prescaler = 41999;
  htim10.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim10.Init.Period = 19999;
  htim10.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim10.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim10) != HAL_OK) {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM10_Init 2 */

  /* USER CODE END TIM10_Init 2 */

}

/**
 * @brief TIM12 Initialization Function
 * @param None
 * @retval None
 */
static void MX_TIM12_Init(void)
{

  /* USER CODE BEGIN TIM12_Init 0 */

  /* USER CODE END TIM12_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = { 0 };
  TIM_OC_InitTypeDef sConfigOC = { 0 };

  /* USER CODE BEGIN TIM12_Init 1 */

  /* USER CODE END TIM12_Init 1 */
  htim12.Instance = TIM12;
  htim12.Init.Prescaler = 41999;
  htim12.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim12.Init.Period = 19;
  htim12.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim12.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim12) != HAL_OK) {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim12, &sClockSourceConfig) != HAL_OK) {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim12) != HAL_OK) {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 9;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim12, &sConfigOC, TIM_CHANNEL_1) != HAL_OK) {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM12_Init 2 */

  /* USER CODE END TIM12_Init 2 */
  HAL_TIM_MspPostInit(&htim12);

}

/**
 * @brief TIM13 Initialization Function
 * @param None
 * @retval None
 */
static void MX_TIM13_Init(void)
{

  /* USER CODE BEGIN TIM13_Init 0 */

  /* USER CODE END TIM13_Init 0 */

  /* USER CODE BEGIN TIM13_Init 1 */

  /* USER CODE END TIM13_Init 1 */
  htim13.Instance = TIM13;
  htim13.Init.Prescaler = 41999;
  htim13.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim13.Init.Period = 3999;
  htim13.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim13.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim13) != HAL_OK) {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM13_Init 2 */

  /* USER CODE END TIM13_Init 2 */

}

/**
 * Enable DMA controller clock
 */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Stream3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream3_IRQn);
  /* DMA1_Stream5_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream5_IRQn);

}

/**
 * @brief GPIO Initialization Function
 * @param None
 * @retval None
 */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = { 0 };

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(ETH_NRST_GPIO_Port, ETH_NRST_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, DCMI_RST_Pin | LED_ORANGE_Pin | LED_RED_Pin | LED_BLUE_Pin | Audio_RST_Pin | DCMI_PWR_EN_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : ETH_NRST_Pin */
  GPIO_InitStruct.Pin = ETH_NRST_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(ETH_NRST_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PE4 PE5 PE6 PE0
   PE1 */
  GPIO_InitStruct.Pin = GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_0 | GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF13_DCMI;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pin : BTN_WKUP_Pin */
  GPIO_InitStruct.Pin = BTN_WKUP_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(BTN_WKUP_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PA6 */
  GPIO_InitStruct.Pin = GPIO_PIN_6;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF13_DCMI;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : BTN_CALL_Pin BTN_CANCEL_Pin */
  GPIO_InitStruct.Pin = BTN_CALL_Pin | BTN_CANCEL_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pins : DCMI_RST_Pin LED_ORANGE_Pin LED_RED_Pin LED_BLUE_Pin
   Audio_RST_Pin DCMI_PWR_EN_Pin */
  GPIO_InitStruct.Pin = DCMI_RST_Pin | LED_ORANGE_Pin | LED_RED_Pin | LED_BLUE_Pin | Audio_RST_Pin | DCMI_PWR_EN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : PC6 */
  GPIO_InitStruct.Pin = GPIO_PIN_6;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF13_DCMI;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : PB7 */
  GPIO_InitStruct.Pin = GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF13_DCMI;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);

  HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
