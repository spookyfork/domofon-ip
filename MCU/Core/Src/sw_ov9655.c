/*
 * sw_ov9655.c
 *
 *  Created on: Oct 26, 2020
 *      Author: rand
 *
 * The OV9655/OV9155 sensor has an active image array of
 * 1300 columns by 1028 rows (1,336,400 pixels)
 *
 *
 * Scaling Image Output
 * The OV9655/OV9155 is capable of scaling down the
 * image size from VGA to 40x30. By using register bits
 * COM14[1] (0x3E), COM16[0] (0x41), and registers
 * POIDX (0x72), XINDX (0x74), and YINDX (0x75), the user
 * can output the desired image size. At certain image sizes,
 * HREF is not consistent in a frame.
 *
 *  The device slave addresses are 60 for write and 61 for read.
 *
 */
#include "main.h"
#include "sw_ov9655.h"

/* @formatter:off */

//Only the ones that are not default
uint8_t OV9655_short1[][3] = {

    /* Common Control 3
     * Bit[7]: Color bar output
     * Bit[6]: Output data MSB and LSB swap
     * Bit[5:4]: Reserved
     * Bit[3]: Pin selection
     * 1: Change RESETB pin to EXPST_B (frame exposure
     * mode timing) and change PWDN pin to FREX
     * (frame exposure enable)
     * NOTE: This is for REV5 of the OV9655 sensor. For previous REV of
     * the sensor, RESETB pin is RESET (high for reset).
     * Bit[2]: RGB 565 format option
     * 0: RGB 565 format
     * 1: Output data average
     * Bit[1]: Reserved
     * Bit[0]: Single frame output (used for Frame Exposure mode
     * only)
     */
    { OV9655_REG_COM3 , 0b10000000, 0b11001101 }, //XXX default is 0x00
    /*
     * Common Control 7
     * Bit[7]: SCCB register reset
     * 0: No change
     * 1: Resets all registers to default values
     * Bit[6:4]: Format control
     * 000: Full resolution or 15fps VGA
     * 110: 30fps VGA with VarioPixel ®
     * Bit[3:2]: Reserved
     * Bit[1:0]: Output format selection
     * 00: Raw RGB data
     * 01: Raw RGB interpolation
     * 10: YUV
     * 11: RGB
     */
    { OV9655_REG_COM7 , 0b00000011, 0b11110011 }, //XXX DEFAULT IS 0x02
    /*
     * Common Control 15
     * Bit[7:6]: Data format - output full range enable
     * 0x: Output range: [10] to [F0]
     * 10: Output range: [01] to [FE]
     * 11: Output range: [00] to [FF]
     * Bit[5:4]: RGB 555/565 option (must set COM7[2] high)
     * x0: Normal RGB output
     * 01: RGB 565
     * 11: RGB 555
     * Bit[3:0]: Reserved
     */
    { OV9655_REG_COM15 , 0b11010000, 0b11110000 }, //XXX DEFAULT IS 0xC0 / 0xD0
    /*
     * Common Control 20
     * Bit[7:5]: Reserved
     * Bit[4]: Color bar test mode
     * Bit[3:0]: Reserved
     */
    { OV9655_REG_COM20 , 0b00010000, 0b00010000 },
};

uint8_t OV9655_short[][2] = {
    /* Vertical Frame Control
     * Bit[7:6]: AGC[9:8] (see register GAIN for AGC[7:0])
     * Bit[5:3]: VREF end low 3 bits (high 8 bits at VSTOP[7:0]
     * Bit[2:0]: VREF start low 3 bits (high 8 bits at VSTRT[7:0]
     */
    { OV9655_REG_VREF , 0x12 },
    /* Common Control 1
     * Bit[7]: Reserved
     * Bit[6]: CCIR656 format
     * Bit[5:4]: Reserved
     * Bit[3:2]: HREF skip option
     * 00: No skip
     * 01: YUV/RGB skip every other row for YUV/RGB, skip
     * 2 rows for every 4 rows for Raw data
     * 1x: Skip 3 rows for every 4 rows for YUV/RGB, skip 6
     * rows for every 8 rows for Raw data
     * Bit[1:0]: AEC low 2 LSBs (see registers AECH for AEC[15:10]
     * and AEC for AEC[9:2])
     */
    { OV9655_REG_COM1 , 0x00 },
    /* Common Control 3
     * Bit[7]: Color bar output
     * Bit[6]: Output data MSB and LSB swap
     * Bit[5:4]: Reserved
     * Bit[3]: Pin selection
     * 1: Change RESETB pin to EXPST_B (frame exposure
     * mode timing) and change PWDN pin to FREX
     * (frame exposure enable)
     * NOTE: This is for REV5 of the OV9655 sensor. For previous REV of
     * the sensor, RESETB pin is RESET (high for reset).
     * Bit[2]: RGB 565 format option
     * 0: RGB 565 format
     * 1: Output data average
     * Bit[1]: Reserved
     * Bit[0]: Single frame output (used for Frame Exposure mode
     * only)
     */
    { OV9655_REG_COM3 , 0x04 }, //XXX default is 0x00
    /* Common Control 6
     * Bit[7]: Output of optical black line option
     * 0: Disable HREF at optical black
     * 1: Enable HREF at optical black
     * Bit[6]: BLC input selection
     * 0: Use electrical black line as BLC signal
     * 1: Use optical black line as BLC signal
     * Bit[5:4]: Reserved
     * Bit[3]: Enable bias for B/Gr/Gb/R channel
     * Bit[2]: Output window setting auto/manual selection when
     * mode is changed
     * 0: Need to manually update window size when
     * working mode changes
     * 1: Auto change registers HSTART (0x17), HSTOP
     * (0x18), VSTRT (0x19), and VSTOP (0x1A) when
     * working mode changes
     * Bit[1]: Reset all timing when format changes
     * Bit[0]: Enable ADBLC option
     */
    { OV9655_REG_COM6 , 0x46 },
    /*
     * Data Format and Internal Clock
     * Bit[7]: Reserved
     * Bit[6]: Use external clock directly (no clock pre-scale available)
     * Bit[5:0]: Internal clock pre-scalar
     * F(internal clock) = F(input clock)/(Bit[5:0]+1)
     * • Range: [0 0000] to [1 1111]
     */
    { OV9655_REG_CLKRC , 0x00 },
    /*
     * Common Control 7
     * Bit[7]: SCCB register reset
     * 0: No change
     * 1: Resets all registers to default values
     * Bit[6:4]: Format control
     * 000: Full resolution or 15fps VGA
     * 110: 30fps VGA with VarioPixel ®
     * Bit[3:2]: Reserved
     * Bit[1:0]: Output format selection
     * 00: Raw RGB data
     * 01: Raw RGB interpolation
     * 10: YUV
     * 11: RGB
     */
    { OV9655_REG_COM7 , 0x03 }, //XXX DEFAULT IS 0x02
    /*
     * Common Control 9
     * Bit[7]: Reserved
     * Bit[6:4]: Automatic Gain Ceiling - maximum AGC value
     * 000: 2x
     * 001: 4x
     * 010: 8x
     * 011: 16x
     * 100: 32x
     * 101: 64x
     * 110: 128x
     * Bit[3]: Exposure timing can be less than limit of banding filter
     * when light is too strong
     * Bit[2]: Data format - VSYNC drop option
     * 0: VSYNC always exists
     * 1: VSYNC will drop when frame data drops
     * Bit[1]: Enable drop frame when AEC step is larger than the
     * Exposure Gap
     * Bit[0]: Reserved
     */
    { OV9655_REG_COM9 , 0x4a },
    /*
     * Common Control 10
     * Bit[7]: Set pin definition
     * 1: Set RESETB to SLHS (slave mode horizontal sync)
     * and set PWDN to SLVS (slave mode vertical sync)
     * NOTE: This is for REV5 of the OV9655 sensor. For previous REV of
     * the sensor, RESETB pin is RESET (high for reset).
     * Bit[6]: HREF changes to HSYNC
     * Bit[5]: PCLK output option
     * 0: PCLK always output
     * 1: No PCLK output when HREF is low
     * Bit[4]: PCLK reverse
     * Bit[3]: HREF reverse
     * Bit[2]: Reset signal end point option
     * Bit[1]: VSYNC negative
     * Bit[0]: HSYNC negative
     */
    { OV9655_REG_COM10 , 0x00 },
    /* Output Format
     * Horizontal Frame (HREF column) start high 8-bit (low 3 bits are at HREF[2:0])
     */
    { OV9655_REG_HSTART , 0x1A },
    /* Output Format
     * Horizontal Frame (HREF column) end high 8-bit (low 3 bits are at HREF[5:3])
     */
    { OV9655_REG_HSTOP , 0xBA },
    /* Output Format
     * Vertical Frame (row) start high 8-bit (low 3 bits are at VREF[2:0])
     */
    { OV9655_REG_VSTRT , 0x01 },
    /* Output Format
     * Vertical Frame (row) end high 8-bit (low 3 bits are at VREF[5:3])
     */
    { OV9655_REG_VSTOP , 0xB1 },
    /* Data Format - Pixel Delay Select (delays timing of the D[9:0] data
     * relative to HREF in pixel units)
     * • Range: [00] (no delay) to [FF] (256 pixel delay which accounts for
     * whole array)
     */
    { OV9655_REG_PSHFT , 0x00 },
    /* Mirror/VFlip Enable
     * Bit[7:6]: Reserved
     * Bit[5]: Mirror
     * 0: Normal image
     * 1: Mirror image
     * Bit[4]: VFlip enable
     * 0: VFlip disable
     * 1: VFlip enable
     * Bit[3:0]: Reserved
     */
    { OV9655_REG_MVFP , 0x00 },
    /*
     * HREF Control
     * Bit[7:6]: HREF edge offset to data output
     * Bit[5:3]: HREF end 3 LSB (high 8 MSB at register HSTOP)
     * Bit[2:0]: HREF start 3 LSB (high 8 MSB at register HSTART)
     */
    { OV9655_REG_HREF , 0xA4 },

    /*
     * Common Control 12
     * Bit[7]: HREF option
     * 0: No HREF when VREF is low
     * 1: Always has HREF
     * Bit[6:5]: Reserved
     * Bit[4]: Night mode speed selection
     * 0: Normal
     * 1: Fast
     * Bit[3]: Contrast expand center selection
     * 0: Use manually entered value at CNST2 (0x57) as
     * center value
     * 1: Use average value of last frame as center value
     * Bit[2:0]: Reserved
     */
    { OV9655_REG_COM12 , 0x0C },
    /*
     * Common Control 15
     * Bit[7:6]: Data format - output full range enable
     * 0x: Output range: [10] to [F0]
     * 10: Output range: [01] to [FE]
     * 11: Output range: [00] to [FF]
     * Bit[5:4]: RGB 555/565 option (must set COM7[2] high)
     * x0: Normal RGB output
     * 01: RGB 565
     * 11: RGB 555
     * Bit[3:0]: Reserved
     */
    { OV9655_REG_COM15 , 0xC0 },
    /*
     * Common Control 16
     * Bit[7:2]: Reserved
     * Bit[1]: Color matrix coefficient double option
     * Bit[0]: Scaling down ON/OFF selection
     * 0: Normal
     * 1: Scaling down
     */
    { OV9655_REG_COM16 , 0x40 },
    /*
     * Pixel Output Index
     * Bit[7]: Reserved
     * Bit[6]: Vertical pixel output option
     * 0: Use pixel average data
     * 1: Drop unused pixel data
     * Bit[5:4]: Vertical pixel output index
     * 00: Normal
     * 01: Output 1 line for every 2 lines
     * 10: Output 1 line for every 4 lines
     * 11: Output 1 line for every 8 lines
     * Bit[3]: Reserved
     * Bit[2]: Horizontal pixel output option
     * 0: Use pixel average data
     * 1: Drop unused pixel data
     * Bit[1:0]: Horizontal pixel output index
     * 00: Normal
     * 01: Output 1 line for every 2 pixels
     * 10: Output 1 line for every 4 pixels
     * 11: Output 1 line for every 8 pixels
     */
    { OV9655_REG_POIDX , 0x00 },
    /*
     * Pixel Clock Output Selection
     * Bit[7:4]: Reserved
     * Bit[3:0]: Pixel clock output frequency adjustment
     */
    { OV9655_REG_PCKDV , 0x01 },
    /*Horizontal Scaling Down Coefficients*/
    { OV9655_REG_XINDX , 0x3A },
    /*Vertical Scaling Down Coefficients*/
    { OV9655_REG_YINDX , 0x35 },
    /*
     * Common Control 20
     * Bit[7:5]: Reserved
     * Bit[4]: Color bar test mode
     * Bit[3:0]: Reserved
     */
    { OV9655_REG_COM20 , 0x00 },
    /*Common Control 24
     * Bit[7:3]: Reserved
     * Bit[2:0]: Pixel clock frequency selection
     */
    { OV9655_REG_COM24 , 0x80 },
};


const unsigned char OV9655_VGA[][2]=
{
  //{0x00, 0x00},
  {0x01, 0x80},
  {0x02, 0x80},
  /*
   * Common Control 22 ?
   * Bit[7:6]: Reserved
   * Bit[5]: BLC ON/OFF selection
   * Bit[4:0]: Reserved
   */
  //{0xb5, 0x00},//rsvd
  {0x35, 0x00},
  {0xa8, 0xc1},
  {0x3a, 0xcc},
  {0x3d, 0x99},
  //{0x77, 0x02},//rsvd
  {0x13, 0xe7},
  {0x26, 0x72},
  {0x27, 0x08},
  {0x28, 0x08},
  {0x2c, 0x08},
  //{0xab, 0x04},//rsvd
  //{0x6e, 0x00},//rsvd
  //{0x6d, 0x55},//rsvd
  {0x00, 0x11},
  {0x10, 0x7b},
  //{0xbb, 0xae},//rsvd
  {0x11, 0x03},
  {0x72, 0x00},//poidx -- no visible change
  {0x3e, 0x0c},
  {0x74, 0x3a},//xindx -- does nothing probably only when zoom is on
  //{0x76, 0x01},//rsvd
  {0x75, 0x35},//yindx -- does nothing probably only when zoom is on
  {0x73, 0x00},//pckdv -- no visible change
  {0xc7, 0x80},
  {0x62, 0x00},
  {0x63, 0x00},
  {0x64, 0x02},
  {0x65, 0x20},
  {0x66, 0x01},
  //{0xc3, 0x4e},//rsvd
  {0x33, 0x00},
  {0xa4, 0x50},
  //{0xaa, 0x92},//rsvd
  //{0xc2, 0x01},//rsvd
  //{0xc1, 0xc8},//rsvd
  {0x1e, 0x04},
  {0xa9, 0xef},
  {0x0e, 0x61},
  {0x39, 0x57},
  {0x0f, 0x48},
  {0x24, 0x3c},
  {0x25, 0x36},
  {0x12, 0x63},//com7 -- changes stuff
  {0x03, 0x12},//vref
  {0x32, 0xff},//href
  {0x17, 0x16},//hstart
  {0x18, 0x02},//hstop
  {0x19, 0x01},//vstrt
  {0x1a, 0x3d},//vstop
  {0x36, 0xfa},
  {0x69, 0x0a},//rsvd -- important fucks up the rest (variopixel?)
  {0x8c, 0x8d},
  //{0xc0, 0xaa},//rsvd
  {0x40, 0xd0},
  //{0x43, 0x14},//rsvd
  //{0x44, 0xf0},//rsvd
  //{0x45, 0x46},//rsvd
  //{0x46, 0x62},//rsvd
  //{0x47, 0x2a},//rsvd
  //{0x48, 0x3c},//rsvd
  {0x59, 0x85},
  {0x5a, 0xa9},
  {0x5b, 0x64},
  {0x5c, 0x84},
  {0x5d, 0x53},
  {0x5e, 0x0e},
  //{0x6c, 0x0c},//rsvd
  //{0xc6, 0x85},//rsvd
  //{0xcb, 0xf0},//rsvd
  //{0xcc, 0xd8},//rsvd
  //{0x71, 0x78},//rsvd
  //{0xa5, 0x68},//rsvd
  //{0x6f, 0x9e},//rsvd
  {0x42, 0xc0},
  {0x3f, 0x82},
  //{0x8a, 0x23},//rsvd
  {0x14, 0x3a},
  {0x3b, 0x05},// (0xcc before) com11 -- night mode / auto exposure / slows down the fps
  {0x34, 0x3d},
  {0x41, 0x40},
  //{0xc9, 0xe0},//rsvd
  //{0xca, 0xe8},//rsvd
  //{0xcd, 0x93},//rsvd
  {0x7a, 0x20},
  {0x7b, 0x1c},
  {0x7c, 0x28},
  {0x7d, 0x3c},
  {0x7e, 0x5a},
  {0x7f, 0x68},
  {0x80, 0x76},
  {0x81, 0x80},
  {0x82, 0x88},
  {0x83, 0x8f},
  {0x84, 0x96},
  {0x85, 0xa3},
  {0x86, 0xaf},
  {0x87, 0xc4},
  {0x88, 0xd7},
  {0x89, 0xe8},
  {0x4f, 0x98},
  {0x50, 0x98},
  {0x51, 0x00},
  {0x52, 0x28},
  {0x53, 0x70},
  {0x54, 0x98},
  {0x58, 0x1a},
  {0x6b, 0x5a},
  //{0x90, 0x92},//rsvd
  //{0x91, 0x92},//rsvd
  //{0x9f, 0x90},//rsvd
  //{0xa0, 0x90},//rsvd
  {0x16, 0x24},
  {0x2a, 0x00},
  {0x2b, 0x00},
  {0xac, 0x80},
  {0xad, 0x80},
  {0xae, 0x80},
  {0xaf, 0x80},
  {0xb2, 0xf2},
  {0xb3, 0x20},
  {0xb4, 0x20},
  //{0xb6, 0xaf},//rsvd
  {0x29, 0x15},
  {0x9d, 0x02},
  {0x9e, 0x02},
  //{0x9e, 0x02},
  {0x04, 0x03},
  {0x05, 0x2e},
  {0x06, 0x2e},
  {0x07, 0x2e},
  {0x08, 0x2e},
  {0x2f, 0x2e},
  //{0x4a, 0xe9},//rsvd
  //{0x4b, 0xdd},//rsvd
  //{0x4c, 0xdd},//rsvd
  //{0x4d, 0xdd},//rsvd
  //{0x4e, 0xdd},//rsvd
  {0x70, 0x06},
  {0xa6, 0x40},
  {0xbc, 0x02},
  {0xbd, 0x01},
  {0xbe, 0x02},
  {0xbf, 0x01},
};

/* @formatter:on */

extern I2C_HandleTypeDef hi2c1;

extern DCMI_HandleTypeDef hdcmi;

uint8_t sw_write_camera_reg(uint8_t controlBytes[], uint8_t numBytes)
{
  while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY) {
  }; //wait
  HAL_StatusTypeDef result = HAL_I2C_Master_Transmit(&hi2c1, OV9655_I2C_ADDRESS_W, controlBytes, numBytes, 1000);
  return result;
}

/**
 *
 * @param registerAddr
 * @param value
 * @param mask 1 - bytes that should be changed
 * @return
 */

uint8_t sw_write_camera_reg_masked(uint8_t registerAddr, uint8_t value, uint8_t mask)
{
  while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY) {
  }; //wait
  //Read current register value
  uint8_t cmd[2];
  cmd[0] = registerAddr;
  HAL_StatusTypeDef result = HAL_I2C_Master_Transmit(&hi2c1, OV9655_I2C_ADDRESS_R, cmd, 1, 1000);
  if (result != HAL_OK)
    return result;

  uint8_t registerValue;
  result = HAL_I2C_Master_Receive(&hi2c1, OV9655_I2C_ADDRESS_R, &registerValue, 1, 1000);
  if (result != HAL_OK)
    return result;

  //value & mask | reg & !mask
  registerValue = (value & mask) | (registerValue & ~mask);

  //send value to camera
  cmd[1] = registerValue;
  result = HAL_I2C_Master_Transmit(&hi2c1, OV9655_I2C_ADDRESS_W, cmd, 2, 1000);
  return result;
}

uint8_t sw_read_camera_reg(uint8_t mapbyte, uint8_t *received_byte)
{
  while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY) {
  }; //wait
  HAL_StatusTypeDef result = HAL_I2C_Master_Transmit(&hi2c1, OV9655_I2C_ADDRESS_W, &mapbyte, 1, 1000);
  if (result != HAL_OK)
    return result;
  while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY) {
  }; //wait
  result = HAL_I2C_Master_Receive(&hi2c1, OV9655_I2C_ADDRESS_R, received_byte, 1, 1000);
  return result;
}

uint8_t sw_ov9655_init()
{
  uint8_t size = sizeof(OV9655_short1) / 2;
  uint8_t command[2];
  command[0] = 0x12;
  command[1] = 0x80;
  sw_write_camera_reg(command, 2); //reset registers
  HAL_Delay(200);
  uint8_t res;
  for (uint8_t i = 0; i < size; i++) {
    if ((res = sw_write_camera_reg_masked(OV9655_short1[i][0], OV9655_short1[i][1], OV9655_short1[i][2])) != HAL_OK)
      return res;
    HAL_Delay(2);
  }
//    command[0] = 0x15;
//    command[1] = 0x08;
//    sw_write_camera_reg(command,2);
  return HAL_OK;
}

uint8_t sw_ov9655_init_qvga()
{
  uint8_t size = sizeof(OV9655_VGA) / 2;
  uint8_t command[2];
  command[0] = 0x12;
  command[1] = 0x80;
  sw_write_camera_reg(command, 2); //reset registers
  HAL_Delay(200);
  uint8_t res;
  for (uint8_t i = 0; i < size; i++) {
    if ((res = sw_write_camera_reg(OV9655_VGA[i],2)) != HAL_OK)
      return res;
    HAL_Delay(2);
  }
  return HAL_OK;
}

uint8_t sw_ov9655_power_up()
{
  return HAL_OK;
}

void sw_read_all_registers(uint8_t *RegArray)
{

  for (uint8_t i = 0x00; i < 0xCF; i++) {
    sw_read_camera_reg(i, &RegArray[i]);
  }
}

uint8_t sw_ov9655_check_registers()
{
  uint8_t size = sizeof(OV9655_short1) / 2;
  uint8_t result = 0x00;
  uint8_t reg = 0x00;
  for (uint8_t i = 0; i < size; i++) {
    sw_read_camera_reg(OV9655_short1[i][0], &reg);
    if (reg != OV9655_short1[i][1])
      result++;
  }
  return result;
}

uint8_t sw_ov9655_reset_regs()
{
  uint8_t val = 0b10000000; //Bit[7] is SCCB register reset
  uint8_t mask = 0b10000000;

  return sw_write_camera_reg_masked(OV9655_REG_COM7, val, mask);
}

uint8_t sw_ov9655_deinit()
{
  return HAL_OK;
}

uint8_t __sw_ov9655_qvga_setup()
{
  return HAL_OK;
}

uint8_t __sw_ov9655_vga_setup()
{

  return HAL_OK;
}
