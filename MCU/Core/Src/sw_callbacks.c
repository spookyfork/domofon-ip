/*
 * sw_callbacks.c
 *
 *  Created on: Oct 9, 2020
 *      Author: rand
 */

#include "main.h"
#include "stm32f4xx_hal.h"

//extern uint8_t rx_complete;
//extern uint8_t tx_complete;
//
//
//extern uint8_t lineEvent;
//extern uint8_t frameEvent;
//extern uint8_t vsyncEvent;
extern uint8_t callback_flag;
extern uint16_t part;

void HAL_I2S_TxHalfCpltCallback(I2S_HandleTypeDef *hi2s)
{

}

void HAL_I2S_TxCpltCallback(I2S_HandleTypeDef *hi2s)
{
  //audio_flag = audio_flag | AUDIO_IN_READY;
  //tx_complete = 1;
  callback_flag |= FLAG_TX_COMPLETE;
}

//void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
//{
//  //HAL_GPIO_TogglePin( LED_GREEN_GPIO_Port, LED_GREEN_Pin);
//}

void HAL_I2S_RxHalfCpltCallback(I2S_HandleTypeDef *hi2s)
{
  //rx_complete = 1;
  callback_flag |= FLAG_RX_HALFCPLT;
}

void HAL_I2S_RxCpltCallback(I2S_HandleTypeDef *hi2s)
{
  //rx_complete = 2;
  callback_flag |= FLAG_RX_CPLT;
}

void HAL_SPI_RxHalfCpltCallback(SPI_HandleTypeDef *hspi)
{
//  rx_complete = 1;
  callback_flag |= FLAG_RX_HALFCPLT;
}

void HAL_SPI_RxCpltCallback(SPI_HandleTypeDef *hspi)
{
//  rx_complete = 2;
  callback_flag |= FLAG_RX_CPLT;
}

void HAL_DCMI_LineEventCallback(DCMI_HandleTypeDef *hdcmi)
{
  //works
//  lineEvent = 1;
  callback_flag |= FLAG_LINE_EVENT;
}

void HAL_DCMI_FrameEventCallback(DCMI_HandleTypeDef *hdcmi)
{
  //works
//  frameEvent = 1;
  callback_flag |= FLAG_FRAME_EVENT;

}

void HAL_DCMI_VsyncEventCallback(DCMI_HandleTypeDef *hdcmi)
{
  //works
//  vsyncEvent = 1;
  callback_flag |= FLAG_VSYNC_EVENT;
}
