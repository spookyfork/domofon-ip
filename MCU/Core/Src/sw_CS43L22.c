/*
 * sw_CS43L22.c
 *
 *  Created on: 08.11.2018
 *      Author: rand
 */

#include "stm32f4xx_hal.h"
#include "sw_CS43L22.h"

extern I2C_HandleTypeDef hi2c1;

extern I2S_HandleTypeDef hi2s3;
extern DMA_HandleTypeDef hdma_spi3_tx;

uint8_t __sw_send_codec_ctrl(uint8_t controlBytes[], uint8_t numBytes)
{
  uint8_t bytesSent = 0;
  while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY) {
  }
  ; //wait
  HAL_StatusTypeDef result = HAL_I2C_Master_Transmit(&hi2c1, CS43L22_I2C_ADDRESS_W, controlBytes, numBytes, 1000);
  return result;
}

uint8_t __sw_read_codec_register(uint8_t mapbyte, uint8_t *received_byte)
{
  while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY) {
  }; //wait
  HAL_StatusTypeDef result = HAL_I2C_Master_Transmit(&hi2c1, CS43L22_I2C_ADDRESS_W, &mapbyte, 1, 1000);
  if (result != HAL_OK)
    return result;
  while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY) {
  }; //wait
  result = HAL_I2C_Master_Receive(&hi2c1, CS43L22_I2C_ADDRESS_R, received_byte, 1, 1000);
  return result;
}

uint8_t sw_CS43L22_power_up()
{
  uint8_t result = 0;
  uint8_t regs_to_set[5];
  uint8_t reg_value;

  HAL_GPIO_WritePin(Audio_RST_GPIO_Port, Audio_RST_Pin, GPIO_PIN_RESET);
  HAL_Delay(1000);
  HAL_GPIO_WritePin(Audio_RST_GPIO_Port, Audio_RST_Pin, GPIO_PIN_SET);

  uint32_t delaycount = 1000000;
  while (delaycount > 0) {
    delaycount--;
  }

  // keep codec off
  regs_to_set[0] = CS43L22_REG_POWER_CTL1;
  regs_to_set[1] = 0x01;
  result = __sw_send_codec_ctrl(regs_to_set, 2);
  if (result != HAL_OK)
    return result;

  //INIT SEQUENCE
  regs_to_set[0] = 0x00;
  regs_to_set[1] = 0x99;
  result = __sw_send_codec_ctrl(regs_to_set, 2);
  if (result != HAL_OK)
    return result;

  regs_to_set[0] = 0x47;
  regs_to_set[1] = 0x80;
  result = __sw_send_codec_ctrl(regs_to_set, 2);
  if (result != HAL_OK)
    return result;

  reg_value = 0;
  result = __sw_read_codec_register(0x32, &reg_value);
  if (result != HAL_OK)
    return result;

  regs_to_set[0] = 0x32;
  regs_to_set[1] = reg_value | 0x80;
  result = __sw_send_codec_ctrl(regs_to_set, 2);
  if (result != HAL_OK)
    return result;

  result = __sw_read_codec_register(0x32, &reg_value);
  if (result != HAL_OK)
    return result;

  regs_to_set[0] = 0x32;
  regs_to_set[1] = reg_value & ~(0x80);
  result = __sw_send_codec_ctrl(regs_to_set, 2);
  if (result != HAL_OK)
    return result;

  regs_to_set[0] = 0x00;
  regs_to_set[1] = 0x00;
  result = __sw_send_codec_ctrl(regs_to_set, 2);
  if (result != HAL_OK)
    return result;

  // END OF INIT SEQIENCE

  result = sw_CS43L22_load_config(CS43L22_48kHZ_16bit);
  if (result != HAL_OK)
    return result;

  //LAST STEP OF POWER-UP
  regs_to_set[0] = 0x02;
  regs_to_set[1] = 0x9E;
  result = __sw_send_codec_ctrl(regs_to_set, 2);
  if (result != HAL_OK)
    return result;

  return HAL_OK;
}
uint8_t sw_CS43L22_init()
{
  return 0;
}
uint8_t sw_CS43L22_power_down()
{
  return 0;
}
uint8_t sw_CS43L22_load_config(CS43L22_CONGIFURATION config)
{
  // Power Ctl 2 (0x04) set 0xaf (1010 1111)
  // 10 - Headphone Channel B is always on
  // 10 - Headphone Channel A is always on
  // 11 - Speaker Channel B is always off
  // 11 - Speaker Channel A is always off
  uint8_t regs_to_set[5];
  regs_to_set[0] = CS43L22_REG_POWER_CTL2;
  regs_to_set[1] = 0xAF;
  uint8_t result = __sw_send_codec_ctrl(regs_to_set, 2);
  if (result != HAL_OK)
    return result;

  // Playback Ctl 1 (0x0D) set 0x70 (0111 0000)
  // 011 - Headphone Analog Gain - 0.6047
  // 1 - Playback Volume Setting B=A - Enabled
  // 0 - Invert PCM signal Polarity B - Not Inverted
  // 0 - Invert PCM signal Polarity A - Not Inverted
  // 0 - Master Playback Mute B -
  // 0 - Master Playback Mute A -
  regs_to_set[0] = CS43L22_REG_PLAYBACK_CTL1;
  regs_to_set[1] = 0x70;
  result = __sw_send_codec_ctrl(regs_to_set, 2);
  if (result != HAL_OK)
    return result;

  // Playback Ctl 2 (0x0F) set 0x3A (0011 1010)
  // 0 - Headphones B Mute
  // 0 - Headphones A Mute
  // 1 - Speaker B Mute
  // 1 - Speaker A Mute
  // 1 - Speaker Volume Setting B=A
  // 0 - Speaker Channel Swap - Channel A/B
  // 1 - Speaker Mono Control
  // 0 - Speaker Mute 50/50 Control

  //This does nothing
  regs_to_set[0] = CS43L22_REG_PLAYBACK_CTL2;
  regs_to_set[1] = 0x0A; //0b00111010; //
  result = __sw_send_codec_ctrl(regs_to_set, 2);
  if (result != HAL_OK)
    return result;

  // Clocking Control (0x05) set 0x81 (1000 0001)//auto detect clock
  // 1 - Auto Detect enabled
  // 00 - Speed Mode - Double Speed 50kHz-100kHz Fs -- Possible change to 01 - Single Speed 4kHz-50kHz
  // 0 - 32kHz Sample Group - input/output is 8,16,32 kHz sample rate
  // 0 - 27MHz Video Clock - MCLK is 27MHz (no)
  // 00 - Internal MCLK/LRCK Ratio - 128 MCLK cycles per LRCK
  // 0 - MCLK Divide by 2 - no
  regs_to_set[0] = CS43L22_REG_CLOCKING_CTL;
  regs_to_set[1] = 0b10100000;
  result = __sw_send_codec_ctrl(regs_to_set, 2);
  if (result != HAL_OK)
    return result;

  // Interface Ctl 1 (0x06) set 0x07 (0000 0111)
  // 0 - Master/Slave Mode - slave
  // 0 - SCLK Polarity - not inverted
  // 0 - Reserved
  // 0 - DSP Mode - Disabled
  // 01 - DAC Interface fromat - I2S, up to 24bit data -- Possible changes needed
  // 11 - Audio Word Length - 16bit data
  regs_to_set[0] = CS43L22_REG_INTERFACE_CTL1;
  regs_to_set[1] = 0x07;
  result = __sw_send_codec_ctrl(regs_to_set, 2);
  if (result != HAL_OK)
    return result;

  // Analog Set (0x0A) set 0x00 ()
  // 0000 - Reserved
  // 0 - Ch. B Analog Soft Ramp
  // 0 - Ch. B Analog Zero Cross
  // 0 - Ch. A Analog Soft Ramp
  // 0 - Ch. A Analog Zero Cross
  regs_to_set[0] = CS43L22_REG_ANALOG_ZCSR;
  regs_to_set[1] = 0x00;
  result = __sw_send_codec_ctrl(regs_to_set, 2);
  if (result != HAL_OK)
    return result;

  // Limit Ctl1 (0x27) set 0x00 ()
  // 000 - Limiter Maximum Threshold - 0dB
  // 000 - Limiter Cushion Threshold - 0dB
  // 0 - Limiter Soft Ramp Disable - OFF
  // 0 - Limiter Zero Cross Disable - OFF
  regs_to_set[0] = CS43L22_REG_LIMIT_CTL_1;
  regs_to_set[1] = 0x00;
  result = __sw_send_codec_ctrl(regs_to_set, 2);
  if (result != HAL_OK)
    return result;

  // PCMA/PCMB Vol 0x1A | CODEC_MAPBYTE_INC set 0x0A, 0x0A (0000 1010)
  // 0 - PCM Channel Mute
  // 000 1010 - PCM Channel Volume - +5dB
  regs_to_set[0] = CS43L22_REG_PCMA_VOL | CS43L22_MAPBYTE_INC;
  regs_to_set[1] = 0x00;
  regs_to_set[2] = 0x00;
  result = __sw_send_codec_ctrl(regs_to_set, 3);
  if (result != HAL_OK)
    return result;

  // Tone Ctl (0x1F) set 0x0F (0000 1111)
  // 0000 - Treble Gain - +12dB
  // 1111 - Bass Gain - -10.5dB

  return HAL_OK;
}

uint8_t sw_CS43L22_read_chip_id(uint8_t *result)
{
  return __sw_read_codec_register(CS43L22_REG_ID, result);
}

uint8_t sw_CS43L22_mute()
{
  uint8_t regs_to_set[5];
  //Power-down
  /*	regs_to_set[0] = CS43L22_REG_PLAYBACK_CTL1;
   regs_to_set[1] = 0b01110011;
   sw_send_codec_ctrl(regs_to_set, 2);
   regs_to_set[0] = CS43L22_REG_PLAYBACK_CTL2;
   regs_to_set[1] = 0b11111010;
   sw_send_codec_ctrl(regs_to_set, 2);

   regs_to_set[0] = CS43L22_REG_PCMA_VOL | CS43L22_MAPBYTE_INC;
   regs_to_set[1] = 0b10000000;
   regs_to_set[2] = 0b10000000;
   sw_send_codec_ctrl(regs_to_set, 3);

   regs_to_set[0] = CS43L22_REG_HEADPHONE_A_VOL | CS43L22_MAPBYTE_INC;
   regs_to_set[1] = 0x01;
   regs_to_set[2] = 0x01;
   sw_send_codec_ctrl(regs_to_set, 3);*/

  regs_to_set[0] = CS43L22_REG_POWER_CTL2;
  regs_to_set[1] = 0xff;
  __sw_send_codec_ctrl(regs_to_set, 2);

  return 0;

}
uint8_t sw_CS43L22_unmute()
{
  uint8_t regs_to_set[5];
  //Power-down
  regs_to_set[0] = CS43L22_REG_POWER_CTL2;
  regs_to_set[1] = 0xAF;
  __sw_send_codec_ctrl(regs_to_set, 2);

  /*
   regs_to_set[0] = CS43L22_REG_HEADPHONE_A_VOL | CS43L22_MAPBYTE_INC;
   regs_to_set[1] = 0x00;
   regs_to_set[2] = 0x00;
   sw_send_codec_ctrl(regs_to_set, 3);

   regs_to_set[0] = CS43L22_REG_PCMA_VOL | CS43L22_MAPBYTE_INC;
   regs_to_set[1] = 0x00;
   regs_to_set[2] = 0x00;
   sw_send_codec_ctrl(regs_to_set, 3);

   regs_to_set[0] = CS43L22_REG_PLAYBACK_CTL1;
   regs_to_set[1] = 0b01110000;
   sw_send_codec_ctrl(regs_to_set, 2);
   regs_to_set[0] = CS43L22_REG_PLAYBACK_CTL2;
   regs_to_set[1] = 0b00111010;
   sw_send_codec_ctrl(regs_to_set, 2);*/

  return 0;
}

uint8_t sw_CS43L22_freeze()
{
  //Freeze Registers
  uint8_t regs_to_set[5];
  regs_to_set[0] = CS43L22_REG_MISC_CTL;
  regs_to_set[1] = 0b00001010;
  return __sw_send_codec_ctrl(regs_to_set, 2);
}
uint8_t sw_CS43L22_unfreeze()
{
  //Freeze Registers
  uint8_t regs_to_set[5];
  regs_to_set[0] = CS43L22_REG_MISC_CTL;
  regs_to_set[1] = 0b00000010;
  return __sw_send_codec_ctrl(regs_to_set, 2);

}
