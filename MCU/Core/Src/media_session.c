/*
 * media_session.c
 *
 *  Created on: Apr 19, 2021
 *      Author: rand
 */

#include <string.h>
#include "media_session.h"

#ifndef UNIT_TESTING
#include "lwip.h"
#else

const ip_addr_t ip_addr_any = "ANY";
extern void* _test_malloc(const size_t size, const char* file, const int line);
extern void* _test_free(const size_t size, const char* file, const int line);
#define malloc(size) _test_malloc(size, __FILE__, __LINE__)
#define free(ptr) _test_free(ptr, __FILE__, __LINE__)

extern struct udp_pcb* udp_new();
extern int udp_bind(struct udp_pcb *pcb, const ip_addr_t *ipaddr, uint16_t port);
extern void udp_recv(struct udp_pcb *pcb, udp_recv_fn recv, void *recv_arg);
extern int udp_sendto(struct udp_pcb *pcb, struct pbuf *p,
  const ip_addr_t *dst_ip, uint16_t dst_port);
extern uint8_t pbuf_free(struct pbuf *p);
extern struct pbuf *pbuf_alloc(int layer, uint16_t length, int type);
#endif // !UNIT_TESTING


/**
 * Initialize media_session structure. Using ports == 0 will cause dynamic allocation.
 * @param ms
 * @param src_port
 * @param dst_port
 * @param callback
 * @return
 */
int swms_Init(struct media_session *ms, int src_port, int recv_port, swms_recv_fn callback, int video_src_port, int video_recv_port,
  swms_recv_fn video_callback)
{
  if (!ms)
    return SWMS_PARAM_ERROR;

  memset(ms, 0, sizeof(struct media_session));

  ms->__state = __SWMS_STATE_INIT;
  ms->audio.src_port = src_port;
  ms->audio.recv_port = recv_port;
  ms->audio.recv = callback;
  ms->audio.stack = NULL;
  ms->audio.__connection = __SWMS_CONNECTION_RESET;

  ms->video.__connection = __SWMS_CONNECTION_RESET;
  ms->video.src_port = video_src_port;
  ms->video.recv_port = video_recv_port;
  ms->video.recv = video_callback;
  ms->video.stack = NULL;

  //memset(ms->callbacks, 0, sizeof(signal_fn) * CB_SIZE);

  return SWMS_OK;
}

/**
 * Allocate resources needed
 * @param ms
 * @return
 */
int swms_Open(struct media_session *ms)
{
  if (!ms)
    return SWMS_PARAM_ERROR;
  int res = swms_OpenSession(&(ms->audio));
  if (res != SWMS_OK) {
    return res;
  }
  res = swms_OpenSession(&(ms->video));
  if (res != SWMS_OK) {
    return res;
  }
  ms->__state |= __SWMS_STATE_OPEN;
  return SWMS_OK;
}

int swms_OpenSession(struct session *s)
{
  if (!s)
    return SWMS_PARAM_ERROR;
  s->stack = udp_new();

  if (s->stack == NULL) {
    return SWMS_NOMEM;
  }

  if (s->recv_port < 0 || s->recv == NULL) {
    //We don't want to receive data
    s->recv_port = 0;
    s->__connection &= ~__SWMS_CONNECTION_IN;
  }
  else {
    s->__connection |= __SWMS_CONNECTION_IN;
    udp_bind(s->stack, IP_ADDR_ANY, s->recv_port);
    if (s->recv_port == 0) {
      s->recv_port = s->stack->local_port;
    }
    udp_recv(s->stack, &__swms_RecvCallback, s);
  }

  if (s->src_port < 0) {
    //We dont want to send data
    s->src_port = 0;
    s->__connection &= ~__SWMS_CONNECTION_OUT;
  }
  else {
    s->__connection |= __SWMS_CONNECTION_OUT;
  }

  return SWMS_OK;
}

int swms_Bind(struct media_session *ms, const char *remote_host, uint16_t audio_src_port, uint16_t audio_dst_port, uint16_t video_src_port,
  uint16_t video_dst_port)
{
  if (!ms)
    return SWMS_PARAM_ERROR;
  if (!remote_host)
    return SWMS_UNKNOWN_HOST;
  if (!(ms->__state & __SWMS_STATE_INIT)) {
    return SWMS_API_NOT_INITIALIZED;
  }
  ms->host = remote_host;

  swms_BindSession(&(ms->audio), audio_src_port, audio_dst_port);
  swms_BindSession(&(ms->video), video_src_port, video_dst_port);

  return SWMS_OK;
}

int swms_BindSession(struct session *s, uint16_t src_port, uint16_t dst_port)
{
  s->remote_src_port = src_port;
  s->remote_dst_port = dst_port;

  if (src_port == 0) {
    //remote host does not want to send data
    s->__connection &= ~__SWMS_CONNECTION_IN;
  }
  if (dst_port == 0) {
    //remote host does not want to receive data
    s->__connection &= ~__SWMS_CONNECTION_OUT;
  }
  return SWMS_OK;
}

int swms_Start(struct media_session *ms)
{
  if (!ms)
    return SWMS_PARAM_ERROR;
  if (!(ms->__state & __SWMS_STATE_OPEN))
    return SWMS_API_NOT_INITIALIZED;

  ms->__state |= __SWMS_STATE_ACTIVE;

  //Reset
  ms->audio.__connection &= ~(__SWMS_CONNECTION_IN_ACT | __SWMS_CONNECTION_OUT_ACT);
  ms->video.__connection &= ~(__SWMS_CONNECTION_IN_ACT | __SWMS_CONNECTION_OUT_ACT);

  //If the there is a connection
  //And There is no callback or there is a callback and it returns OK
  //Make the connection active
  if ((ms->audio.__connection & __SWMS_CONNECTION_IN)
    && (!ms->callbacks[MS_START_AUDIO_IN] || (ms->callbacks[MS_START_AUDIO_IN](MS_START_AUDIO_IN) == SWMS_OK))) {
    ms->audio.__connection |= __SWMS_CONNECTION_IN_ACT;
  }

  if ((ms->audio.__connection & __SWMS_CONNECTION_OUT)
    && (!ms->callbacks[MS_START_AUDIO_OUT] || (ms->callbacks[MS_START_AUDIO_OUT](MS_START_AUDIO_OUT) == SWMS_OK))) {
    ms->audio.__connection |= __SWMS_CONNECTION_OUT_ACT;
  }
  if ((ms->video.__connection & __SWMS_CONNECTION_OUT)
    && (!ms->callbacks[MS_START_VIDEO_OUT] || (ms->callbacks[MS_START_VIDEO_OUT](MS_START_VIDEO_OUT) == SWMS_OK))) {
    ms->video.__connection |= __SWMS_CONNECTION_OUT_ACT;
  }
  return SWMS_OK;
}

int swms_Stop(struct media_session *ms)
{
  if (!ms)
    return SWMS_PARAM_ERROR;
  if (!(ms->__state & __SWMS_STATE_ACTIVE))
    return SWMS_API_NOT_INITIALIZED;

  ms->__state &= !__SWMS_STATE_ACTIVE;
  ms->audio.__connection &= ~(__SWMS_CONNECTION_IN_ACT | __SWMS_CONNECTION_OUT_ACT);
  ms->video.__connection &= ~(__SWMS_CONNECTION_IN_ACT | __SWMS_CONNECTION_OUT_ACT);

  if (ms->callbacks[MS_STOP_AUDIO_IN])
    ms->callbacks[MS_STOP_AUDIO_IN](MS_STOP_AUDIO_IN);
  if (ms->callbacks[MS_STOP_AUDIO_OUT])
    ms->callbacks[MS_STOP_AUDIO_OUT](MS_STOP_AUDIO_OUT);
  if (ms->callbacks[MS_STOP_VIDEO_OUT])
    ms->callbacks[MS_STOP_VIDEO_OUT](MS_STOP_VIDEO_OUT);

  return SWMS_OK;
}

int swms_Close(struct media_session *ms)
{
  if (!ms)
    return SWMS_PARAM_ERROR;

  udp_remove(ms->audio.stack);
  udp_remove(ms->video.stack);
  ms->audio.stack = NULL;
  ms->video.stack = NULL;
  ms->__state = __SWMS_STATE_CLOSED;

  return SWMS_OK;
}

int swms_SetCallback(struct media_session *ms, enum CB_SIGNALS signal, signal_fn callback)
{
  if (ms == NULL || signal >= CB_SIZE)
    return SWMS_PARAM_ERROR;
  ms->callbacks[signal] = callback;

  return SWMS_OK;
}

int swms_SendAudio(struct media_session *ms, void *data, uint16_t len)
{
  if (!ms)
    return SWMS_PARAM_ERROR;
  if (!(ms->__state & __SWMS_STATE_ACTIVE))
    return SWMS_NO_NETWORK;

  if (!(ms->audio.__connection & __SWMS_CONNECTION_OUT_ACT)) {
    return SWMS_NO_NETWORK;
  }

  struct pbuf *p = pbuf_alloc(PBUF_TRANSPORT, len, PBUF_ROM);
  if (data)
//    memcpy(p->payload, data, len);
    p->payload = data;

  //pass it to lwip
  ip4_addr_t dst;
  ipaddr_aton(ms->host, &dst);

  udp_sendto(ms->audio.stack, p, &dst, ms->audio.remote_dst_port);
  //free pbuf
  pbuf_free(p);
  return SWMS_OK;
}

int swms_SendVideo(struct media_session *ms, void *data, uint16_t len)
{
  if (!ms || !data)
    return SWMS_PARAM_ERROR;
  if (!(ms->__state & __SWMS_STATE_ACTIVE))
    return SWMS_NO_NETWORK;

  if (!(ms->video.__connection & __SWMS_CONNECTION_OUT_ACT)) {
    return SWMS_NO_NETWORK;
  }

  struct pbuf *p = pbuf_alloc(PBUF_TRANSPORT, len, PBUF_ROM);
  if (data)
//    memcpy(p->payload, data, len);
    p->payload = data;

  //pass it to lwip
  ip4_addr_t dst;
  ipaddr_aton(ms->host, &dst);

  udp_sendto(ms->video.stack, p, &dst, ms->video.remote_dst_port);
  //free pbuf
  pbuf_free(p);
  return SWMS_OK;
}

void __swms_RecvCallback(void *arg, struct udp_pcb *pcb, struct pbuf *p, const ip_addr_t *addr, uint16_t port)
{
  if (!arg) {
    //media_session not attached to stack?
    if (p)
      pbuf_free(p);
    return;
  }

  struct session *s = (struct session*) arg;
  if ((s->__connection & __SWMS_CONNECTION_IN_ACT) && s->remote_src_port == port) {
    //Process the data
    if (s->recv) {
      if (p)
        s->recv(p->payload, p->len);
      else
        s->recv(NULL, 0);
    }
  }

  if (p)
    pbuf_free(p);

}
