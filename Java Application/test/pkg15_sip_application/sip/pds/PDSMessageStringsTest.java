/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg15_sip_application.sip.pds;

import java.util.Arrays;
import java.util.List;
import javax.sound.sampled.AudioFormat;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import pkg15_sip_application.exception.MalformedPacketException;
import pkg15_sip_application.video.ImageReceiver;

/**
 *
 * @author rand
 */
@RunWith(Parameterized.class)
public class PDSMessageStringsTest {

    @Parameter(0)
    public PDSMessage message;
    @Parameter(1)
    public String messageString;
    @Parameter(2)
    public Class<? extends Exception> expectedException;
    @Parameter(3)
    public String expectedExceptionMsg;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Parameterized.Parameters
    public static Iterable<Object[]> data() {
        String s1 = "a = 1 2 8000 16 2 true true\n"
                + "a = 3 4 16000 8 1 false false\n"
                + "a = 5 6 44100 32 2 1 1\n"
                + "a = 7 8 7777 8 3 0 0\n"
                + "v = 9 10 640 480 1280 TYPE_USHORT_565_RGB\n";
        PDSMessage m1 = new PDSMessage();
        m1.addAudioHeader(new AudioHeader(1, 2, new AudioFormat(8000, 16, 2, true, true)));
        m1.addAudioHeader(new AudioHeader(3, 4, new AudioFormat(16000, 8, 1, false, true)));
        m1.addAudioHeader(new AudioHeader(5, 6, new AudioFormat(44100, 16, 2, true, true)));
        m1.addAudioHeader(new AudioHeader(7, 8, new AudioFormat(7777, 8, 3, false, false)));
        m1.addVideoHeader(new VideoHeader(9, 10, 640, 480, 1280, ImageReceiver.ImageFormat.TYPE_USHORT_565_RGB));

        String s2 = "a= 1 2 8000 16 2 true true\n"
                + "a=3 4 16000 8 1 false false\n"
                + "a =5 6 44100 32 2 1 1\n"
                + "v= 9 10 640 480 100 TYPE_4BYTE_ABGR\n"
                + "v=11 12 900 900 200 TYPE_INT_RGB\n"
                + "v =13 14 600 600 300 TYPE_USHORT_GRAY\n";
        PDSMessage m2 = new PDSMessage();
        m1.addAudioHeader(new AudioHeader(1, 2, new AudioFormat(8000, 16, 2, true, true)));
        m1.addAudioHeader(new AudioHeader(3, 4, new AudioFormat(16000, 8, 1, false, true)));
        m1.addAudioHeader(new AudioHeader(5, 6, new AudioFormat(44100, 16, 2, true, true)));
        m1.addVideoHeader(new VideoHeader(9, 10, 640, 480, 100, ImageReceiver.ImageFormat.TYPE_4BYTE_ABGR));
        m1.addVideoHeader(new VideoHeader(11, 12, 900, 900, 200, ImageReceiver.ImageFormat.TYPE_INT_RGB));
        m1.addVideoHeader(new VideoHeader(13, 14, 600, 600, 300, ImageReceiver.ImageFormat.TYPE_USHORT_GRAY));

        String s3 = "a= 1 2 8d000 16 2 true true\n"
                + "v =13 14 600 600 300 TYPE_USHORT_GRAY\n";

        String s4 = "a= 1 2 8000 16 2 true true\n"
                + "v =13 14 600 60f0 300 TYPE_USHORT_GRAY\n";

        return Arrays.asList(new Object[][]{
            {m1, s1, null, null},
            {m2, s2, null, null},
            {new PDSMessage(), "", null, null},
            {new PDSMessage(), null, null, null},
            {null, s3, MalformedPacketException.class, null},
            {null, s4, MalformedPacketException.class, null}
        });
    }

    /**
     * Test of addAudioHeader method, of class PDSMessage.
     */
    @Test
    public void testAddAudioHeader_AudioHeader() {
        System.out.println("addAudioHeader");
        AudioHeader header = null;
        PDSMessage instance = new PDSMessage();
        instance.addAudioHeader(header);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addAudioHeader method, of class PDSMessage.
     */
    @Test
    public void testAddAudioHeader_List() {
        System.out.println("addAudioHeader");
        List<AudioHeader> headers = null;
        PDSMessage instance = new PDSMessage();
        instance.addAudioHeader(headers);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addVideoHeader method, of class PDSMessage.
     */
    @Test
    public void testAddVideoHeader() {
        System.out.println("addVideoHeader");
        VideoHeader header = null;
        PDSMessage instance = new PDSMessage();
        instance.addVideoHeader(header);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAudioHeaders method, of class PDSMessage.
     */
    @Test
    public void testGetAudioHeaders() {
        System.out.println("getAudioHeaders");
        PDSMessage instance = new PDSMessage();
        List<AudioHeader> expResult = null;
        List<AudioHeader> result = instance.getAudioHeaders();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAudioHeaderFirst method, of class PDSMessage.
     */
    @Test
    public void testGetAudioHeaderFirst() {
        System.out.println("getAudioHeaderFirst");
        PDSMessage instance = new PDSMessage();
        AudioHeader expResult = null;
        AudioHeader result = instance.getAudioHeaderFirst();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getVideoHeaders method, of class PDSMessage.
     */
    @Test
    public void testGetVideoHeaders() {
        System.out.println("getVideoHeaders");
        PDSMessage instance = new PDSMessage();
        List<VideoHeader> expResult = null;
        List<VideoHeader> result = instance.getVideoHeaders();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getVideoHeaderFirst method, of class PDSMessage.
     */
    @Test
    public void testGetVideoHeaderFirst() {
        System.out.println("getVideoHeaderFirst");
        PDSMessage instance = new PDSMessage();
        VideoHeader expResult = null;
        VideoHeader result = instance.getVideoHeaderFirst();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getGateState method, of class PDSMessage.
     */
    @Test
    public void testGetGateState() {
        System.out.println("getGateState");
        PDSMessage instance = new PDSMessage();
        PDSMessage.GateState expResult = null;
        PDSMessage.GateState result = instance.getGateState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setGateState method, of class PDSMessage.
     */
    @Test
    public void testSetGateState() {
        System.out.println("setGateState");
        PDSMessage.GateState state = null;
        PDSMessage instance = new PDSMessage();
        instance.setGateState(state);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of parse method, of class PDSMessage.
     */
    @Test
    public void testParse() {
        System.out.println("parse");
        String s = "";
        PDSMessage instance = new PDSMessage();
        instance.parse(s);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of toString method, of class PDSMessage.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        PDSMessage instance = new PDSMessage();
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

}
