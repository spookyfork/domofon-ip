/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg15_sip_application.sip.pds;

import java.util.Arrays;
import javax.sound.sampled.AudioFormat;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

/**
 *
 * @author rand
 */
@RunWith(Parameterized.class)
public class AudioHeaderParameterTest {

    @Parameter(0)
    public int src_port;
    @Parameter(1)
    public int dst_port;
    @Parameter(2)
    public AudioFormat format;
    @Parameter(3)
    public Class<? extends Exception> expectedException;
    @Parameter(4)
    public String expectedExceptionMsg;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Parameters
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
            //Bottom boundary for source port
            {-1, 666, new AudioFormat(8000.f, 16, 2, true, true), IllegalArgumentException.class, null},
            {0, 666, new AudioFormat(8000.f, 16, 2, true, true), null, null},
            {1, 666, new AudioFormat(8000.f, 16, 2, true, true), null, null},
            //Top boundary for source port
            {0xFFFF - 1, 666, new AudioFormat(8000.f, 16, 2, true, true), null, null},
            {0xFFFF, 666, new AudioFormat(8000.f, 16, 2, true, true), null, null},
            {0xFFFF + 1, 666, new AudioFormat(8000.f, 16, 2, true, true), IllegalArgumentException.class, null},
            //Bottom boundary for destination port
            {666, -1, new AudioFormat(8000.f, 16, 2, true, true), IllegalArgumentException.class, null},
            {666, 0, new AudioFormat(8000.f, 16, 2, true, true), null, null},
            {666, 1, new AudioFormat(8000.f, 16, 2, true, true), null, null},
            //Top boundary for destination port
            {666, 0xFFFF - 1, new AudioFormat(8000.f, 16, 2, true, true), null, null},
            {666, 0xFFFF, new AudioFormat(8000.f, 16, 2, true, true), null, null},
            {666, 0xFFFF + 1, new AudioFormat(8000.f, 16, 2, true, true), IllegalArgumentException.class, null},
            //Null for audio format
            {666, 666, null, IllegalArgumentException.class, null}
        });
    }

    /**
     * Test of getFormat method, of class AudioHeader.
     */
    @Test
    public void testConstructor() {
        System.out.println(String.format("Constructor(%d, %d, %s)",
                this.src_port, this.dst_port, this.format));

        //setup expected exception
        if (expectedException != null) {
            thrown.expect(expectedException);
            //thrown.expectMessage(expectedExceptionMsg);
        }

        AudioHeader instance = new AudioHeader(src_port, dst_port, format);
        assertEquals(src_port, instance.getSrcPort());
        assertEquals(dst_port, instance.getRcvPort());
        assertTrue(format.matches(instance.getFormat()));
    }

    /**
     * Test of setFormat method, of class AudioHeader.
     */
    @Test
    public void testSetFormat() {
        System.out.println(String.format("setFormat(%s)",
                this.format));

        //setup expected exception
        if (expectedException != null) {
            thrown.expect(expectedException);
            //thrown.expectMessage(expectedExceptionMsg);
        }

        AudioFormat f = new AudioFormat(44100, 8, 1, false, false);
        AudioHeader instance = new AudioHeader(src_port, dst_port, f);
        instance.setFormat(format);
        assertTrue(format.matches(instance.getFormat()));
    }

    @Test
    public void testSetSrcPort() {
        System.out.println(String.format("setSrcPort(%d)",
                this.src_port));

        //setup expected exception
        if (expectedException != null) {
            thrown.expect(expectedException);
            //thrown.expectMessage(expectedExceptionMsg);
        }

        AudioHeader instance = new AudioHeader(1000, dst_port, format);
        instance.setSrcPort(src_port);
        assertEquals(this.src_port, instance.getSrcPort());

    }

    @Test
    public void testSetDstPort() {
        System.out.println(String.format("setDstPort(%d)",
                this.dst_port));

        //setup expected exception
        if (expectedException != null) {
            thrown.expect(expectedException);
            //thrown.expectMessage(expectedExceptionMsg);
        }

        AudioHeader instance = new AudioHeader(src_port, 1000, format);
        instance.setRcvPort(dst_port);
        assertEquals(this.dst_port, instance.getRcvPort());
    }

}
