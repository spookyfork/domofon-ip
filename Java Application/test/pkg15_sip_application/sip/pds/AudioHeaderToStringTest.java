/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg15_sip_application.sip.pds;

import java.util.Arrays;
import javax.sound.sampled.AudioFormat;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

/**
 *
 * @author rand
 */
@RunWith(Parameterized.class)
public class AudioHeaderToStringTest {

    @Parameter(0)
    public AudioHeader header;
    @Parameter(1)
    public String expectedResult;
    @Parameter(2)
    public Class<? extends Exception> expectedException;
    @Parameter(3)
    public String expectedExceptionMsg;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Parameters
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
            //
            {new AudioHeader(333, 666, new AudioFormat(8000.f, 16, 2, true, true)), 
                "333 666 8000 16 2 true true", null, null},
            {new AudioHeader(1, 2, new AudioFormat(16000.12f, 32, 1, false, false)), 
                "1 2 16000 32 1 false false", null, null}
        });
    }

    /**
     * Test of toString method, of class AudioHeader.
     */
    @Test
    public void testToString() {
        String result = header.toString();
        assertEquals(expectedResult, result);
    }

}
