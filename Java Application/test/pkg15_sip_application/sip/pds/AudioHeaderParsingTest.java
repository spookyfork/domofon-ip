/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg15_sip_application.sip.pds;

import java.util.Arrays;
import javax.sound.sampled.AudioFormat;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import pkg15_sip_application.exception.MalformedPacketException;

/**
 *
 * @author rand
 */
@RunWith(Parameterized.class)
public class AudioHeaderParsingTest {

    @Parameter(0)
    public String headerString;
    @Parameter(1)
    public AudioHeader expectedResult;
    @Parameter(2)
    public Class<? extends Exception> expectedException;
    @Parameter(3)
    public String expectedExceptionMsg;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Parameters
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
            //regular data
            {"111 222 8000 16 2 true true", new AudioHeader(111, 222, new AudioFormat(8000, 16, 2, true, true)), null, null},
            {"123 321 44100 8 1 false false", new AudioHeader(123, 321, new AudioFormat(44100, 8, 1, false, false)), null, null},
            {"0 0 16000 32 3 0 1", new AudioHeader(0, 0, new AudioFormat(16000, 32, 3, false, true)), null, null},
            //more whitespaces
            {"111  222  8000  16  2  true    true", new AudioHeader(111, 222, new AudioFormat(8000, 16, 2, true, true)), null, null},
            {"\t123\t321\t44100\t8\t1\tfalse\tfalse", new AudioHeader(123, 321, new AudioFormat(44100, 8, 1, false, false)), null, null},
            {"     0 0 16000 32 3 0 1    ", new AudioHeader(0, 0, new AudioFormat(16000, 32, 3, false, true)), null, null},
            //edge port values
            {"-1 222 8000 16 2 true true", null, MalformedPacketException.class, null},
            {"0 222 8000 16 2 true true", new AudioHeader(0, 222, new AudioFormat(8000, 16, 2, true, true)), null, null},
            {"1 222 8000 16 2 true true", new AudioHeader(1, 222, new AudioFormat(8000, 16, 2, true, true)), null, null},
            {"123 -1 44100 8 1 false false", null, MalformedPacketException.class, null},
            {"123 0 44100 8 1 false false", new AudioHeader(123, 0, new AudioFormat(44100, 8, 1, false, false)), null, null},
            {"123 1 44100 8 1 false false", new AudioHeader(123, 1, new AudioFormat(44100, 8, 1, false, false)), null, null},
            {"65534 222 8000 16 2 true true", new AudioHeader(65534, 222, new AudioFormat(8000, 16, 2, true, true)), null, null},
            {"65535 222 8000 16 2 true true", new AudioHeader(65535, 222, new AudioFormat(8000, 16, 2, true, true)), null, null},
            {"65536 222 8000 16 2 true true", null, MalformedPacketException.class, null},
            {"123 65534 44100 8 1 false false", new AudioHeader(123, 65534, new AudioFormat(44100, 8, 1, false, false)), null, null},
            {"123 65535 44100 8 1 false false", new AudioHeader(123, 65535, new AudioFormat(44100, 8, 1, false, false)), null, null},
            {"123 65536 44100 8 1 false false", null, MalformedPacketException.class, null},
            // audio format
            {"111 222 -8000 16 2 true true", null, MalformedPacketException.class, null},
            {"111 222 8000.00 16 2 true true", null, NumberFormatException.class, null},
            {"111 222 0 16 2 true true", null, MalformedPacketException.class, null},
            {"111 222 8000 -16 2 true true", null, MalformedPacketException.class, null},
            {"111 222 8000 0 2 true true", null, MalformedPacketException.class, null},
            {"111 222 8000 1 2 true true", new AudioHeader(111, 222, new AudioFormat(8000, 1, 2, true, true)), null, null},
            {"111 222 8000 16 -1 true true", null, MalformedPacketException.class, null},
            {"111 222 8000 16 0 true true", null, MalformedPacketException.class, null},
            {"111 222 8000 16 1 true true", new AudioHeader(111, 222, new AudioFormat(8000, 16, 1, true, true)), null, null},
            //malformed data (wrong amount of params, invalid types, characters swapped)
            {"111.0 222 8000 16 2 true true", null, NumberFormatException.class, null},
            {"111 222.0 8000 16 2 true true", null, NumberFormatException.class, null},
            {"111 222 8000 16.0 2 true true", null, NumberFormatException.class, null},
            {"111 222 8000 16 2.0 true true", null, NumberFormatException.class, null},
            {"1c11 222 8000 16 2 true true", null, NumberFormatException.class, null},
            {"111 22c2 8000 16 2 true true", null, NumberFormatException.class, null},
            {"111 222 80c00 16 2 true true", null, NumberFormatException.class, null},
            {"111 222 8000 1c6 2 true true", null, NumberFormatException.class, null},
            {"111 222 8000 16 2c true true", null, NumberFormatException.class, null},
            {"111 222 8000 16 2 truce true", null, MalformedPacketException.class, null},
            {"111 222 8000 16 2 true truce", null, MalformedPacketException.class, null},});
    }

    /**
     * Test of parse method, of class AudioHeader.
     */
    @Test
    public void testParse() {
        System.out.println(String.format("parse(\"%s\")", headerString));
        //setup expected exception
        if (expectedException != null) {
            thrown.expect(expectedException);
            //thrown.expectMessage(expectedExceptionMsg);
        }
        AudioHeader result = AudioHeader.parse(headerString);
        assertEquals(expectedResult, result);
    }

}
