/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.Color;
import java.awt.Graphics;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import javax.sound.sampled.AudioFormat;
import pkg15_sip_application.audio.TargetLineListener;

/**
 *
 * @author rand
 */
public class AudioLevelMeter extends javax.swing.JPanel implements TargetLineListener {

    private int windowMS = 100;

    private float RMS = 0f;
    private float peak = 0f;
    private float topDB = 0f;
    private float bottomDB = -40f;

    private byte[] audioBuffer;
    private Buffer wrapper;

    private float samplingRate;
    private boolean bigEndian;

    public AudioLevelMeter() {
        this(8000.0f, true);
    }

    public AudioLevelMeter(float rate, boolean bigEndian) {
        this.bigEndian = bigEndian;
        this.samplingRate = rate;
    }

    public void setAudioFormat(AudioFormat format) {
        this.bigEndian = format.isBigEndian();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.blue);
        g.fillRect(0, 0, getWidth(), getHeight());
        g.setColor(Color.GREEN);
        g.fillRect(0, (int) ((1f - RMS) * getHeight()), getWidth(), (int) (RMS * getHeight()) + 1);
        g.setColor(Color.red);
        g.drawLine(0, (int) ((1f - peak) * getHeight()), getWidth(), (int) ((1f - peak) * getHeight()));

    }

    private void calculateRMS(Buffer wrapper, int size) {
        long sumSq = 0;
        int shortPeak = 0;
        wrapper.rewind();
        int sizeLeft = size;
        if (wrapper instanceof ShortBuffer) {
            ShortBuffer sw = (ShortBuffer) wrapper;
            sw.rewind();
            while (sw.hasRemaining() && sizeLeft-- > 0) {
                short val = sw.get();
                sumSq += val * (long) val;
                if (shortPeak < Math.abs(val)) {
                    shortPeak = Math.abs(val);
                }
            }
        }
        double shortRMS = Math.pow(sumSq / size, 0.5d);

        RMS = (float) (10 * Math.log10(shortRMS / Short.MAX_VALUE));
        RMS = 1 - (topDB - RMS) / (topDB - bottomDB);
        peak = shortPeak / (float) Short.MAX_VALUE;
    }

    @Override
    public void onLineOpen() {

    }

    @Override
    public void onLineClose() {

    }

    @Override
    public void onChunkReceive(byte[] data) {
        ShortBuffer buff = ByteBuffer.wrap(data).order(
                bigEndian ? ByteOrder.BIG_ENDIAN : ByteOrder.LITTLE_ENDIAN
        ).asShortBuffer();
        calculateRMS(buff, buff.remaining());
        repaint();
    }

}
