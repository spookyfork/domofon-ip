/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.SourceDataLine;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 *
 * @author rand
 */
public class OutputPanel extends javax.swing.JPanel {

    private DefaultComboBoxModel<javax.sound.sampled.Mixer.Info> mixerModel;
    private DefaultComboBoxModel<javax.sound.sampled.AudioFormat> formatModel;


    /**
     * Creates new form OutputPanel
     */
    public OutputPanel() {
        initComponents();

        cbDevice.setRenderer(new ListCellRenderer<Mixer.Info>() {
            @Override
            public Component getListCellRendererComponent(JList<? extends Mixer.Info> list, Mixer.Info value, int index, boolean isSelected, boolean cellHasFocus) {
                DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();
                JLabel renderer = (JLabel) defaultRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                renderer.setText(value.getName());
                return renderer;
            }
        });
        cbFormat.setRenderer(new ListCellRenderer<javax.sound.sampled.AudioFormat>() {
            @Override
            public Component getListCellRendererComponent(JList<? extends javax.sound.sampled.AudioFormat> list, javax.sound.sampled.AudioFormat value, int index, boolean isSelected, boolean cellHasFocus) {
                DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();
                JLabel renderer = (JLabel) defaultRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                renderer.setText(value.getEncoding().toString() + ", " + (value.getChannels() == 1 ? "mono" : "stereo") + ", " +
                        value.getSampleSizeInBits() + "bit, " + value.getFrameSize() +" bytes/frame, " +
                        (value.isBigEndian() ? "BE" : "LE"));
                return renderer;
            }
        });
        initPanel();
    }

    void initPanel() {
        mixerModel = new DefaultComboBoxModel<>();
        formatModel = new DefaultComboBoxModel<>();
        //get Devices 
        for (Mixer.Info info : AudioSystem.getMixerInfo()) {
            Mixer mixer = AudioSystem.getMixer(info);
            for (javax.sound.sampled.Line.Info sinfo : mixer.getSourceLineInfo()) {
                if (sinfo instanceof DataLine.Info && ((DataLine.Info) sinfo).getLineClass().equals(SourceDataLine.class)) {
                    if(((DataLine.Info) sinfo).getFormats().length > 0){
                        mixerModel.addElement(info);
                    }
                }
            }
        }

        cbDevice.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                formatModel.removeAllElements();
                javax.sound.sampled.Mixer.Info info = (javax.sound.sampled.Mixer.Info) ((JComboBox) e.getSource()).getSelectedItem();
                javax.sound.sampled.Mixer mixer = AudioSystem.getMixer(info);
                for (javax.sound.sampled.Line.Info sinfo : mixer.getSourceLineInfo()) {
                    if (sinfo instanceof DataLine.Info && ((DataLine.Info) sinfo).getLineClass().equals(SourceDataLine.class)) {
                        for (AudioFormat format : ((DataLine.Info) sinfo).getFormats()) {
                            formatModel.addElement(format);
                        }
                    }
                }
            }
        });

        cbDevice.setModel(mixerModel);
        cbFormat.setModel(formatModel);
        cbDevice.setSelectedIndex(0);
    }

        
    public Mixer.Info getMixerInfo(){
        return cbDevice.getItemAt(cbDevice.getSelectedIndex());
    }
    
    public AudioFormat getAudioFormat(){
        //return cbFormat.getItemAt(cbFormat.getSelectedIndex());
        AudioFormat format = cbFormat.getItemAt(cbFormat.getSelectedIndex());
        float freq  = Float.parseFloat(tfFreq.getText());
        return new AudioFormat(format.getEncoding(),
                freq, 
                format.getSampleSizeInBits(),
                format.getChannels(), 
                format.getFrameSize(), 
                freq,
                format.isBigEndian(),
                format.properties());
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        cbDevice = new javax.swing.JComboBox<>();
        cbFormat = new javax.swing.JComboBox<>();
        tfFreq = new javax.swing.JTextField();

        java.awt.GridBagLayout layout = new java.awt.GridBagLayout();
        layout.columnWeights = new double[] {0.0, 1.0};
        setLayout(layout);

        jLabel1.setText("Device");
        add(jLabel1, new java.awt.GridBagConstraints());

        jLabel2.setText("Format");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        add(jLabel2, gridBagConstraints);

        jLabel3.setText("SamplingFrequency");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        add(jLabel3, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(cbDevice, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(cbFormat, gridBagConstraints);

        tfFreq.setText("8000");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(tfFreq, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<javax.sound.sampled.Mixer.Info> cbDevice;
    private javax.swing.JComboBox<javax.sound.sampled.AudioFormat> cbFormat;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JTextField tfFreq;
    // End of variables declaration//GEN-END:variables
}
