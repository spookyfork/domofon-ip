/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg15_sip_application.exception;

/**
 *
 * @author rand
 */
public class AlreadyInUseException extends RuntimeException{

    public AlreadyInUseException() {
        super();
    }

    public AlreadyInUseException(String message) {
        super(message);
    }
   
}
