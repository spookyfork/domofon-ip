/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg15_sip_application.audio;

import java.lang.reflect.Array;

/**
 *
 * end points to first empty if end = start -> empty if end + 1 = start -> full
 *
 * @author rand
 */
public class RingBuffer {

    private float[] buffer;
    private int size;
    private int start, end;

    public RingBuffer(int size) {
        this.size = size;
        buffer = new float[size + 1];
    }

    public void resize(int newSize) throws IllegalArgumentException{
        if(newSize <=0)
            throw new IllegalArgumentException("Size needs to be a positive number");
        float[] newBuffer;
        newBuffer = new float[newSize + 1];
        //System.arraycopy(buffer, 0, newBuffer, 0, Math.min(newSize, size));
        size = newSize;
        buffer = newBuffer;
    }

    private void incEnd() {
        end = (end + 1) % buffer.length;
    }

    private void incStart() {
        start = (start + 1) % buffer.length;
    }

    public boolean isEmpty() {
        return start == end;
    }

    public boolean isFull() {
        return start == (end + 1) % buffer.length;
    }

    public int size() {
        return modulus(end - start, buffer.length);
    }
    
    public int getBufferSize(){
        return buffer.length;
    }

    public int spaceLeft() {
        return buffer.length - modulus(end - start, buffer.length);
    }

    public boolean push(float val) {
        if (!isFull()) {
            buffer[end] = val;
            incEnd();
            return true;
        } else {
            return false;
        }
    }

    public int push(Float[] array) {
        return push(array, 0, array.length);
    }

    public int push(Float[] array, int len) {
        return push(array, 0, len);
    }

    public int push(Float[] array, int offset, int len) {
        int i;
       
        for(i=offset; i<offset + len && !isFull() && i < array.length; i++){
            push(array[i]);
        }
        
        return i - offset;
    }

    public Float pop() {
        if (!isEmpty()) {
            Float res = (Float) buffer[start];
            incStart();
            return res;
        } else {
            return null;
        }
    }
    
    public Float[] pop(int len){
        int actLen = Math.min(size(), len);
        
        Float[] r =  new Float[len];
        int i = 0;
        while(i < len){
            if(i < actLen)
                r[i++] = pop();
            else 
                r[i++] = 0.f;
        }
        return r;
    }
    
    public static int modulus(int a, int b) {
        return (a % b + b) % b;
    }

    Float get(int i){
        if(i<=size()){
            return (Float) buffer[(start + i) % buffer.length];
        } else 
            //return 0.f;
            throw new IndexOutOfBoundsException("Index " + i + " exceeds the number of currently holding elements "+ size());
    }

    void clear() {
        start = end = 0;
    }

}
