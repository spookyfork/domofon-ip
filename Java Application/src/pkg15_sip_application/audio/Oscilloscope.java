/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg15_sip_application.audio;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import javax.sound.sampled.AudioFormat;
import javax.swing.JPanel;

/**
 *
 * @author rand
 */
public class Oscilloscope extends JPanel implements Runnable, TargetLineListener {

    private volatile RingBuffer samples;

    private volatile RingBuffer windowBuffer;

    private volatile int timeWindow; // in ms
    private volatile float samplingRate;
    private volatile float yScale;
    private volatile int audioFormatChannels;
    private volatile int displayChannels;

    private int[] xPoints;
    private int[] yPoints;
    private long samplesToDraw;

    private volatile Thread thisThread;
    private volatile boolean shouldClose = false;
    private boolean bigEndian;

    public Oscilloscope() {
        this(300, 8000, 1.0f, 1, false);
    }

    public Oscilloscope(int timeWindow, float samplingRate, float yScale, int channels, boolean bigEndian) {
        this.timeWindow = timeWindow;
        this.samplingRate = samplingRate;
        this.yScale = yScale;
        this.audioFormatChannels = channels;
        this.bigEndian = bigEndian;
        
        recalculate();

        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent event) {
                //TODO: Create new arrays for x and y points
                //
            }
        });
    }

    private void recalculate() {
        samples = new RingBuffer((int) samplingRate * audioFormatChannels * 5);
        windowBuffer = new RingBuffer((int) samplingRate * audioFormatChannels * 5);
        samplesToDraw = (long) (timeWindow * samplingRate / 1000);
        fillSilence();
        //How many samples to draw based on window
        xPoints = new int[(int) samplesToDraw];
        yPoints = new int[(int) samplesToDraw];
    }

    public synchronized void clear() {
        samples.clear();
        fillSilence();
    }

    private synchronized void fillSilence() {
        //Push silenece at the start so the signal appears from the right
        while (windowBuffer.size() < samplesToDraw * audioFormatChannels) {
            windowBuffer.push(0f);
        }
    }

    public int getTimeWindow() {
        return timeWindow;
    }

    public void setTimeWindow(int timeWindow) {
        this.timeWindow = timeWindow;
        recalculate();
    }

    public float getSamplingRate() {
        return samplingRate;
    }

    public void setSamplingRate(float samplingRate) {
        this.samplingRate = samplingRate;
        recalculate();
    }

    public float getScale() {
        return yScale;
    }

    public void setScale(float yScale) {
        this.yScale = yScale;
    }

    public int getChannels() {
        return audioFormatChannels;
    }

    public void setChannels(int channels) {
        this.audioFormatChannels = channels;
        recalculate();
    }

    public void setBigEndian(boolean bigEndian) {
        this.bigEndian = bigEndian;
    }

    public void setAudioFormat(AudioFormat format) {
        setChannels(format.getChannels());
        setSamplingRate(format.getSampleRate());
        setBigEndian(format.isBigEndian());
    }

    /**
     * Use it like SourceDataLine.write() as if you were writing to audio output
     *
     * @param samples
     */
    int write(Float[] b, int off, int len) {
        synchronized (this) {
            samples.push(b, off, len);
        }
        //return i - off;
        return len;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        //Background
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, getWidth(), getHeight());

        g.setColor(Color.GREEN);
        int width = getWidth();
        int height = getHeight();

        float pixelsPerSample = (float) width / samplesToDraw;
        //FIXME:move allocation so it is created once only 
        //Limit drawing to only few samples per pixel
        float currentX = 0;
        //Draw samples

        synchronized (this) {
            for (int i = 0; i < samplesToDraw; i++) {
                xPoints[i] = (int) currentX;
                if (windowBuffer.size() > i * audioFormatChannels) {
                    yPoints[i] = (int) ((windowBuffer.get(i * audioFormatChannels) * yScale + 0.5f) * height);
                } else {
                    yPoints[i] = height / 2;
                }
                currentX += pixelsPerSample;
            }
        }

        g.drawPolyline(xPoints, yPoints, xPoints.length);
    }

    /**
     * Samples should be consumed "from the right side"
     *
     * How to achieve this -- double buffer: - double buffer is always updated
     * so it will fill itself with silence:
     *
     */
    @Override
    public void run() {
        long calculationStart, calculationEnd;
        long currentTime, lastTime;
        float sleepTime = 1000f / 60;
        lastTime = System.currentTimeMillis();
        while (!shouldClose && !Thread.currentThread().isInterrupted()) {
            try {
                //get time
                calculationStart = currentTime = System.currentTimeMillis();
                //draw
                repaint();
                //"consume" lastTime-currentTime worth of samples
                synchronized (this) {
                    windowBuffer.pop((int) ((currentTime - lastTime) * samplingRate * audioFormatChannels / 1000));
                    Float[] s = samples.pop((int) ((currentTime - lastTime) * samplingRate * audioFormatChannels / 1000));
                    windowBuffer.push(s);
                }
                //get time
                calculationEnd = System.currentTimeMillis();
                //sleep for 1/60 of second - time spent drawing
                Thread.sleep(Math.max(0l, (long) (sleepTime - (calculationEnd - calculationStart))));
                lastTime = currentTime;
            } catch (InterruptedException ex) {
                //Logger.getLogger(Oscilloscope.class.getName()).log(Level.SEVERE, null, ex);
                break;
            }
        }
        clear();
    }

    public void start() {
        synchronized (this) {
            if (thisThread == null) {
                shouldClose = false;
                thisThread = new Thread(this);
                thisThread.start();
            }
        }
    }

    public synchronized void close() {
        if (thisThread != null) {
            shouldClose = true;
        }
    }

    @Override
    public void onChunkReceive(byte[] data) {
        Float[] floatSamples = new Float[data.length / 2];
        int i = 0;
        ShortBuffer sb = ByteBuffer.wrap(data).order(
                bigEndian ? ByteOrder.BIG_ENDIAN : ByteOrder.LITTLE_ENDIAN
        ).asShortBuffer();
        while (sb.hasRemaining()) {
            floatSamples[i++] = sb.get() / (float) Short.MAX_VALUE;
        }
        write(floatSamples, 0, floatSamples.length);
    }

    @Override
    public void onLineOpen() {

    }

    @Override
    public void onLineClose() {

    }

}
