/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg15_sip_application.audio;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.TargetDataLine;

/**
 *
 * @author rand
 */
public class TargetLineWrapper extends AudioSourceWrapper{
    
    private AudioFormat format;
    private TargetDataLine line;
    private Mixer mixer;
    
    private int timeWindowMS = 10;
    private int chunkSize;
    private volatile byte[] dataBuffer;
       

    public TargetLineWrapper(AudioFormat format, Mixer.Info mixerInfo) throws LineUnavailableException {
        this.format = format;
        this.mixer = AudioSystem.getMixer(mixerInfo);
        this.line = AudioSystem.getTargetDataLine(format, mixerInfo);

    }
    
    public TargetLineWrapper(AudioFormat format) throws LineUnavailableException {
        this.format = format;
        this.mixer = null;
        this.line = AudioSystem.getTargetDataLine(format);

    }

    public int getTimeWindowMS() {
        return timeWindowMS;
    }

    public void setTimeWindowMS(int timeWindowMS) {
        this.timeWindowMS = timeWindowMS;
    }

    public int getChunkSize() {
        return chunkSize;
    }

    public Mixer getMixer() {
        return mixer;
    }
    
    public AudioFormat getAudioFormat(){
        return line.getFormat();
    }

    @Override
    protected void onOpen() throws LineUnavailableException {
        line.open();
        //get number of frames in the time window
        chunkSize = (int) (timeWindowMS * format.getFrameRate() / 1000);
        //get how many samples are in there (maybe getFrameSize)
        chunkSize = chunkSize * format.getChannels();
        //now calcucate how many bytes this is 
        chunkSize = chunkSize * ((format.getSampleSizeInBits() + 7) >> 3);
        dataBuffer = new byte[512];
    }

    @Override
    protected void onClose() {
        line.close();
    }

    @Override
    protected void onStart() {
        line.start();
    }

    @Override
    protected void onStop() {
        line.stop();
    }

    @Override
    protected byte[] read() {
        int realChunkSize = line.read(dataBuffer, 0, dataBuffer.length);
        return dataBuffer;
    }
}
