/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg15_sip_application.audio.net;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.LineUnavailableException;
import pkg15_sip_application.audio.AudioSourceWrapper;

/**
 *
 * @author rand
 */
public class AudioReceiver extends AudioSourceWrapper {

    DatagramPacket p;
    DatagramSocket sock;
    String host;
    int port = 6966; //FIXME: REad this from preferences ? 
    int remotePort = 0;
    volatile boolean exit = false;
    byte[] buffer;
    int chunkSize;

    public AudioReceiver(int port) {
        this(port, 1024);
    }

    public AudioReceiver(int port, int chunkSize) {
        this.port = port;
        this.chunkSize = chunkSize;
    }

    public AudioReceiver(int port, int remotePort, int chunkSize) {
        this.port = port;
        this.remotePort = remotePort;
        this.chunkSize = chunkSize;
    }

    public int getRemotePort() {
        return remotePort;
    }

    public void setRemotePort(int remotePort) {
        this.remotePort = remotePort;
    }

    public int getLocalPort() {
        if (this.sock == null) {
            return 0;
        }
        return this.sock.getLocalPort();
    }

    @Override
    public int getChunkSize() {
        return buffer.length;
    }

    @Override
    protected void onOpen() throws LineUnavailableException {
        buffer = new byte[1024];
        try {
            sock = new DatagramSocket(port);
            p = new DatagramPacket(buffer, buffer.length);
        } catch (SocketException ex) {
            Logger.getLogger(AudioReceiver.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void onClose() {
        sock.close();
    }

    @Override
    protected void onStart() {

    }

    @Override
    protected void onStop() {

    }

    @Override
    protected byte[] read() {
        try {
            sock.receive(p);
//            if (remotePort != 0 && p.getPort() == remotePort){
//                return null;
//            }
            return Arrays.copyOf(buffer, p.getLength());
        } catch (IOException ex) {
            //This happens when we wait for the next packet but the socket gets closed
            //Logger.getLogger(AudioReceiver.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
