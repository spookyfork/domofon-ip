/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg15_sip_application.audio.net;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;
import org.zoolu.tools.Pair;
import pkg15_sip_application.audio.AudioSourceWrapper;
import pkg15_sip_application.audio.SourceLineWrapper;
import pkg15_sip_application.audio.TargetLineWrapper;
import pkg15_sip_application.sip.pds.AudioHeader;
import pkg15_sip_application.sip.pds.VideoHeader;
import pkg15_sip_application.video.ImageReceiver;
import view.SIPApplicationWindow;

/**
 * FIXME: For now only one image receiver
 *
 * Based on PDS Headers or message create audio connections If Source port is 0
 * then do not create a sender and TargetLineWrapper if destination port is 0
 * then do not create receiver and SourceLineWrapper
 *
 * Scenario 1: - get Supported Audio/Video formats - decide what to offer (out
 * audio, in audio, out video, in video) for instance offer that you can receive
 * video, but not send it - based on the offer create receivers and senders and
 * get their ports - create PDS message with offer and ports - receive PDS
 * message with confirmation - close unused receivers and senders (the ones that
 * are not in PDS or those that have both ports set to 0 (dont send dont
 * receive) or both offer and confirmation is only for outgoing/incoming media -
 * set ports for sender and receiver from received PDS - start media transfer
 *
 * Scenario 2: - Receive INVITE with PDS message - Choose what audio or video
 * connection to use - Create senders/receivers for those connections - Create
 * PDS message with selected formats and created ports - Send PDS with 2xx
 * response - Start media transfer after receiving ACK
 *
 *
 * Create Session for given host Add different media connections for that given
 * host Start media transfer stop media transfer
 *
 *
 * TODO:Instead of throwing Exceptions for unavailable AudioFormat, take default
 * format and convert it for sending/receiving using: TargetDataLine tdl =
 * AudioSystem.getTargetDataLine(format1); AudioInputStream stream = new
 * AudioInputStream(tdl); AudioInputStream stream2 =
 * AudioSystem.getAudioInputStream(format2, stream);
 *
 *
 * There can be only one TargetDataLine Hold TargetLineWrapper separately Create
 * Map
 *
 * @author rand
 */
public class MediaTransferManager {

    private String host;
    private Map<AudioFormat, Map<Integer, Object>> connections;

    private static final Integer RECEIVER = 0;
    private static final Integer SENDER = 1;
    //private static final Integer TLW = 2; not needed since there can be only one
    private static final Integer SLW = 3;
    private TargetLineWrapper tlw; //There Can be only one!

    private Comparator<AudioFormat> comparator
            = (AudioFormat format1, AudioFormat format2) -> {
                if (format1.matches(format2)) {
                    return 0;
                }
                return format1.toString().compareTo(format2.toString());
            };

    public MediaTransferManager(String host) {
        this.host = host;
        connections = new TreeMap<>(comparator);
    }

    /**
     * Create connection for received offer
     *
     * @param offer
     * @return
     */
    public AudioHeader createAudioConnection(AudioHeader offer) throws LineUnavailableException {
        AudioHeader h = createAudioConnection(offer.getFormat(), offer.isIncomingStream(), offer.isOutgoingStream());
        bindAudioConnection(offer.getFormat(), offer.getRcvPort(), offer.getSrcPort());
        return h;
    }

    /**
     *
     * @param format
     * @param remote_src_port
     * @param remote_recv_port
     */
    public void bindAudioConnection(AudioFormat format, int remote_src_port, int remote_recv_port) {
        Map<Integer, Object> entry = connections.get(format);
        if (entry == null) {
            return;
            //FIXME: Make it an exception
        }
        AudioReceiver receiver = (AudioReceiver) entry.get(RECEIVER);
        AudioSender sender = (AudioSender) entry.get(SENDER);
        if (sender != null) {
            sender.setPort(remote_recv_port);
        }
        if (receiver != null) {
            receiver.setRemotePort(remote_src_port);
        }
    }

    public AudioHeader createAudioReceiver(AudioFormat format)
            throws LineUnavailableException {
        //check if it already exists in the map

        AudioReceiver receiver;
        SourceLineWrapper slw;

        Map<Integer, Object> entry = connections.get(format);
        if (entry == null) {
            //Create entry 
            entry = new TreeMap<>();
            connections.put(format, entry);
        }

        //Check if there is AudioReceiver and SourceLineWrapper for it 
        if (entry.containsKey(RECEIVER)) {
            receiver = (AudioReceiver) entry.get(RECEIVER);
        } else {
            receiver = new AudioReceiver(0);
            entry.put(RECEIVER, receiver);
        }

        if (entry.containsKey(SLW)) {
            slw = (SourceLineWrapper) entry.get(SLW);
        } else {
            slw = new SourceLineWrapper(AudioSystem.getSourceDataLine(format));
            entry.put(SLW, slw);
        }
        receiver.addTargetLineListener(slw);
        slw.open();
        receiver.open();

        int senderPort = entry.containsKey(SENDER)
                ? ((AudioSender) entry.get(SENDER)).getLocalPort() : 0;

        return new AudioHeader(senderPort, receiver.getLocalPort(), format);
    }

    public AudioHeader createAudioSender(AudioFormat format)
            throws LineUnavailableException {
        //check if it already exists in the map

        AudioSender sender;

        Map<Integer, Object> entry = connections.get(format);
        if (entry == null) {
            //Create entry 
            entry = new TreeMap<>();
            connections.put(format, entry);
        }

        //Check if there is AudioReceiver and SourceLineWrapper for it 
        if (entry.containsKey(SENDER)) {
            sender = (AudioSender) entry.get(SENDER);
        } else {
            sender = new AudioSender(host, 0);
            entry.put(SENDER, sender);
        }

        if (tlw == null) {
            tlw = new TargetLineWrapper(format);
        }

        tlw.addTargetLineListener(sender);
        tlw.open();

        int receiverPort = entry.containsKey(RECEIVER)
                ? ((AudioReceiver) entry.get(RECEIVER)).getLocalPort() : 0;

        return new AudioHeader(sender.getLocalPort(), receiverPort, format);
    }

    public AudioHeader createAudioSender(AudioFormat format, int port)
            throws LineUnavailableException {
        //check if it already exists in the map

        AudioSender sender;

        Map<Integer, Object> entry = connections.get(format);
        if (entry == null) {
            //Create entry 
            entry = new TreeMap<>();
            connections.put(format, entry);
        }

        //Check if there is AudioReceiver and SourceLineWrapper for it 
        if (entry.containsKey(SENDER)) {
            sender = (AudioSender) entry.get(SENDER);
        } else {
            sender = new AudioSender();
            entry.put(SENDER, sender);
        }

        if (tlw == null) {
            tlw = new TargetLineWrapper(format);
        }
        tlw.addTargetLineListener(sender);
        tlw.open();

        int receiverPort = entry.containsKey(RECEIVER)
                ? ((AudioReceiver) entry.get(RECEIVER)).getLocalPort() : 0;

        return new AudioHeader(sender.getLocalPort(), receiverPort, format);
    }

    public AudioHeader createAudioConnection(AudioFormat format)
            throws LineUnavailableException {
        createAudioSender(format);
        return createAudioReceiver(format);
    }

    public AudioHeader createAudioConnection(AudioFormat format,
            boolean sender, boolean receiver) throws LineUnavailableException {
        AudioHeader header = null;
        if (sender) {
            header = createAudioSender(format);
        }
        if (receiver) {
            header = createAudioReceiver(format);
        }
        return header;
    }

    /**
     * Create audio connections for given audio formats. For offer.
     *
     * @param formatsList
     * @return
     * @throws LineUnavailableException
     */
    public List<AudioHeader> createAudioConnection(List<AudioFormat> formatsList)
            throws LineUnavailableException {
        List<AudioHeader> headers = new ArrayList<>(formatsList.size());
        for (AudioFormat format : formatsList) {
            headers.add(this.createAudioConnection(format));
        }
        return headers;
    }

    public VideoHeader createVideoConnection() {
        return null;
    }

    public Map<AudioFormat, Pair>
            getAudioConnections() {
        return null;
    }

    public AudioReceiver getAudioReceiver(AudioFormat format) {
        return connections.containsKey(format)
                && connections.get(format).containsKey(RECEIVER)
                ? (AudioReceiver) connections.get(format).get(RECEIVER) : null;
    }

    public AudioSender getAudioSender(AudioFormat format) {
        return connections.containsKey(format)
                && connections.get(format).containsKey(SENDER)
                ? (AudioSender) connections.get(format).get(SENDER) : null;
    }

    public ImageReceiver getImageReceiver() {
        return null;
    }

    public Map<AudioFormat, AudioReceiver> getAudioReceivers() {
        Map<AudioFormat, AudioReceiver> result = new TreeMap<>(comparator);
        for (Map.Entry<AudioFormat, Map<Integer, Object>> entry
                : connections.entrySet()) {
            AudioFormat key = entry.getKey();
            Map<Integer, Object> val = entry.getValue();
            if (val.containsKey(RECEIVER)) {
                result.put(key, (AudioReceiver) val.get(RECEIVER));
            }
        }
        return result;
    }

    public Map<AudioFormat, AudioSender> getAudioSenders() {
        Map<AudioFormat, AudioSender> result = new TreeMap<>(comparator);
        for (Map.Entry<AudioFormat, Map<Integer, Object>> entry : connections.entrySet()) {
            AudioFormat key = entry.getKey();
            Map<Integer, Object> val = entry.getValue();
            if (val.containsKey(SENDER)) {
                result.put(key, (AudioSender) val.get(SENDER));
            }
        }
        return result;
    }

    public AudioSourceWrapper getInputAudioWrapper() {
        return tlw;
    }

    public List<AudioSourceWrapper> getOutputAudioWrappers() {
        List<AudioSourceWrapper> list = new ArrayList<>();
        for (Map.Entry<AudioFormat, Map<Integer, Object>> entry : connections.entrySet()) {
            Object key = entry.getKey();
            Map<Integer, Object> value = entry.getValue();
            if (value.get(RECEIVER) != null) {
                list.add((AudioSourceWrapper) value.get(RECEIVER));
            }
        }
        return list;
    }

    public void checkUnusedAndRemove(AudioFormat format) {
        Map<Integer, Object> entry = connections.get(format);
        if (entry == null) {
            return;
        }
        if (entry.get(RECEIVER) == null && entry.get(SENDER) == null
                && entry.get(SLW) == null) {
            connections.remove(format);
        }
    }

    public void closeAudioSender(AudioFormat format) {
        if (format == null) {
            throw new NullPointerException("AudioFormat cannot be null");
        }
        Map<Integer, Object> entry = connections.get(format);
        if (entry == null) {
            return;
        }
        AudioSender sender = (AudioSender) entry.get(SENDER);
        tlw.stop();
        tlw.close();
        entry.remove(SENDER);
        checkUnusedAndRemove(format);
    }

    public void closeAudioReceiver(AudioFormat format) {
        if (format == null) {
            throw new NullPointerException("AudioFormat cannot be null");
        }
        Map<Integer, Object> entry = connections.get(format);
        if (entry == null) {
            return;
        }
        AudioReceiver receiver = (AudioReceiver) entry.get(RECEIVER);
        SourceLineWrapper slw = (SourceLineWrapper) entry.get(SLW);
        receiver.stop();
        slw.start();
        receiver.close();
        slw.close();
        entry.remove(RECEIVER);
        entry.remove(SLW);
        checkUnusedAndRemove(format);
    }

    /**
     * Start all prepared media connections
     */
    public void startMediaTransfer() {
        if (tlw != null) {
            tlw.start();
        }
        for (Map.Entry<AudioFormat, Map<Integer, Object>> entry : connections.entrySet()) {
            Object key = entry.getKey();
            Map<Integer, Object> value = entry.getValue();

            AudioSender sender = (AudioSender) value.get(SENDER);
            AudioReceiver receiver = (AudioReceiver) value.get(RECEIVER);
            SourceLineWrapper slw = (SourceLineWrapper) value.get(SLW);

            if (slw != null) {
                slw.start();
            }
            if (receiver != null) {
                receiver.start();
            }
        }

    }

    /**
     * Start selected media connections
     *
     * @param formatsList
     */
    public void startMediaTransfer(List<AudioFormat> formatsList) {
        if (tlw != null) {
            tlw.start();
        }
        for (AudioFormat format : formatsList) {
            if (!connections.containsKey(format)) {
                continue;
            }

            try {
                AudioSender sender = (AudioSender) connections.get(format).get(SENDER);
                AudioReceiver receiver = (AudioReceiver) connections.get(format).get(RECEIVER);
                SourceLineWrapper slw = (SourceLineWrapper) connections.get(format).get(SLW);

                if (slw != null) {
                    slw.start();
                }
                if (receiver != null) {
                    receiver.start();
                }

            } catch (Exception ex) {
                Logger.getLogger(SIPApplicationWindow.class.getName()).log(Level.SEVERE, "Cannot Start media Transfer on format " + format, ex);
            }
        }
    }

    public void stopMediaTransfer() {
        for (Map.Entry<AudioFormat, Map<Integer, Object>> entry : connections.entrySet()) {
            Object key = entry.getKey();
            Map<Integer, Object> value = entry.getValue();

            AudioSender sender = (AudioSender) value.get(SENDER);
            AudioReceiver receiver = (AudioReceiver) value.get(RECEIVER);
            SourceLineWrapper slw = (SourceLineWrapper) value.get(SLW);

            if (slw != null) {
                slw.stop();
            }
            if (receiver != null) {
                receiver.stop();
            }
        }
        if (tlw != null) {
            tlw.stop();
        }
    }

    public void stopMediaTransfer(List<AudioFormat> formatsList) {
        for (AudioFormat format : formatsList) {
            if (!connections.containsKey(format)) {
                continue;
            }
            try {

                AudioSender sender = (AudioSender) connections.get(format).get(SENDER);
                AudioReceiver receiver = (AudioReceiver) connections.get(format).get(RECEIVER);
                SourceLineWrapper slw = (SourceLineWrapper) connections.get(format).get(SLW);

                if (slw != null) {
                    slw.stop();
                }
                if (receiver != null) {
                    receiver.stop();

                }

            } catch (Exception ex) {
                Logger.getLogger(SIPApplicationWindow.class
                        .getName()).log(Level.SEVERE, "Cannot Stop media Transfer on format " + format, ex);
            }
        }
        if (tlw != null) {
            tlw.stop();
        }
    }

    /**
     * Close media connections that are on the list
     *
     * @param formatsList
     */
    public void closeMediaTransfers() {
        for (Map.Entry<AudioFormat, Map<Integer, Object>> entry : connections.entrySet()) {
            Object key = entry.getKey();
            Map<Integer, Object> value = entry.getValue();

            AudioSender sender = (AudioSender) value.get(SENDER);
            AudioReceiver receiver = (AudioReceiver) value.get(RECEIVER);
            SourceLineWrapper slw = (SourceLineWrapper) value.get(SLW);

            if (slw != null) {
                slw.stop();
                slw.close();
            }
            if (receiver != null) {
                receiver.stop();
                receiver.stop();
            }
        }
        if (tlw != null) {
            tlw.stop();
            tlw.close();
        }
    }

    public void closeMediaTransfers(List<AudioFormat> formatsList) {
        for (AudioFormat format : formatsList) {
            if (!connections.containsKey(format)) {
                continue;
            }
            AudioSender sender = (AudioSender) connections.get(format).get(SENDER);
            AudioReceiver receiver = (AudioReceiver) connections.get(format).get(RECEIVER);
            SourceLineWrapper slw = (SourceLineWrapper) connections.get(format).get(SLW);

            if (receiver != null) {
                receiver.stop();
                receiver.close();
            }
            if (slw != null) {
                slw.stop();
                slw.close();
            }
        }
        //ONly close if there are no senders working 
        //Go through connections until one sender is found 
        boolean haveSenders = false;
        for (Map.Entry<AudioFormat, Map<Integer, Object>> entry : connections.entrySet()) {
            Object key = entry.getKey();
            Map<Integer, Object> value = entry.getValue();
            if(value.get(SENDER) != null){
                haveSenders = true;
                break;
            }
        }
        if (tlw != null && !haveSenders) {
            tlw.stop();
            tlw.close();
        }
    }

    /**
     * Close connections that are not on the given list
     *
     * @param formatsList
     */
    public void closeMediaTransfersDifference(List<AudioFormat> formatsList) {
        List<AudioFormat> toRemove = new ArrayList<>();
        for (Map.Entry<AudioFormat, Map<Integer, Object>> entry : connections.entrySet()) {
            AudioFormat key = entry.getKey();
            Map<Integer, Object> value = entry.getValue();

            //Cannot use equals or ==; ONLY MATCHES !
            //so we need to loop through the list to be sure 
            boolean contains = false;
            for (AudioFormat format : formatsList) {
                if (format.matches(key)) {
                    contains = true;
                    break;
                }
            }
            //Add to list for closing
            if (!contains) {
                toRemove.add(key);
            }
        }

        closeMediaTransfers(toRemove);
    }

    /**
     * Stop, close and remove all connections.
     */
    public void clear() {
        for (Map.Entry<AudioFormat, Map<Integer, Object>> entry : connections.entrySet()) {
            AudioFormat key = entry.getKey();
            Map<Integer, Object> value = entry.getValue();
            AudioSender sender = (AudioSender) value.get(SENDER);
            AudioReceiver receiver = (AudioReceiver) value.get(RECEIVER);
            SourceLineWrapper slw = (SourceLineWrapper) value.get(SLW);

            if (slw != null) {
                slw.stop();
                slw.close();
            }
            if (receiver != null) {
                receiver.stop();
                receiver.close();
            }
        }

        if (tlw != null) {
            tlw.stop();
            tlw.close();
            tlw = null;
        }
        connections.clear();
    }

    public void silence() {
        getAudioSenders().entrySet().forEach(entry -> {
            entry.getValue().silence();
        });
    }

    public void talk() {
        getAudioSenders().entrySet().forEach(entry -> {
            entry.getValue().talk();
        });
    }

}
