/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg15_sip_application.audio.net;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import pkg15_sip_application.audio.TargetLineListener;

/**
 *
 * @author rand
 */
public class AudioSender implements TargetLineListener {

    DatagramSocket sock;
    String host;
    private int port = 6966; //FIXME: Read this from preferences
    private volatile boolean pushToTalk;

    public AudioSender(String host, int port) {
        this.port = port;
        this.host = host;
        pushToTalk = false;
    }

    public AudioSender() {
        this.port = 0;
        this.host = "";
        pushToTalk = false;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getLocalPort() {
        if (sock != null) {
            return sock.getLocalPort();
        } else {
            return 0;
        }
    }

    @Override
    public void onChunkReceive(byte[] data) {
        DatagramPacket p;
        if (!pushToTalk) {
            return;
        }
        try {
            int sizeLeft = data.length;

            int packetSize = 1024;
            int offset = 0;
            while (sizeLeft > 0) {
                int realSize = (sizeLeft - packetSize) >= 0 ? packetSize : sizeLeft;
                p = new DatagramPacket(data, offset, realSize, InetAddress.getByName(host), port);
                sock.send(p);
                offset += packetSize;
                sizeLeft -= packetSize;
            }
        } catch (UnknownHostException ex) {
            Logger.getLogger(AudioSender.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AudioSender.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void onLineOpen() {
        try {
            sock = new DatagramSocket();
            sock.setSoTimeout(500);
        } catch (SocketException ex) {
            Logger.getLogger(AudioSender.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void onLineClose() {
        sock.close();
    }

    public void talk() {
        pushToTalk = true;
    }

    public void silence() {
        pushToTalk = false;
    }
}
