/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg15_sip_application.audio;

/**
 *
 * @author rand
 */
public interface TargetLineListener {
    
    public void onChunkReceive(byte[] data);
    public void onLineOpen();
    public void onLineClose();
}
