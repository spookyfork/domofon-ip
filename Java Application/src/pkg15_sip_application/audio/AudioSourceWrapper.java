/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg15_sip_application.audio;

import java.util.LinkedList;
import javax.sound.sampled.LineUnavailableException;

/**
 *
 * @author rand
 */
public abstract class AudioSourceWrapper implements Runnable {

    private final LinkedList<TargetLineListener> listeners;
    private volatile boolean shouldExit = false;
    private Thread myThread;
    
    private static int threadCounter = 0;

    public AudioSourceWrapper() {
        this.listeners = new LinkedList<>();
    }

    public abstract int getChunkSize();

    protected abstract void onOpen() throws LineUnavailableException;

    protected abstract void onClose();

    protected abstract void onStart();

    protected abstract void onStop();

    /**
     * Make sure all listeners are added before the audio source is opened.
     *
     * @throws javax.sound.sampled.LineUnavailableException
     */
    public void open() throws LineUnavailableException {
        onOpen();
        raiseOnLineOpen();
    }

    public void close() {
        onClose();
        raiseOnLineClose();
    }

    public void start() {
        onStart();
        shouldExit = false;
        myThread = new Thread(this, "AudioSourceWrapper-"+threadCounter++);
        myThread.start();
    }

    public void stop() {
        shouldExit = true;
        onStop();
    }

    protected abstract byte[] read();

    @Override
    public void run() {
        while (!shouldExit) {
            byte[] data = read();
            if (data != null) {
                raiseOnChunkReceived(data);
            }
        }
    }

    /**
     * Add listeners before the Audio Source is opened
     *
     * @param listener
     */
    public void addTargetLineListener(TargetLineListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    public void removeTargetLineListener(TargetLineListener listener) {
        listeners.remove(listener);
    }

    private void raiseOnChunkReceived(byte[] data) {
//        listeners.forEach(listener -> {
//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    listener.onChunkReceive(data);
//                }
//            }).start();
//        });
        //Creating thread for each is better cause no one listener can block all others, but it's also very slow
        //maybe some schedulers would be better
        listeners.forEach(l -> {
            l.onChunkReceive(data);
        });
    }

    private void raiseOnLineOpen() {
        listeners.forEach(listener -> {
            new Thread(listener::onLineOpen).start();
        });
    }

    private void raiseOnLineClose() {
        listeners.forEach(listener -> {
            new Thread(listener::onLineClose).start();
        });
    }
}
