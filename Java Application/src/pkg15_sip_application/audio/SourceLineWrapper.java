/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg15_sip_application.audio;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

/**
 *
 * @author rand
 */
public class SourceLineWrapper implements TargetLineListener{

    private SourceDataLine line;
    private AudioFormat audioFormat;

    public SourceLineWrapper(SourceDataLine line) {
        this.line = line;
    }
    
    public void open() throws LineUnavailableException{
        line.open();
    }
    
    public void open(AudioFormat format) throws LineUnavailableException{
        line.open(format);
    }
    
    public void close(){
        line.close();
    }
    
    public void start(){
        line.start();
    }
    
    public void stop(){
        line.stop();
    }
    
    @Override
    public void onChunkReceive(byte[] data) {
        //System.out.println("tes");
        if(line != null && line.isOpen()){
//            byte[] array = new byte[data.length];
//            int i = 0;
//            while(i < data.length)
//                array[i++] = data[i];
            line.write(data, 0, data.length);
        }
    }

    @Override
    public void onLineOpen() {
        
    }

    @Override
    public void onLineClose() {
        
    }

}
