/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg15_sip_application;

/**
 *
 * @author rand
 */
public interface SettingsListener {
    
    public void onSettingsChange(String name, Object value);
}
