/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg15_sip_application.sip;

import java.io.IOException;
import java.util.logging.Logger;
import java.net.BindException;
import java.net.Inet4Address;
import pkg15_sip_application.exception.AlreadyInUseException;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.prefs.Preferences;
import local.ua.MessageAgent;
import local.ua.MessageAgentListener;
import local.ua.UserAgentProfile;
import org.zoolu.sip.address.NameAddress;
import org.zoolu.sip.address.SipURL;
import org.zoolu.sip.call.Call;
import org.zoolu.sip.call.CallWatcher;
import org.zoolu.sip.call.CallWatcherListener;
import org.zoolu.sip.call.ExtendedCall;
import org.zoolu.sip.call.NotImplementedServer;
import org.zoolu.sip.call.OptionsServer;
import org.zoolu.sip.message.BaseMessageFactory;
import org.zoolu.sip.message.Message;
import org.zoolu.sip.message.SipMethods;
import org.zoolu.sip.provider.SipProvider;
import org.zoolu.sip.provider.Transport;
import org.zoolu.sip.provider.UdpTransport;
import org.zoolu.sip.transaction.TransactionClient;
import org.zoolu.sip.transaction.TransactionClientListener;
import org.zoolu.tools.DateFormat;
import org.zoolu.tools.Timer;
import org.zoolu.tools.TimerListener;
import pkg15_sip_application.sip.pds.PDSMessage;

/**
 *
 * @author rand
 */
public class CallAgent implements CallWatcherListener, TimerListener, MessageAgentListener {

    private static int DEFAULT_PORT = 5060;
    //private static String DEFAULT_UA_ADDRESS = "0.0.0.0";
    private static boolean DEFAULT_DO_REGISTER = false;

    Preferences prefs = Preferences.userNodeForPackage(CallAgent.class);

    public interface StateListener {

        public void onStateChange(int oldState, int newState);
    }

    private Consumer<String> logger;

    /**
     * This is needed if we want to free sockets
     */
    private UdpTransport transport;
    private SipProvider provider;
    private UserAgentProfile profile;
    private Call call;
    private MessageAgent ma;
    private OptionsServer options_server;
    private NotImplementedServer null_server;
    private CallWatcher ua_server;

    private Set<CallAgentListener> listeners = new HashSet<>();

    private int local_port = -1;
    private String address;

    private SipURL remote_url;

    private int status;
    public static final int STATUS_IDLE = 0;
    public static final int STATUS_CALLING = 1;
    public static final int STATUS_RINGING = 2;
    public static final int STATUS_ACTIVE = 3; //ACK RECEIVED or ACK SENT
    public static final int STATUS_INCOMING = 4;

    public CallAgent(SipProvider provider, UserAgentProfile profile) {
        this.local_port = provider.getPort();
        this.address = provider.getViaAddress();

        this.provider = provider;
        this.profile = profile;

        null_server = new NotImplementedServer(provider);
        ua_server = new CallWatcher(provider, this);
        ma = new MessageAgent(provider, profile, this);
        ma.receive();
        options_server = new OptionsServer(provider, "INVITE, ACK, CANCEL, OPTIONS, BYE, MESSAGE", "application/sw_domofon");
    }

    public CallAgent() throws UnknownHostException, IOException {
        start();
    }

    public CallAgent(int port) throws UnknownHostException, IOException {
        this.local_port = port;
        start();
    }

    public void halt() {
        if (transport != null) {
            transport.halt();
        }
        if (ua_server != null) {
            ua_server.halt();
        }
        if (ma != null) {
            ma.halt();
        }
        if (options_server != null) {
            options_server.halt();
        }
        if (null_server != null) {
            null_server.halt();
        }
        if (provider != null) {
            provider.halt();
        }

        transport = null;
        ua_server = null;
        ma = null;
        options_server = null;
        null_server = null;
        provider = null;
    }

    public final void start() throws UnknownHostException, BindException, IOException {
        this.address = java.net.Inet4Address.getLocalHost().getHostAddress();
        if (this.local_port < 0) {
            this.local_port = DEFAULT_PORT; //FIXME: make this read from preferences
        }

        if (transport == null) {
            transport = new UdpTransport(local_port);
            local_port = transport.getLocalPort();
        }
        if (profile == null) {
            profile = new UserAgentProfile();
            profile.ua_address = this.address;//FIXME: make this read from preferences
            profile.do_register = false;//FIXME: make this read from preferences
        }
        if (provider == null) {
            //FIXME: This may be incorrect way to get address
            Transport[] ta = {transport};
            provider = new SipProvider(address, transport.getLocalPort(), ta);
        }

        if (ua_server == null) {
            ua_server = new CallWatcher(provider, this);
        }
        if (ma == null) {
            ma = new MessageAgent(provider, profile, this);
            ma.receive();
        }
        if (options_server == null) {
            options_server = new OptionsServer(provider, "INVITE, ACK, CANCEL, OPTIONS, BYE, MESSAGE", "application/sw_domofon");
        }
        if (null_server == null) {
            null_server = new NotImplementedServer(provider);
        }
    }

    public void setUserAgentProfile(UserAgentProfile profile) {
        this.profile = profile;
    }

    public UserAgentProfile getUserAgentProfile() {
        return this.profile;
    }

    public void addCallAgentListener(CallAgentListener listener) {
        listeners.add(listener);
    }

    public void removeCallAgentListener(CallAgentListener listener) {
        listeners.remove(listener);
    }

    private void updateStatus(int newStatus) {
        if (this.status != newStatus) {
            int oldStatus = this.status;
            this.status = newStatus;
        }
    }

    public String getStatus() {
        if (call == null) {
            return "IDLE";
        }
        if (call.isIdle()) {
            return "IDLE";
        }
        if (call.isActive()) {
            return "ACTIVE";
        }
        if (call.isClosed()) {
            return "CLOSED";
        }
        if (call.isIncoming()) {
            return "INCOMING";
        }
        if (call.isOutgoing()) {
            return "OUTGOING";
        }
        return "IDLE";
    }

    public boolean isIncoming() {
        return call != null && call.isIncoming();
    }

    public boolean isOutgoing() {
        return call != null && call.isOutgoing();
    }

    public void fireOnCallEnded() {
        for (CallAgentListener l : listeners) {
//            new Thread(() -> {
//                l.onStateChange(oldState, newState);
//            }).start();
            l.onCallEnded();
        }
    }

    public void fireOnCallEstablished(String body) {
        for (CallAgentListener l : listeners) {
            l.onCallEstablished(body);
        }
    }

    public void fireOnCallIncoming(String body) {
        for (CallAgentListener l : listeners) {
            l.onCallIncoming(body);
        }
    }

    public void fireOnCallRinging() {
        for (CallAgentListener l : listeners) {
            l.onCallRinging();
        }
    }

    public void fireOnCallTimeout() {
        for (CallAgentListener l : listeners) {
            l.onCallTimeout();
        }
    }

    public void fireOnMessageDelivered(String body) {
        for (CallAgentListener l : listeners) {
            l.onMessageDelivered(body);
        }
    }

    public void fireOnMessageFailure(String body) {
        for (CallAgentListener l : listeners) {
            l.onMessageFailure(body);
        }
    }

    public void fireOnMessageReceived(String subject, String body) {
        for (CallAgentListener l : listeners) {
            l.onMessageReceived(subject, body);
        }
    }

    public void setLogger(Consumer<String> loggingFunction) {
        logger = loggingFunction;
    }

    public String getHost() {
        return remote_url.getHost();
    }

    public int getPort() {
        return this.local_port;
    }

    /* OPTIONS REQUEST */
    public void onOptionsProvisionalResponse(TransactionClient arg0, Message arg1) {
        log("OPTIONS transaction 1xx response");
    }

    public void onOptionsSuccessResponse(TransactionClient arg0, Message arg1) {
        log("OPTIONS transaction response: " + arg1.getAllowHeader().getValue());
    }

    public void onOptionsFailureResponse(TransactionClient arg0, Message arg1) {
        log("OPTIONS transaction failed");
    }

    public void onOptionsTimeout(TransactionClient arg0) {
        log("OPTIONS transaction timeout");
    }

    public void options(String host, int port) {
        options(host + ":" + port);
    }

    public void options(String contact) {
        NameAddress addr = new NameAddress(contact);
        Message msg = BaseMessageFactory.createRequest(this.provider, "OPTIONS", addr, this.profile.getUserURI(), "");

        new TransactionClient(provider, msg, new TransactionClientListener() {
            @Override
            public void onTransProvisionalResponse(TransactionClient arg0, Message arg1) {
                onOptionsProvisionalResponse(arg0, arg1);
            }

            @Override
            public void onTransSuccessResponse(TransactionClient arg0, Message arg1) {
                onOptionsSuccessResponse(arg0, arg1);
            }

            @Override
            public void onTransFailureResponse(TransactionClient arg0, Message arg1) {
                onOptionsFailureResponse(arg0, arg1);
            }

            @Override
            public void onTransTimeout(TransactionClient arg0) {
                onOptionsTimeout(arg0);
            }
        }).request();
    }

    /* CALL HANDLING METHODS */
    public void call(String host, int port) throws AlreadyInUseException, UnknownHostException {
        call(host + ":" + port);
    }

    public void call(String host, PDSMessage pds) throws AlreadyInUseException, UnknownHostException {
        if (call != null) {
            throw new AlreadyInUseException("Only one call can be handled at once.");
        }

        this.call = new ExtendedCall(this.provider, this.profile.getUserURI(), this.profile.auth_user, this.profile.auth_realm, this.profile.auth_passwd, this);
        String remote_user = host;
        if (host.lastIndexOf(":") == 3) {
            remote_user += ":" + 5060;//FIXME: make this read from preferences
        }
        if (!host.startsWith("sip:")) {
            remote_user = "sip:" + remote_user;
        }

        remote_url = new SipURL(remote_user);

        if (pds == null) {
            this.call.call(new NameAddress(remote_url));
        } else {
            Message invite = BaseMessageFactory.createRequest(provider, SipMethods.INVITE, new NameAddress(remote_url), new NameAddress(Inet4Address.getLocalHost().getHostAddress()), pds.toString());
            invite.setBody("application/pds", pds.toString());
            this.call.call(invite);
        }

        updateStatus(STATUS_CALLING);

        log("Calling " + remote_user);
    }

    public void call(String host) throws AlreadyInUseException, UnknownHostException {
        call(host, null);
    }

    public void hangup() {
        if (call == null) {
            return;
        }
        call.hangup();
        call = null;
        updateStatus(STATUS_IDLE);
        log("Hanging up");
    }

    public void accept() {
        if (call == null && !call.isIncoming()) {
            return;
        }
        call.accept("");
        updateStatus(STATUS_ACTIVE);
        log("Accepting");
    }

    public void accept(PDSMessage pds) {
        if (call == null && !call.isIncoming()) {
            return;
        }
        if (pds == null) {
            call.accept("");
        } else {
            call.accept(pds.toString());
        }
        updateStatus(STATUS_ACTIVE);
        log("Accepting with pds");
    }

    public void refuse() {
        if (call == null && !call.isIncoming()) {
            return;
        }
        call.refuse();
        call = null;
        updateStatus(STATUS_IDLE);
        log("Refusing");
    }

    /* LOGGING */
    public void log(String s) {
        Logger.getLogger(CallAgent.class.getCanonicalName()).log(Level.INFO, s);
        if (logger == null) {
            return;
        }
        logger.accept(DateFormat.formatHHMMSS(new Date()) + "--" + s.stripTrailing());
    }

    /*  TIMER LISTENER  */
    @Override
    public void onTimeout(Timer timer) {
        log("Timeout");
        fireOnCallTimeout();
    }

    /* MESSAGE AGENT LISTENER */
    @Override
    public void onMaReceivedMessage(MessageAgent ma, NameAddress sender, NameAddress recipient, String subject, String content_type, String body) {
        log("onMaReceivedMessage--" + subject);
        fireOnMessageReceived(subject, body);
    }

    @Override
    public void onMaDeliverySuccess(MessageAgent ma, NameAddress recipient, String subject, String result) {
        log("onMaDeliverySuccess--" + subject);
        fireOnMessageDelivered(result);
    }

    @Override
    public void onMaDeliveryFailure(MessageAgent ma, NameAddress recipient, String subject, String result) {
        log("onMaDeliveryFailure--" + subject);
        fireOnMessageFailure(result);
    }

    public void send(String host, String text) {
        ma.send(host, null, text);
        log("Sending Message: " + text);
    }

    public void send(String text) {
        ma.send(remote_url.getHost() + ":" + remote_url.getPort(), null, text);
        log("Sending Message: " + text);
    }

    /* CALL WATCHER LISTENER */
    /**
     * When the CallWatcher receives a new invite request that creates a new
     * Call.
     *
     * @param cw
     * @param extendedCall
     * @param callee
     * @param caller
     * @param sdp
     * @param invite
     */
    @Override
    public void onNewIncomingCall(CallWatcher cw, ExtendedCall extendedCall, NameAddress callee, NameAddress caller, String sdp, Message invite) {
        this.call = extendedCall;
        this.remote_url = new SipURL(invite.getRemoteAddress(), invite.getRemotePort());

        extendedCall.ring();
        updateStatus(STATUS_INCOMING);
        log("onNewIncomingCall--");
        fireOnCallIncoming(sdp);
    }


    /* EXTENDED CALL LISTENER */
    /**
     * Callback function called when arriving a new REFER method (transfer
     * request).
     *
     * @param call
     * @param refer_to
     * @param refered_by
     * @param refer
     */
    @Override
    public void onCallTransfer(ExtendedCall call, NameAddress refer_to, NameAddress refered_by, Message refer) {
        log("onCallTransfer--");
        /* UNUSED */
    }

    /**
     * Callback function called when arriving a new REFER method (transfer
     * request) with Replaces header, replacing an existing call.
     *
     * @param call
     * @param refer_to
     * @param refered_by
     * @param replcall_id
     * @param refer
     */
    @Override
    public void onCallAttendedTransfer(ExtendedCall call, NameAddress refer_to, NameAddress refered_by, java.lang.String replcall_id, Message refer) {
        log("onCallAttendedTransfer--");
        /* UNUSED */
    }

    /**
     * Callback function called when a call transfer is accepted.
     *
     * @param call
     * @param resp
     */
    @Override
    public void onCallTransferAccepted(ExtendedCall call, Message resp) {
        log("onCallTransferAccepted--");
        /* UNUSED */
    }

    /**
     * Callback function called when a call transfer is refused.
     *
     * @param call
     * @param reason
     * @param resp
     */
    @Override
    public void onCallTransferRefused(ExtendedCall call, java.lang.String reason, Message resp) {
        log("onCallTransferRefused--");
        /* UNUSED */
    }

    /**
     * Callback function called when a call transfer is successfully completed.
     *
     * @param call
     * @param notify
     */
    @Override
    public void onCallTransferSuccess(ExtendedCall call, Message notify) {
        log("onCallTransferSuccess--");
        /* UNUSED */
    }

    /**
     * Callback function called when a call transfer is NOT sucessfully
     * completed.
     *
     * @param call
     * @param reason
     * @param notify
     */
    @Override
    public void onCallTransferFailure(ExtendedCall call, java.lang.String reason, Message notify) {
        log("onCallTransferFailure--");
        /* UNUSED */
    }

    /* CALL LISTENER */
    /**
     * Callback function called when arriving a new INVITE method (incoming
     * call) This seems to never be called, use onNewIncomingCall instead.
     *
     * @param call
     * @param callee
     * @param caller
     * @param sdp
     * @param invite
     */
    @Override
    public void onCallInvite(Call call, NameAddress callee, NameAddress caller, java.lang.String sdp, Message invite) {
        log("onCallInvite--");
        /* UNUSED */
    }

    /**
     * Callback function called when arriving a new Re-INVITE method
     * (re-inviting/call modify)
     *
     * @param call
     * @param sdp
     * @param invite
     */
    @Override
    public void onCallModify(Call call, java.lang.String sdp, Message invite) {
        log("onCallModify--");
        /* UNUSED */
    }

    /**
     * Callback function called when arriving a 183 Session Progress
     *
     * @param call
     * @param resp
     */
    @Override
    public void onCallProgress(Call call, Message resp) {
        log("onCallProgress--");
        /* UNSUSED */
    }

    /**
     * Callback function called when arriving a 180 Ringing
     *
     * @param call
     * @param resp
     */
    @Override
    public void onCallRinging(Call call, Message resp) {
        log("onCallRinging--");
        updateStatus(STATUS_RINGING);
        fireOnCallRinging();
    }

    /**
     * Callback function called when arriving a 2xx (call accepted)
     *
     * @param call
     * @param sdp
     * @param resp
     */
    @Override
    public void onCallAccepted(Call call, java.lang.String sdp, Message resp) {
        log("onCallAccepted--");
        //call.ackWithAnswer("ackWithAnswer");
        updateStatus(STATUS_ACTIVE);
        fireOnCallEstablished(sdp);
    }

    /**
     * Callback function called when arriving a 4xx (call failure)
     *
     * @param call
     * @param reason
     * @param resp
     */
    @Override
    public void onCallRefused(Call call, java.lang.String reason, Message resp) {
        log("onCallRefused--");
        this.call = null;
        updateStatus(STATUS_IDLE);
        fireOnCallEnded();
    }

    /**
     * Callback function called when arriving a 3xx (call redirection)
     *
     * @param call
     * @param reason
     * @param contact_list
     * @param resp
     */
    @Override
    public void onCallRedirected(Call call, java.lang.String reason, java.util.Vector contact_list, Message resp) {
        log("onCallRedirected--");
        this.call = null;
        updateStatus(STATUS_IDLE);
        /* UNUSED */
    }

    /**
     * Callback function called when arriving an ACK method (call confirmed)
     *
     * @param call
     * @param sdp
     * @param ack
     */
    @Override
    public void onCallConfirmed(Call call, java.lang.String sdp, Message ack) {
        log("onCallConfirmed--");
        updateStatus(STATUS_ACTIVE);
        fireOnCallEstablished(null);
    }

    /**
     * Callback function called when the invite expires
     *
     * @param call
     */
    @Override
    public void onCallTimeout(Call call) {
        log("onCallTimeout--");
        call = null;
        updateStatus(STATUS_IDLE);
        fireOnCallTimeout();
    }

    /**
     * Callback function called when arriving a 2xx (re-invite/modify accepted)
     *
     * @param call
     * @param sdp
     * @param resp
     */
    @Override
    public void onCallReInviteAccepted(Call call, java.lang.String sdp, Message resp) {
        log("onCallReInviteAccepted--");
        /* UNUSED */
    }

    /**
     * Callback function called when arriving a 4xx (re-invite/modify failure)
     *
     * @param call
     * @param reason
     * @param resp
     */
    @Override
    public void onCallReInviteRefused(Call call, java.lang.String reason, Message resp) {
        log("onCallReInviteRefused--");
        /* UNUSED */

    }

    /**
     * Callback function called when a re-invite expires
     *
     * @param call
     */
    @Override
    public void onCallReInviteTimeout(Call call) {
        log("onCallReInviteTimeout--");
        /* UNUSED */
    }

    /**
     * Callback function called when arriving a CANCEL request
     *
     * @param call
     * @param msg
     */
    @Override
    public void onCallCancel(Call call, Message cancel) {
        log("onCallCancel--");
        this.call = null;
        updateStatus(STATUS_IDLE);
        fireOnCallEnded();
    }

    /**
     * Callback function called when arriving a BYE request
     *
     * @param call
     * @param bye
     */
    @Override
    public void onCallBye(Call call, Message bye) {
        log("onCallBye--");
        this.call = null;
        updateStatus(STATUS_IDLE);
        fireOnCallEnded();
    }

    /**
     * Callback function called when arriving a response for the BYE request
     * (call closed)
     *
     * @param call
     * @param resp
     */
    @Override
    public void onCallClosed(Call call, Message resp) {
        log("onCallClosed--");
        this.call = null;
        updateStatus(STATUS_IDLE);
        fireOnCallEnded();
    }

}
