/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg15_sip_application.sip;

/**
 *  Simplified combination of CallWatcherListener, TimerListener, MessageAgentListener
 * @author rand
 */
public interface CallAgentListener {
    
    /**
     * 0 IDLE 
     * 1 INCOMING CALL - SHOW A DIALOG WINDOW AND PLAY A RINGING SOUND
     * 2 MEDIA TRANSFER 
     * 3 OUTGOING CALL 180 - PLAY A TONE 
     * 
     * REFUSED, CANCEL, BYE, CLOSED - are effectively the same (correctly closed)
     * ACCEPTED, CONFIRMED - are the same (established)
     * TIMEOUT - will be used to show error
     */
    
    public abstract void onCallIncoming(String body);
    public abstract void onCallRinging();
    public abstract void onCallEstablished(String body); //received 2xx or ACK
    public abstract void onCallEnded(); //Session correctly closed
    public abstract void onCallTimeout(); //Error
    
    
    public abstract void onMessageReceived(String subject, String body);
    public abstract void onMessageDelivered(String body);
    public abstract void onMessageFailure(String body);
}
