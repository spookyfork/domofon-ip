/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg15_sip_application.sip.pds;

import java.util.Objects;
import pkg15_sip_application.exception.MalformedPacketException;
import pkg15_sip_application.video.ImageReceiver;

/**
 *
 * @author rand
 */
public class VideoHeader extends PDSHeader {

    private int video_height;
    private int video_width;
    private int packet_size;
    private ImageReceiver.ImageFormat video_format;
    private String video_proto;

    public VideoHeader(int video_src_port, int video_dst_port, int video_width, int video_height, int packet_size, ImageReceiver.ImageFormat video_format) {
        super(video_src_port, video_dst_port);
        setHeight(video_height);
        setWidth(video_width);
        setPacketSize(packet_size);
        setFormat(video_format);
    }

    public int getPacketSize() {
        return packet_size;
    }

    public final void setPacketSize(int packet_size) {
        if (packet_size <= 0) {
            throw new IllegalArgumentException("packet size has to be positive: " + packet_size);
        }
        this.packet_size = packet_size;
    }

    public int getHeight() {
        return video_height;
    }

    public final void setHeight(int video_height) {
        if (video_height <= 0) {
            throw new IllegalArgumentException("heigth has to be positive: " + video_height);
        }
        this.video_height = video_height;
    }

    public int getWidth() {
        return video_width;
    }

    public final void setWidth(int video_width) {
        if (video_width <= 0) {
            throw new IllegalArgumentException("width has to be positive: " + video_width);
        }
        this.video_width = video_width;
    }

    public ImageReceiver.ImageFormat getFormat() {
        return video_format;
    }

    public final void setFormat(ImageReceiver.ImageFormat video_format) {
        this.video_format = video_format;
    }

    public static VideoHeader parse(String value) {
        //<src_port> <recv_port> <width> <height> <packet size> <format>
        String tokens[] = value.strip().split(" ");
        //Either all tokens are there or only port with 0 
        if (tokens.length != 6) {
            throw new MalformedPacketException("Video header needs exactly 6 parameters");
        }

        int video_src_port = Integer.parseInt(tokens[0]);
        int video_dst_port = Integer.parseInt(tokens[1]);
        if (video_src_port < 0 || video_src_port > 65353) {
            throw new MalformedPacketException("Incorect video port");
        }
        if (video_dst_port < 0 || video_dst_port > 65353) {
            throw new MalformedPacketException("Incorect video port");
        }

        //FIXME
        //String video_proto = tokens[2];
        int width = Integer.parseInt(tokens[2]);
        if (width <= 0) {
            throw new MalformedPacketException("Incorect video width");
        }
        int height = Integer.parseInt(tokens[3]);
        if (height <= 0) {
            throw new MalformedPacketException("Incorect video height");
        }
        int packetSize = Integer.parseInt(tokens[4]);
        if (packetSize <= 0) {
            throw new MalformedPacketException("Incorect video packet size");
        }
        ImageReceiver.ImageFormat format;
        try {
            format = ImageReceiver.ImageFormat.valueOf(tokens[5]);
        } catch (IllegalArgumentException ex) {
            throw new MalformedPacketException("Unknown video format");
        }

        return new VideoHeader(video_src_port, video_dst_port, width, height, packetSize, format);
    }

    @Override
    public String toString() {
        return src_port + " "
                + rcv_port + " "
                + video_width + " "
                + video_height + " "
                + packet_size + " "
                + video_format;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + this.src_port;
        hash = 59 * hash + this.rcv_port;
        hash = 59 * hash + this.video_height;
        hash = 59 * hash + this.video_width;
        hash = 59 * hash + this.packet_size;
        hash = 59 * hash + Objects.hashCode(this.video_format);
        hash = 59 * hash + Objects.hashCode(this.video_proto);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final VideoHeader other = (VideoHeader) obj;
        if (this.src_port != other.src_port) {
            return false;
        }
        if (this.rcv_port != other.rcv_port) {
            return false;
        }
        if (this.video_height != other.video_height) {
            return false;
        }
        if (this.video_width != other.video_width) {
            return false;
        }
        if (this.packet_size != other.packet_size) {
            return false;
        }
        if (!Objects.equals(this.video_proto, other.video_proto)) {
            return false;
        }
        return this.video_format == other.video_format;
    }

}
