/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg15_sip_application.sip.pds;

import java.util.Objects;
import javax.sound.sampled.AudioFormat;
import pkg15_sip_application.exception.MalformedPacketException;

/**
 *
 * @author rand
 */
public class AudioHeader extends PDSHeader {

    private AudioFormat audio_format;
    private String audio_proto;

    public AudioHeader(int src_port, int dst_port, AudioFormat format) {
        super(src_port, dst_port);
        setFormat(format);
    }

    public AudioFormat getFormat() {
        return audio_format;
    }

    public final void setFormat(AudioFormat audio_format) {
        if (audio_format == null) {
            throw new IllegalArgumentException("audio format is null");
        }
        if (!validateFormat(audio_format)){
            throw new IllegalArgumentException("audio format is not valid: " + audio_format);
        }
        this.audio_format = audio_format;
    }
    
    private static boolean validateFormat(AudioFormat format){
        return format.getSampleRate() > 0 && format.getChannels() > 0 && 
                format.getSampleSizeInBits() > 0;
    }

    private static boolean parseBoolean(String s) {
        boolean value = false;
        if (s.equalsIgnoreCase("true")) {
            value = true;
        } else if (s.equalsIgnoreCase("false")) {
            value = false;
        } else {
            try {
                value = Integer.parseInt(s) > 0;
            } catch (NumberFormatException e) {
                throw new MalformedPacketException("Cannot parse boolean token: " + s);
            }
        }
        return value;
    }

    /**
     * 
     * @param value
     * @throws MalformedPacketException 
     * @throws NumberFormatException
     * @return 
     */
    public static AudioHeader parse(String value) throws MalformedPacketException, NumberFormatException {
        //<port> <port> <proto> <sample rate> <bit size> <channels> <signed> <endianness>
        String tokens[] = value.strip().split("\\s+");
        //Either all tokens are there or only port with 0 
        if (tokens.length != 7) {
            throw new MalformedPacketException("Audio header needs exactly 7 parameters");
        }
        
        int audio_src_port = Integer.parseInt(tokens[0]);
        int audio_dst_port = Integer.parseInt(tokens[1]);
        if (audio_src_port < 0 || audio_src_port > 0xFFFF) {
            throw new MalformedPacketException("Incorect audio port");
        }
        if (audio_dst_port < 0 || audio_dst_port > 0xFFFF) {
            throw new MalformedPacketException("Incorect audio port");
        }

        //FIXME
        //String audio_proto = tokens[2];
        float samplingRate = Integer.parseInt(tokens[2]);
        if (samplingRate <= 0) {
            throw new MalformedPacketException("Incorect audio sampling rate");
        }
        int bitSize = Integer.parseInt(tokens[3]);
        if (bitSize <= 0) {
            throw new MalformedPacketException("Incorect audio bit size");
        }
        int channels = Integer.parseInt(tokens[4]);
        if (channels <= 0) {
            throw new MalformedPacketException("Incorect audio channels");
        }
        boolean signed = AudioHeader.parseBoolean(tokens[5]);
        boolean bigEndian = AudioHeader.parseBoolean(tokens[6]);

        AudioFormat audio_format = new AudioFormat(samplingRate, bitSize, channels, signed, bigEndian);
        return new AudioHeader(audio_src_port, audio_dst_port, audio_format);
    }

    @Override
    public String toString() {
        return src_port + " "
                + rcv_port + " "
                + (int) audio_format.getSampleRate() + " "
                + audio_format.getSampleSizeInBits() + " "
                + audio_format.getChannels() + ' '
                + audio_format.getEncoding().equals(AudioFormat.Encoding.PCM_SIGNED) + ' '
                + audio_format.isBigEndian();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.src_port);
        hash = 97 * hash + Objects.hashCode(this.rcv_port);
        hash = 97 * hash + Objects.hashCode(this.audio_format);
        hash = 97 * hash + Objects.hashCode(this.audio_proto);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AudioHeader other = (AudioHeader) obj;
        if (!Objects.equals(this.src_port, other.src_port)) {
            return false;
        }
        if (!Objects.equals(this.rcv_port, other.rcv_port)) {
            return false;
        }
        if (!Objects.equals(this.audio_proto, other.audio_proto)) {
            return false;
        }
        return this.audio_format.matches(other.audio_format);
    }

}
