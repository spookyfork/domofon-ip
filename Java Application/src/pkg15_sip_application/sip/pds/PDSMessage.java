/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg15_sip_application.sip.pds;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pkg15_sip_application.exception.MalformedPacketException;

/**
 *
 * @author rand
 */
public class PDSMessage{


    private List<AudioHeader> audio_headers; 
    private List<VideoHeader> video_headers;   
    
    public enum GateState{
        OPEN,
        CLOSE,
        ERROR
    }
    
    GateState gateState;

    public PDSMessage() {
        audio_headers = new ArrayList<>();
        video_headers = new ArrayList<>();
        gateState = null;
    }

    public PDSMessage(String message) throws MalformedPacketException{
        audio_headers = new ArrayList<>();
        video_headers = new ArrayList<>();
        parse(message);
    }
    
    /**Should not allow additions of the same element
     * 
     * @param header 
     */
    public void addAudioHeader(AudioHeader header){
        for (AudioHeader h : audio_headers){
            if(h.equals(header))
                return;
        }
        audio_headers.add(header);
    }
    
    public void addAudioHeader(List<AudioHeader> headers) {
        headers.forEach(h -> {
            addAudioHeader(h);
        });
    }
    
    public void addVideoHeader(VideoHeader header){
        for (VideoHeader h : video_headers){
            if(h.equals(header))
                return;
        }
        video_headers.add(header);
    }
    
    public List<AudioHeader> getAudioHeaders(){
        return audio_headers;
    }        
    
    public AudioHeader getAudioHeaderFirst(){
        return audio_headers != null && audio_headers.size() > 0 ? audio_headers.get(0) : null;
    }    
    
    public List<VideoHeader> getVideoHeaders(){
        return video_headers;
    }
    
    public VideoHeader getVideoHeaderFirst(){
         return video_headers != null && video_headers.size() > 0 ? video_headers.get(0) : null;
    }
    
    public GateState getGateState(){
        return gateState;
    }

    public void setGateState(GateState state){
        gateState = state;
    }
    
    public final void parse(String s) throws MalformedPacketException, NumberFormatException, IllegalArgumentException{
        s.lines().forEach( line -> {
            String tokens[] = line.split("=");
            if(tokens.length > 2){
                //Something is not right 
                throw new MalformedPacketException("Only one '=' should be per line");
            }
            switch(tokens[0].strip()){
                case "a":
                    AudioHeader header = AudioHeader.parse(tokens[1]);
                    addAudioHeader(header);
                    break;
                case "b":
                    parseGateHeader(tokens[1]);
                    break;
                case "v":
                    VideoHeader vh = VideoHeader.parse(tokens[1]);
                    addVideoHeader(vh);
                    break;
                default:
                    Logger.getLogger(PDSMessage.class.getName()).log(Level.WARNING, "Unknown PDS Header: {0}", tokens[0]);
            }
        });
    }
   
    private void parseGateHeader(String value){
        //open or close
        value = value.strip();
        if(value.equalsIgnoreCase("open")){
            gateState = GateState.OPEN;
        } else if(value.equalsIgnoreCase("close")) {
            gateState = GateState.CLOSE;
        } else if (value.equalsIgnoreCase("error")){
            gateState = GateState.ERROR;
        } else {
            throw new MalformedPacketException("Unknown Gate State");
        }
    }    


    @Override
    public String toString() {
        String s = "";
        
        for (AudioHeader ah : audio_headers){
            s += "a = " + ah + "\n";
        }
        for (VideoHeader vh : video_headers){
            s += "v = " + vh + "\n";
        }
        
        if(gateState != null){
            s += "b = " + gateState.name() +"\n";
        }
        
        return s;
    }
    
    
}
