/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg15_sip_application.sip.pds;

/**
 *
 * @author rand
 */
public class PDSHeader {

    protected int src_port;
    protected int rcv_port;

    public PDSHeader() {
        src_port = rcv_port = 0;
    }

    public PDSHeader(int src_port, int dst_port) {
        setRcvPort(dst_port);
        setSrcPort(src_port);
    }

    public boolean isIncomingStream() {
        return rcv_port > 0;
    }

    public boolean isOutgoingStream() {
        return src_port > 0;
    }

    public int getSrcPort() {
        return src_port;
    }

    private static int checkPort(int port) {
        if (port < 0 || port > 0xFFFF) {
            throw new IllegalArgumentException("port out of range:" + port);
        }
        return port;
    }

    public final void setSrcPort(int src_port) {
        this.src_port = checkPort(src_port);
    }

    public int getRcvPort() {
        return rcv_port;
    }

    public final void setRcvPort(int rcv_port) {
        this.rcv_port = checkPort(rcv_port);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + this.src_port;
        hash = 41 * hash + this.rcv_port;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PDSHeader other = (PDSHeader) obj;
        if (this.src_port != other.src_port) {
            return false;
        }
        if (this.rcv_port != other.rcv_port) {
            return false;
        }
        return true;
    }

}
