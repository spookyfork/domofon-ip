/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg15_sip_application.video;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author rand
 */
public class ImageReceiver extends javax.swing.JPanel implements Runnable {
    public enum PacketFormat {
        PACKET_BYTE(0),
        PACKET_SHORT(1),
        PACKET_INT(3);

        public final int value;

        private PacketFormat(int val) {
            value = val;
        }
    }

    public enum ImageFormat {
        //TYPE_CUSTOM(0),
        TYPE_INT_RGB(1),
        TYPE_INT_ARGB(2),
        TYPE_INT_ARGB_PRE(3),
        TYPE_INT_BGR(4),
        TYPE_3BYTE_BGR(5),
        TYPE_4BYTE_ABGR(6),
        TYPE_4BYTE_ABGR_PRE(7),
        TYPE_USHORT_565_RGB(8),
        TYPE_USHORT_555_RGB(9),
        TYPE_BYTE_GRAY(10),
        TYPE_USHORT_GRAY(11),
        TYPE_BYTE_BINARY(12),
        TYPE_BYTE_INDEXED(13);

        public final int value;

        private ImageFormat(int val) {
            value = val;
        }
    }

    private boolean shouldExit;
    private Thread thread;
    private DatagramSocket sock;
    private DatagramPacket pack;
    private int port;
    private byte[] packetPayload = new byte[4096];
    private ByteBuffer packetBuffer;
    private int partSize;
    private PacketFormat __packetFormat = PacketFormat.PACKET_SHORT;

    private volatile int __width;
    private volatile int __height;
    private volatile ImageFormat __imageFormat = ImageFormat.TYPE_USHORT_565_RGB;
    private volatile int __bytesPerPixel = 3;

    private BufferedImage img;
    private DataBuffer data;

    public ImageReceiver() {

        this(640,480, 6999);

    }

    public ImageReceiver(int width, int height, int port) {
        this.__width = width;
        this.__height = height;
        this.port = port;

        init();
    }

    private void init() {
        synchronized (this) {
            img = new BufferedImage(__width, __height, __imageFormat.value);
            data = (DataBuffer) img.getRaster().getDataBuffer();
            packetBuffer = ByteBuffer.wrap(packetPayload).order(ByteOrder.LITTLE_ENDIAN);
        }

    }

    public PacketFormat getPacketFormat() {
        return __packetFormat;
    }

    public void setPacketFormat(PacketFormat packetFormat) {
        this.__packetFormat = packetFormat;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getPartSize() {
        return this.partSize;
    }

    public void setPartSize(int partSize) {
        this.partSize = partSize;
    }

    public int getImageWidth() {
        return __width;
    }

    public void setImageWidth(int width) {
        this.__width = width;
        init();
        repaint();
    }

    public int getImageHeight() {
        return __height;
    }

    public void setImageHeight(int height) {
        this.__height = height;
        init();
        repaint();
    }

    public ImageReceiver.ImageFormat getImageFormat() {
        return __imageFormat;
    }

    public void setImageFormat(ImageReceiver.ImageFormat format) {
        this.__imageFormat = format;
        init();
        repaint();
    }

    public int getBytesPerPixel() {
        return __bytesPerPixel;
    }

    public void setBytesPerPixel(int bytesPerPixel) {
        this.__bytesPerPixel = bytesPerPixel;
    }

    @Override
    public void run() {
        System.out.println("Started ImageReceiver Thread");
        int part, i;
        part = -1;
        while (!shouldExit) {
            try {
                pack = new DatagramPacket(packetPayload, packetPayload.length);
                sock.receive(pack);

                packetBuffer.rewind();
                part = packetBuffer.getShort();
                i = 0;
                synchronized (this) {
                    while (i < pack.getLength() / 2 - 1) {
                        //TODO this was changed from 510 to 512 so the sender wont with 512 packet size 
                        //data.getData()[part * (partSize / 2) + i++] = packetBuffer.get();
                        switch (__packetFormat) {
                            case PACKET_BYTE:
                                data.setElem(part * (partSize / 2) + i++, packetBuffer.get());
                                break;
                            case PACKET_INT:
                                data.setElem(part * (partSize / 2) + i++, packetBuffer.getInt());
                                break;
                            case PACKET_SHORT:
                                data.setElem(part * (partSize / 2) + i++, packetBuffer.getShort());
                                break;
                        }
                    }
                }

                //FIXME Repaint only when last packet is received or the packet we receive is of lower index than the previous one?
                repaint();
            } catch (SocketTimeoutException ex) {
                continue;
            } catch (IOException ex) {
                Logger.getLogger(ImageReceiver.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ArrayIndexOutOfBoundsException ex) {
                ;
            }
        }
        System.out.println("Ended ImageReceiver Thread");
    }

    public void open() throws SocketException {
        sock = new DatagramSocket(port);
        sock.setSoTimeout(500);
    }
    
    public void close(){
        sock.close();
    }
    
    public boolean isOpen() {
        return sock != null && !sock.isClosed();
    }

    public void start() {
        if (thread != null) {
            return;
        }
        shouldExit = false;
        thread = new Thread(this);
        thread.start();
    }

    public void exit() {
        shouldExit = true;
        //join();
    }

    public void join() throws InterruptedException {
        thread.join();
        thread = null;
    }

    @Override
    protected void paintComponent(Graphics g) {

//        for (int x = 0; x < __width; x++) {
//            for (int y = 0; y < __height; y++) {
//                //RGB
//                short red = (short) (0b11111 * (1.f - y / (float) __height));
//                short green = (short) (0b111111 * x / (float) __width);
//                short blue = (short) (0b11111 * y / (float) __height);
//
//                short rgb = (short) ((red << 11) | (green << 5) | blue);
//                //img.setRGB(x, y, 0xFFFF);
//                data.getData()[y * __width + x] = rgb;
//            }
//        }
        super.paintComponent(g); //To change body of generated methods, choose Tools | Templates.
        g.setColor(Color.blue);
        g.fillRect(0, 0, getWidth(), getHeight());
        g.drawImage(img, 0, 0, null);
        //System.out.println(Arrays.toString(data.getData()));
    }

    public boolean working() {
        if (thread != null) {
            if (thread.isAlive()) {
                return true;
            } else {
                thread = null;
                return false;
            }
        }
        return false;
    }

    void clear() {
        synchronized (this) {
            for (int i = 0; i < data.getSize(); i++) {
                data.setElem(i, 0);
            }
        }
        repaint();
    }

    BufferedImage copy() {
        synchronized (this) {
            ColorModel cm = img.getColorModel();
            boolean isRasterPremultiplied = img.isAlphaPremultiplied();
            WritableRaster raster = img.copyData(null);
            BufferedImage ret = new BufferedImage(cm, raster, isRasterPremultiplied, null);
            return ret;
        }
    }

    void saveToFile(String path, String format) throws IOException {
        //check if already has extention
        File f = new File(path);
        if (!f.getName().contains(".")) {
            f = new File(path + '.' + format);
        }
        ImageIO.write(img, format, f);
    }

}
