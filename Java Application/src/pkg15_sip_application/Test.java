/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg15_sip_application;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.BindException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.TargetDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;
import local.media.AudioClipPlayer;
import local.ua.GraphicalUA;
import local.ua.UserAgentProfile;
import org.zoolu.sdp.AttributeField;
import org.zoolu.sdp.MediaField;
import org.zoolu.sdp.SessionDescriptor;
import org.zoolu.sip.call.OptionsServer;
import org.zoolu.sip.provider.SipProvider;
import org.zoolu.sip.provider.SipStack;
import org.zoolu.sip.provider.Transport;
import org.zoolu.sip.provider.UdpTransport;
import pkg15_sip_application.audio.net.MediaTransferManager;
import pkg15_sip_application.sip.pds.AudioHeader;

/**
 *
 * @author rand
 */
public class Test {

    public static void main_other(String[] args) throws IOException {
        try {
            BufferedReader reader
                    = new BufferedReader(new InputStreamReader(System.in));

            // Reading data using readLine 
            String line = reader.readLine();

            SipProvider provider = new SipProvider(java.net.Inet4Address.getLocalHost().getHostAddress(), Integer.parseInt(line));
            UserAgentProfile profile = new UserAgentProfile();
            profile.ua_address = "192.168.1.11";
            profile.do_register = false;

            GraphicalUA gui = new GraphicalUA(provider, profile);
            java.awt.EventQueue.invokeLater(new Runnable() {
                public void run() {
                    gui.setVisible(true);
                }
            });
        } catch (UnknownHostException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void rebindPortWithTransport() throws InterruptedException {
        try {
            Transport transport = new UdpTransport(5060);
            Transport t[] = {transport};
            SipProvider provider = new SipProvider(java.net.Inet4Address.getLocalHost().getHostAddress(), transport.getLocalPort(), t);
            OptionsServer options_server = new OptionsServer(provider, "INVITE, ACK, CANCEL, OPTIONS, BYE, MESSAGE", "application/sw_domofon");
            transport.halt(); //this stops all objects using this transport

            while (true) {
                try {
                    transport = new UdpTransport(5060);
                    System.out.println("We cool!");
                    break;
                } catch (BindException ex) {
                    Thread.sleep(100);
                }
            }

            t[0] = transport;
            provider = new SipProvider(java.net.Inet4Address.getLocalHost().getHostAddress(), transport.getLocalPort(), t);
            options_server = new OptionsServer(provider, "INVITE, ACK, CANCEL, OPTIONS, BYE, MESSAGE", "application/sw_domofon");
            //transport.halt(); //this stops all objects using this transport

        } catch (UnknownHostException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void rebindPortWithoutTransport() throws InterruptedException {
        try {

            SipProvider provider = new SipProvider(java.net.Inet4Address.getLocalHost().getHostAddress(), 5060);
            OptionsServer options_server = new OptionsServer(provider, "INVITE, ACK, CANCEL, OPTIONS, BYE, MESSAGE", "application/sw_domofon");
            provider.halt(); //

            while (true) {
                try {
                    provider = new SipProvider(java.net.Inet4Address.getLocalHost().getHostAddress(), 5060);
                    System.out.println("We cool!");
                    break;
                } catch (Exception ex) {
                    Thread.sleep(100);
                }
            }

            provider = new SipProvider(java.net.Inet4Address.getLocalHost().getHostAddress(), 5060);
            options_server = new OptionsServer(provider, "INVITE, ACK, CANCEL, OPTIONS, BYE, MESSAGE", "application/sw_domofon");
            //transport.halt(); //this stops all objects using this transport

        } catch (UnknownHostException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void creatingSDP(){
        SessionDescriptor sd = new SessionDescriptor();
        sd.addMedia(new MediaField("audio", 666, 0, "udp", "format"), new AttributeField("huj wie"));
        
        System.out.println(sd.toString());
    }
    
    
    public static void mediaTransferManagerTest() throws LineUnavailableException{
        MediaTransferManager m = new MediaTransferManager("localhost");
       
        AudioFormat format = new AudioFormat(16000, 16, 1, true, true);
        AudioHeader header = m.createAudioConnection(format);
        
    }

    
    public static void doubleOutputLines() throws LineUnavailableException, IOException, UnsupportedAudioFileException, InterruptedException {
//        AudioClipPlayer p1 = new AudioClipPlayer("progress.wav", null);
//        AudioClipPlayer p2 = new AudioClipPlayer("ring.wav", null);
//        p1.setLoopCount(2);
//        p1.play();
//        p2.setLoopCount(2);
//        p2.play();
//        Thread.sleep(10000);
//        System.out.println("Test");


        AudioFormat format = new AudioFormat(44100, 16, 1, true, true);
        AudioInputStream stream1 = AudioSystem.getAudioInputStream(format, AudioSystem.getAudioInputStream(new File("progress.wav")));
        AudioInputStream stream2 = AudioSystem.getAudioInputStream(format, AudioSystem.getAudioInputStream(new File("ring.wav")));
        byte[] bytes1 = stream1.readAllBytes();
        byte[] bytes2 = stream2.readAllBytes();

        DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);
//        Clip clip1 = (Clip) AudioSystem.getLine(info);
//        Clip clip2 = (Clip) AudioSystem.getLine(info);
//        clip1.open(AudioSystem.getAudioInputStream(new File("progress.wav")));
//        clip1.start();
//        clip2.open(AudioSystem.getAudioInputStream(new File("ring.wav")));
//        clip2.start();
//        Thread.sleep(10000);

//        SourceDataLine clip1 = (SourceDataLine) AudioSystem.getLine(info);
//        SourceDataLine clip2 = (SourceDataLine) AudioSystem.getLine(info);
        SourceDataLine clip1 = AudioSystem.getSourceDataLine(format);
        SourceDataLine clip2 = AudioSystem.getSourceDataLine(format);
        clip1.open();
        clip1.start();
        clip2.open();
        clip2.start();
        
        new Thread(()->{clip1.write(bytes1, 0, bytes1.length);}).start();
        new Thread(()->{clip2.write(bytes2, 0, bytes2.length);}).start();
        
        
        //Thread.sleep(10000);
    }
    
    public static void doubleTargetLines() throws LineUnavailableException, UnsupportedAudioFileException, IOException {
        
        AudioFormat format = new AudioFormat(8000, 16, 1, true, true);
        AudioFormat format2 = new AudioFormat(16000, 16, 1, true, true);
        
        TargetDataLine tdl1 = AudioSystem.getTargetDataLine(format);
        SourceDataLine clip = AudioSystem.getSourceDataLine(format);
        SourceDataLine clip1 = AudioSystem.getSourceDataLine(format);
        tdl1.open();
        tdl1.start();
        clip.open();
        clip.start();
        clip1.open();
        clip1.start();
        
        AudioInputStream stream1 = AudioSystem.getAudioInputStream(format, AudioSystem.getAudioInputStream(new File("progress.wav")));
        byte[] bytes1 = stream1.readAllBytes();
        
        new Thread(()->{while(true){clip1.write(bytes1, 0, bytes1.length);}}).start();
        
        byte[] bytes = new byte[1024];
        
        while(true) {
            int len = tdl1.read(bytes, 0, bytes.length);
            clip.write(bytes, 0, len);
        }
        
    }
    
    public static void main(String[] args) throws LineUnavailableException, InterruptedException, IOException, UnsupportedAudioFileException {

//        SipStack.init();
//
//        try {
//            mediaTransferManagerTest();
//        } catch (LineUnavailableException ex) {
//            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
//        }

        doubleTargetLines();

    }

}
