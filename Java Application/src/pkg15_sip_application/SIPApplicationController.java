/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg15_sip_application;

import java.io.IOException;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.LineUnavailableException;
import org.zoolu.sip.address.SipURL;
import org.zoolu.sip.provider.SipStack;
import pkg15_sip_application.audio.AudioSourceWrapper;
import pkg15_sip_application.audio.TargetLineListener;
import pkg15_sip_application.audio.net.AudioReceiver;
import pkg15_sip_application.audio.net.AudioSender;
import pkg15_sip_application.audio.net.MediaTransferManager;
import pkg15_sip_application.exception.AlreadyInUseException;
import pkg15_sip_application.exception.MalformedPacketException;
import pkg15_sip_application.sip.CallAgent;
import pkg15_sip_application.sip.CallAgentListener;
import pkg15_sip_application.sip.pds.AudioHeader;
import pkg15_sip_application.sip.pds.PDSMessage;
import pkg15_sip_application.sip.pds.VideoHeader;
import pkg15_sip_application.video.ImageReceiver;
import view.SIPApplicationWindow;

/**
 *
 * @author rand
 */
public class SIPApplicationController implements CallAgentListener {

    private CallAgent callAgent;
    private MediaTransferManager mediaTransferManager;
    private SIPApplicationWindow view;
    private ImageReceiver imageReceiver;

    private AudioFormat supportedFormat = new AudioFormat(8000, 16, 1, true, true);

    private PDSMessage pdsOffer = null;

    private List<TargetLineListener> viewOutputListeners;
    private List<TargetLineListener> viewInputListeners;

    public SIPApplicationController() throws IOException {
        SipStack.init(); //initiate mjsip stack
        viewOutputListeners = new ArrayList<>();
        viewInputListeners = new ArrayList<>();
    }

    public SIPApplicationController(SIPApplicationWindow view) throws IOException {
        this();
        this.view = view;
        imageReceiver = view.getImageReceiver();
    }

    public void setView(SIPApplicationWindow window) {
        this.view = window;
        imageReceiver = view.getImageReceiver();
    }

    public SIPApplicationWindow getView() {
        return view;
    }

    public void open() {
        try {
            callAgent = new CallAgent(); //try creating CallAgent on default 5060 port
        } catch (IOException ex) {
            //Cannot create on port 5060, Create on port assigned by system (0)
            Logger.getLogger(SIPApplicationController.class.getName()).log(Level.WARNING, "Cannot bind callAgent to default port (5060)");
            try {
                if (callAgent != null) {
                    callAgent.halt();
                    callAgent = null;
                }
                callAgent = new CallAgent(0);
                Logger.getLogger(SIPApplicationController.class.getName()).log(Level.WARNING, "CallAgent is using port " + callAgent.getPort());
                if (view != null) {
                    view.displayModalWarning(//FIXME
                            "Could not bind to port " + "5060" + "\n"
                            + "Using port " + callAgent.getPort() + " instead\nUse settings if you wish to change it.",
                            "Port in use!");
                }
            } catch (IOException ex1) {
                Logger.getLogger(SIPApplicationController.class.getName()).log(Level.SEVERE, "Cannot bind CallAgent to any port", ex1);
                if (view != null) {
                    view.displayModalError("Cannot create CallAgent", "Fatal Error");
                    view.dispose();
                }
                if (callAgent != null) {
                    callAgent.halt();
                    callAgent = null;
                }
            }
        }
        if (callAgent != null) {
            callAgent.addCallAgentListener(this);
        }
    }

    public void close() {
        if (callAgent != null) {
            callAgent.halt();
        }
        if (mediaTransferManager != null) {
            mediaTransferManager.clear();
        }
    }

    /**
     * Start a new outgoing session
     *
     * @param host
     */
    public void call(String host) {
        SipURL url = new SipURL(host);

        try {
            if (mediaTransferManager != null) {
                mediaTransferManager.clear();
            }
            //Create session manager for new call
            mediaTransferManager = new MediaTransferManager(url.getHost());
            //Decide what media connections to offer
            //Try opening the connections for formats we want handled
            prepareMediaSessionToOffer();
            //Create PDS offer based on connections we were able to open
            PDSMessage pds = createPDSOffer();
            //Send invite with the offer
            this.callAgent.call(host, pds);
        } catch (AlreadyInUseException | UnknownHostException ex) {
            Logger.getLogger(SIPApplicationController.class.getName()).log(Level.SEVERE, "Unable to call " + host, ex);
            if (view != null) {
                view.displayModalError("Unable to call " + host, "Call Error");
            }
            //Close receivers and senders
            mediaTransferManager.clear();
        } catch (LineUnavailableException ex) {
            Logger.getLogger(SIPApplicationController.class.getName()).log(Level.SEVERE, "Cannot Create media session", ex);
            if (view != null) {
                view.displayModalError(ex.getMessage(), "Call Error");;
            }
            mediaTransferManager.clear();
        }
    }

    /**
     * Accept incoming call
     */
    public void accept() {
        try {
            //If we got a PDS offer prepare media session for it
            //And pass the ports we got from the offer
            if (pdsOffer != null) {
                mediaTransferManager = new MediaTransferManager(callAgent.getHost());
                prepareMediaSessionBasedOnOffer(pdsOffer);
            }
            //Based on created media session create a PDS response
            PDSMessage response = createPDSOffer();
            //Accept SIP call
            this.callAgent.accept(response);
        } catch (LineUnavailableException e) {
            Logger.getLogger(SIPApplicationController.class.getName()).log(Level.WARNING,
                    "Line not available", e);
            if (view != null) {
                view.displayModalError(e.getMessage(), "Error");
            }
        } catch (SocketException ex) {
            Logger.getLogger(SIPApplicationController.class.getName()).log(Level.WARNING, null, ex);
        }
    }

    /**
     * Refuse incoming session
     *
     */
    public void refuse() {
        if (view != null) {
            view.stopClipPlayer();
        }
        this.callAgent.refuse();
        if (mediaTransferManager != null) {
            mediaTransferManager.clear();
        }
        if (imageReceiver != null && imageReceiver.isOpen()) {
            imageReceiver.close();
        }
    }

    /**
     * Hangup established session
     *
     */
    public void hangup() {
        this.callAgent.hangup();
        if (mediaTransferManager != null) {
            mediaTransferManager.clear();
        }
        if (view != null) {
            view.stopClipPlayer();
        }
        if (imageReceiver != null && imageReceiver.working()) {
            try {
                imageReceiver.exit();
                imageReceiver.join();
                imageReceiver.close();
            } catch (InterruptedException ex) {
                Logger.getLogger(SIPApplicationController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Push-to-talk pressed
     *
     */
    public void talk() {
        this.mediaTransferManager.talk();
    }

    /**
     * Push-to-talk released
     *
     */
    public void silence() {
        this.mediaTransferManager.silence();
    }

    /**
     * Send the open gate signal
     */
    public void gateOpen() {
        if (!callAgent.getStatus().equals("ACTIVE")) {
            return;
        }
        PDSMessage msg = new PDSMessage();
        msg.setGateState(PDSMessage.GateState.OPEN);
        callAgent.send(msg.toString());
    }

    /**
     * Send the open gate signal
     */
    public void gateClose() {
        if (!callAgent.getStatus().equals("ACTIVE")) {
            return;
        }
        PDSMessage msg = new PDSMessage();
        msg.setGateState(PDSMessage.GateState.CLOSE);
        callAgent.send(msg.toString());
    }

    /**
     * Prepare media session based on PDS offer
     *
     * @param pds
     * @throws javax.sound.sampled.LineUnavailableException
     * @throws java.net.SocketException
     */
    protected void prepareMediaSessionBasedOnOffer(PDSMessage pds)
            throws LineUnavailableException, SocketException {
        if (pds == null) {
            return;
        }

        //Create One Outgoing Audio Connection
        //Create One Incoming Audio Connection
        //Go throught all headers until both Incoming and Outgoing Connections 
        //are created
        boolean local_incoming = false;
        boolean local_outgoing = false;
        for (AudioHeader ah : pds.getAudioHeaders()) {
            if (ah.isIncomingStream() && !local_outgoing) {
                //remote wants to receive data and we do not have sender
                mediaTransferManager.createAudioConnection(ah.getFormat(), true, false);
                mediaTransferManager.bindAudioConnection(ah.getFormat(), ah.getSrcPort(), ah.getRcvPort());
                local_outgoing = true;
            }
            if (ah.isOutgoingStream() && !local_incoming) {
                //remote wants to send data and we dont have receiver
                mediaTransferManager.createAudioConnection(ah.getFormat(), false, true);
                mediaTransferManager.bindAudioConnection(ah.getFormat(), ah.getSrcPort(), ah.getRcvPort());
                local_incoming = true;
            }
            if (local_incoming && local_outgoing) {
                break;
            }
        }

        //Get First video header that is offering an Incoming Stream
        //Create One Incoming Video Connection Only!
        for (VideoHeader vh : pds.getVideoHeaders()) {
            if (vh.isOutgoingStream()) {
                imageReceiver.setImageFormat(vh.getFormat());
                imageReceiver.setPacketFormat(ImageReceiver.PacketFormat.PACKET_SHORT);
                imageReceiver.setImageHeight(vh.getHeight());
                imageReceiver.setImageWidth(vh.getWidth());
                imageReceiver.setPartSize(vh.getPacketSize());
                //TODO: set source port 
                imageReceiver.open();
                break;
            }
        }
    }

    /**
     * Get PDS response and bind it to opened connections
     *
     */
    protected void prepareMediaTransferBasedOnResponse(PDSMessage response) throws LineUnavailableException {
        if (response == null) {
            return;
        }
        Set<AudioFormat> acceptedFormats = new HashSet<>(response.getAudioHeaders().size());
        //Create connections for all responses if they responded with ports >0 
        for (AudioHeader header : response.getAudioHeaders()) {
            AudioFormat format = header.getFormat();
            AudioSender sender = mediaTransferManager.getAudioSender(format);
            AudioReceiver receiver = mediaTransferManager.getAudioReceiver(format);

            if (sender != null) {
                //There is sender 
                if (header.getRcvPort() != 0) {
                    //There is a remote receiver
                    sender.setPort(header.getRcvPort());
                    acceptedFormats.add(format);
                } else {
                    //No remote receiver, close the sender
                    mediaTransferManager.closeAudioSender(format);
                }
            }
            if (receiver != null) {
                if (header.getRcvPort() != 0) {
                    receiver.setRemotePort(header.getSrcPort());
                    acceptedFormats.add(format);
                } else {
                    //Close the receiver
                    mediaTransferManager.closeAudioReceiver(format);
                }
            }
        }
        mediaTransferManager.closeMediaTransfersDifference(new ArrayList<>(acceptedFormats));
    }

    /**
     * Prepare media session based on internal supported formats. This is to
     * later create PDS offer.
     */
    protected void prepareMediaSessionToOffer() throws LineUnavailableException {
        //FIXME: Get list of supported formats?
        //mediaTransferManager.createAudioConnection(supportedFormat);
        mediaTransferManager.createAudioConnection(supportedFormat, true, true);
    }

    protected PDSMessage createPDSOffer() {
        //Based on created media connections 
        Map<AudioFormat, AudioSender> senders = mediaTransferManager.getAudioSenders();
        Map<AudioFormat, AudioReceiver> receivers = mediaTransferManager.getAudioReceivers();

        Set<AudioFormat> formats = new HashSet<>();
        formats.addAll(senders.keySet());
        formats.addAll(receivers.keySet());
        PDSMessage pds = new PDSMessage();
        for (AudioFormat format : formats) {
            AudioSender sender = senders.get(format);
            AudioReceiver receiver = receivers.get(format);
            AudioHeader header = new AudioHeader(0, 0, format);
            if (sender != null) {
                header.setSrcPort(sender.getLocalPort());
            }
            if (receiver != null) {
                header.setRcvPort(receiver.getLocalPort());
            }
            pds.addAudioHeader(header);
        }

        if (imageReceiver.isOpen()) {
            VideoHeader vh = new VideoHeader(0, imageReceiver.getPort(),
                    imageReceiver.getImageWidth(),
                    imageReceiver.getImageHeight(),
                    imageReceiver.getPartSize(),
                    imageReceiver.getImageFormat());
            pds.addVideoHeader(vh);
        }
        return pds;
    }

    //------------------------ CALL AGENT LISTENER ----------------------------
    @Override
    public void onCallIncoming(String body) {
        if (body != null && !"".equals(body)) {
            try {
                //Get offer and store it 
                pdsOffer = new PDSMessage(body);

            } catch (MalformedPacketException | IllegalArgumentException e) {
                //Cannot parse pds message
                Logger.getLogger(SIPApplicationController.class
                        .getName()).log(Level.WARNING, "SIP body unknown: {0}", body);
            }
        }
        view.startRingingClip();
        view.displayIncomingCallDialog(callAgent.getHost());
    }

    @Override
    public void onCallRinging() {
        if (view != null) {
            view.startToneClip();
        }
    }

    @Override
    public void onCallEstablished(String body) {
        if (view != null) {
            view.stopClipPlayer();
        }
        if (body != null) {
            //This should only happen when we get 2xx response
            try {
                PDSMessage pds = new PDSMessage(body);
                //Bind the resonse
                prepareMediaTransferBasedOnResponse(pds);

            } catch (LineUnavailableException ex) {
                Logger.getLogger(SIPApplicationController.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }

        //Attach view to outgoing signal (local input)
        if (mediaTransferManager.getInputAudioWrapper() != null) {
            //get first only 
            AudioSourceWrapper asw = mediaTransferManager.getInputAudioWrapper();
            for (TargetLineListener tll : viewInputListeners) {
                asw.addTargetLineListener(tll);
            }
        }
        //Attach view to incoming signal (oscilloscope)
        if (!mediaTransferManager.getOutputAudioWrappers().isEmpty()) {
            //get first only 
            AudioSourceWrapper asw = mediaTransferManager.getOutputAudioWrappers().get(0);
            for (TargetLineListener tll : viewOutputListeners) {
                asw.addTargetLineListener(tll);
            }
        }

        mediaTransferManager.startMediaTransfer();
        if (imageReceiver != null && imageReceiver.isOpen()) {
            imageReceiver.start();
        }
    }

    @Override
    public void onCallEnded() {
        view.stopClipPlayer();
        view.hideIncomingCallDialog();
        mediaTransferManager.stopMediaTransfer();
        mediaTransferManager.clear();
        if (imageReceiver != null && imageReceiver.working()) {
            try {
                imageReceiver.exit();
                imageReceiver.join();
                imageReceiver.close();
            } catch (InterruptedException ex) {
                Logger.getLogger(SIPApplicationController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void onCallTimeout() {
        if (view != null) {
            view.stopClipPlayer();
            view.displayModalError("Connection to " + callAgent.getHost() + " timed out.", "Call Timeout");

        }
    }

    @Override
    public void onMessageReceived(String subject, String body) {
        if (view != null) {
            //view.displayModalInfo(body, subject);
            try {
                PDSMessage msg = new PDSMessage(body);
                view.setGateOpened(msg.getGateState().equals(PDSMessage.GateState.OPEN));
            } catch (MalformedPacketException e) {
                ;

            }
        }
        Logger.getLogger(SIPApplicationController.class
                .getName()).log(Level.INFO,
                        "Message received:" + body);
    }

    @Override
    public void onMessageDelivered(String body) {
        if (view != null) {
            //view.displayModalInfo("Message \"" + body + "\" delivered.", "Message Delivered");
        }
        Logger.getLogger(SIPApplicationController.class
                .getName()).log(Level.INFO,
                        "Message delivered:" + body);
    }

    @Override
    public void onMessageFailure(String body) {
        if (view != null) {
            //view.displayModalError("Message \"" + body + "\" couldn't be delivered.", "Message Error");
        }
        Logger.getLogger(SIPApplicationController.class
                .getName()).log(Level.INFO,
                        "Message failed:" + body);
    }

    public void options(String host) {
        callAgent.options(host);
    }

    public void addInputListener(TargetLineListener tll) {
        viewInputListeners.add(tll);
    }

    public void addOutputListener(TargetLineListener tll) {
        viewOutputListeners.add(tll);
    }

    public void removeInputListener(TargetLineListener tll) {
        viewOutputListeners.remove(tll);
    }

    public void removeOutputListener(TargetLineListener tll) {
        viewOutputListeners.remove(tll);
    }

}
